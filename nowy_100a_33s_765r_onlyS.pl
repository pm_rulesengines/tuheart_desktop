
xtype [	name : xtype_0 ,
	base : symbolic ,
	domain : ['r'/1, 's'/2, 't'/3, 'u'/4, 'v'/5, 'w'/6, 'x'/7, 'y'/8, 'z'/9, 'aa'/10, 'ab'/11, 'ac'/12, 'ad'/13, 'ae'/14, 'af'/15, 'ag'/16, 'ah'/17, 'ai'/18, 'aj'/19, 'ak'/20, 'al'/21, 'am'/22, 'an'/23, 'ao'/24, 'ap'/25, 'aq'/26, 'ar'/27, 'as'/28, 'at'/29, 'au'/30, 'av'/31, 'aw'/32, 'ax'/33, 'ay'/34, 'az'/35, 'ba'/36, 'bb'/37, 'bc'/38, 'bd'/39, 'be'/40] ,
	ordered : yes ] .
xtype [	name : xtype_1 ,
	base : symbolic ,
	domain : ['aa'/1, 'ab'/2, 'ac'/3, 'ad'/4, 'ae'/5, 'af'/6, 'ag'/7, 'ah'/8, 'ai'/9, 'aj'/10, 'ak'/11, 'al'/12, 'am'/13, 'an'/14, 'ao'/15, 'ap'/16, 'aq'/17, 'ar'/18, 'as'/19, 'at'/20, 'au'/21, 'av'/22, 'aw'/23, 'ax'/24, 'ay'/25, 'az'/26, 'ba'/27, 'bb'/28, 'bc'/29, 'bd'/30, 'be'/31, 'bf'/32, 'bg'/33, 'bh'/34, 'bi'/35, 'bj'/36, 'bk'/37, 'bl'/38, 'bm'/39, 'bn'/40] ,
	ordered : yes ] .
xtype [	name : xtype_2 ,
	base : symbolic ,
	domain : ['c'/1, 'd'/2, 'e'/3, 'f'/4, 'g'/5, 'h'/6, 'i'/7, 'j'/8, 'k'/9, 'l'/10, 'm'/11, 'n'/12, 'o'/13, 'p'/14, 'q'/15, 'r'/16, 's'/17, 't'/18, 'u'/19, 'v'/20, 'w'/21, 'x'/22, 'y'/23, 'z'/24, 'aa'/25, 'ab'/26, 'ac'/27, 'ad'/28, 'ae'/29, 'af'/30, 'ag'/31, 'ah'/32, 'ai'/33, 'aj'/34, 'ak'/35, 'al'/36, 'am'/37, 'an'/38, 'ao'/39, 'ap'/40] ,
	ordered : yes ] .
xtype [	name : xtype_3 ,
	base : symbolic ,
	domain : ['m'/1, 'n'/2, 'o'/3, 'p'/4, 'q'/5, 'r'/6, 's'/7, 't'/8, 'u'/9, 'v'/10, 'w'/11, 'x'/12, 'y'/13, 'z'/14, 'aa'/15, 'ab'/16, 'ac'/17, 'ad'/18, 'ae'/19, 'af'/20, 'ag'/21, 'ah'/22, 'ai'/23, 'aj'/24, 'ak'/25, 'al'/26, 'am'/27, 'an'/28, 'ao'/29, 'ap'/30, 'aq'/31, 'ar'/32, 'as'/33, 'at'/34, 'au'/35, 'av'/36, 'aw'/37, 'ax'/38, 'ay'/39, 'az'/40] ,
	ordered : yes ] .
xtype [	name : xtype_4 ,
	base : symbolic ,
	domain : ['x'/1, 'y'/2, 'z'/3, 'aa'/4, 'ab'/5, 'ac'/6, 'ad'/7, 'ae'/8, 'af'/9, 'ag'/10, 'ah'/11, 'ai'/12, 'aj'/13, 'ak'/14, 'al'/15, 'am'/16, 'an'/17, 'ao'/18, 'ap'/19, 'aq'/20, 'ar'/21, 'as'/22, 'at'/23, 'au'/24, 'av'/25, 'aw'/26, 'ax'/27, 'ay'/28, 'az'/29, 'ba'/30, 'bb'/31, 'bc'/32, 'bd'/33, 'be'/34, 'bf'/35, 'bg'/36, 'bh'/37, 'bi'/38, 'bj'/39, 'bk'/40] ,
	ordered : yes ] .
xtype [	name : xtype_5 ,
	base : symbolic ,
	domain : ['u'/1, 'v'/2, 'w'/3, 'x'/4, 'y'/5, 'z'/6, 'aa'/7, 'ab'/8, 'ac'/9, 'ad'/10, 'ae'/11, 'af'/12, 'ag'/13, 'ah'/14, 'ai'/15, 'aj'/16, 'ak'/17, 'al'/18, 'am'/19, 'an'/20, 'ao'/21, 'ap'/22, 'aq'/23, 'ar'/24, 'as'/25, 'at'/26, 'au'/27, 'av'/28, 'aw'/29, 'ax'/30, 'ay'/31, 'az'/32, 'ba'/33, 'bb'/34, 'bc'/35, 'bd'/36, 'be'/37, 'bf'/38, 'bg'/39, 'bh'/40] ,
	ordered : yes ] .
xtype [	name : xtype_6 ,
	base : symbolic ,
	domain : ['f'/1, 'g'/2, 'h'/3, 'i'/4, 'j'/5, 'k'/6, 'l'/7, 'm'/8, 'n'/9, 'o'/10, 'p'/11, 'q'/12, 'r'/13, 's'/14, 't'/15, 'u'/16, 'v'/17, 'w'/18, 'x'/19, 'y'/20, 'z'/21, 'aa'/22, 'ab'/23, 'ac'/24, 'ad'/25, 'ae'/26, 'af'/27, 'ag'/28, 'ah'/29, 'ai'/30, 'aj'/31, 'ak'/32, 'al'/33, 'am'/34, 'an'/35, 'ao'/36, 'ap'/37, 'aq'/38, 'ar'/39, 'as'/40] ,
	ordered : yes ] .
xtype [	name : xtype_7 ,
	base : symbolic ,
	domain : ['l'/1, 'm'/2, 'n'/3, 'o'/4, 'p'/5, 'q'/6, 'r'/7, 's'/8, 't'/9, 'u'/10, 'v'/11, 'w'/12, 'x'/13, 'y'/14, 'z'/15, 'aa'/16, 'ab'/17, 'ac'/18, 'ad'/19, 'ae'/20, 'af'/21, 'ag'/22, 'ah'/23, 'ai'/24, 'aj'/25, 'ak'/26, 'al'/27, 'am'/28, 'an'/29, 'ao'/30, 'ap'/31, 'aq'/32, 'ar'/33, 'as'/34, 'at'/35, 'au'/36, 'av'/37, 'aw'/38, 'ax'/39, 'ay'/40] ,
	ordered : yes ] .
xtype [	name : xtype_8 ,
	base : symbolic ,
	domain : ['h'/1, 'i'/2, 'j'/3, 'k'/4, 'l'/5, 'm'/6, 'n'/7, 'o'/8, 'p'/9, 'q'/10, 'r'/11, 's'/12, 't'/13, 'u'/14, 'v'/15, 'w'/16, 'x'/17, 'y'/18, 'z'/19, 'aa'/20, 'ab'/21, 'ac'/22, 'ad'/23, 'ae'/24, 'af'/25, 'ag'/26, 'ah'/27, 'ai'/28, 'aj'/29, 'ak'/30, 'al'/31, 'am'/32, 'an'/33, 'ao'/34, 'ap'/35, 'aq'/36, 'ar'/37, 'as'/38, 'at'/39, 'au'/40] ,
	ordered : yes ] .
xtype [	name : xtype_9 ,
	base : symbolic ,
	domain : ['d'/1, 'e'/2, 'f'/3, 'g'/4, 'h'/5, 'i'/6, 'j'/7, 'k'/8, 'l'/9, 'm'/10, 'n'/11, 'o'/12, 'p'/13, 'q'/14, 'r'/15, 's'/16, 't'/17, 'u'/18, 'v'/19, 'w'/20, 'x'/21, 'y'/22, 'z'/23, 'aa'/24, 'ab'/25, 'ac'/26, 'ad'/27, 'ae'/28, 'af'/29, 'ag'/30, 'ah'/31, 'ai'/32, 'aj'/33, 'ak'/34, 'al'/35, 'am'/36, 'an'/37, 'ao'/38, 'ap'/39, 'aq'/40] ,
	ordered : yes ] .
xtype [	name : xtype_10 ,
	base : symbolic ,
	domain : ['b'/1, 'c'/2, 'd'/3, 'e'/4, 'f'/5, 'g'/6, 'h'/7, 'i'/8, 'j'/9, 'k'/10, 'l'/11, 'm'/12, 'n'/13, 'o'/14, 'p'/15, 'q'/16, 'r'/17, 's'/18, 't'/19, 'u'/20, 'v'/21, 'w'/22, 'x'/23, 'y'/24, 'z'/25, 'aa'/26, 'ab'/27, 'ac'/28, 'ad'/29, 'ae'/30, 'af'/31, 'ag'/32, 'ah'/33, 'ai'/34, 'aj'/35, 'ak'/36, 'al'/37, 'am'/38, 'an'/39, 'ao'/40] ,
	ordered : yes ] .
xtype [	name : xtype_11 ,
	base : symbolic ,
	domain : ['v'/1, 'w'/2, 'x'/3, 'y'/4, 'z'/5, 'aa'/6, 'ab'/7, 'ac'/8, 'ad'/9, 'ae'/10, 'af'/11, 'ag'/12, 'ah'/13, 'ai'/14, 'aj'/15, 'ak'/16, 'al'/17, 'am'/18, 'an'/19, 'ao'/20, 'ap'/21, 'aq'/22, 'ar'/23, 'as'/24, 'at'/25, 'au'/26, 'av'/27, 'aw'/28, 'ax'/29, 'ay'/30, 'az'/31, 'ba'/32, 'bb'/33, 'bc'/34, 'bd'/35, 'be'/36, 'bf'/37, 'bg'/38, 'bh'/39, 'bi'/40] ,
	ordered : yes ] .
xtype [	name : xtype_12 ,
	base : symbolic ,
	domain : ['s'/1, 't'/2, 'u'/3, 'v'/4, 'w'/5, 'x'/6, 'y'/7, 'z'/8, 'aa'/9, 'ab'/10, 'ac'/11, 'ad'/12, 'ae'/13, 'af'/14, 'ag'/15, 'ah'/16, 'ai'/17, 'aj'/18, 'ak'/19, 'al'/20, 'am'/21, 'an'/22, 'ao'/23, 'ap'/24, 'aq'/25, 'ar'/26, 'as'/27, 'at'/28, 'au'/29, 'av'/30, 'aw'/31, 'ax'/32, 'ay'/33, 'az'/34, 'ba'/35, 'bb'/36, 'bc'/37, 'bd'/38, 'be'/39, 'bf'/40] ,
	ordered : yes ] .
xtype [	name : xtype_13 ,
	base : symbolic ,
	domain : ['f'/1, 'g'/2, 'h'/3, 'i'/4, 'j'/5, 'k'/6, 'l'/7, 'm'/8, 'n'/9, 'o'/10, 'p'/11, 'q'/12, 'r'/13, 's'/14, 't'/15, 'u'/16, 'v'/17, 'w'/18, 'x'/19, 'y'/20, 'z'/21, 'aa'/22, 'ab'/23, 'ac'/24, 'ad'/25, 'ae'/26, 'af'/27, 'ag'/28, 'ah'/29, 'ai'/30, 'aj'/31, 'ak'/32, 'al'/33, 'am'/34, 'an'/35, 'ao'/36, 'ap'/37, 'aq'/38, 'ar'/39, 'as'/40] ,
	ordered : yes ] .
xtype [	name : xtype_14 ,
	base : symbolic ,
	domain : ['c'/1, 'd'/2, 'e'/3, 'f'/4, 'g'/5, 'h'/6, 'i'/7, 'j'/8, 'k'/9, 'l'/10, 'm'/11, 'n'/12, 'o'/13, 'p'/14, 'q'/15, 'r'/16, 's'/17, 't'/18, 'u'/19, 'v'/20, 'w'/21, 'x'/22, 'y'/23, 'z'/24, 'aa'/25, 'ab'/26, 'ac'/27, 'ad'/28, 'ae'/29, 'af'/30, 'ag'/31, 'ah'/32, 'ai'/33, 'aj'/34, 'ak'/35, 'al'/36, 'am'/37, 'an'/38, 'ao'/39, 'ap'/40] ,
	ordered : yes ] .
xtype [	name : xtype_15 ,
	base : symbolic ,
	domain : ['w'/1, 'x'/2, 'y'/3, 'z'/4, 'aa'/5, 'ab'/6, 'ac'/7, 'ad'/8, 'ae'/9, 'af'/10, 'ag'/11, 'ah'/12, 'ai'/13, 'aj'/14, 'ak'/15, 'al'/16, 'am'/17, 'an'/18, 'ao'/19, 'ap'/20, 'aq'/21, 'ar'/22, 'as'/23, 'at'/24, 'au'/25, 'av'/26, 'aw'/27, 'ax'/28, 'ay'/29, 'az'/30, 'ba'/31, 'bb'/32, 'bc'/33, 'bd'/34, 'be'/35, 'bf'/36, 'bg'/37, 'bh'/38, 'bi'/39, 'bj'/40] ,
	ordered : yes ] .
xtype [	name : xtype_16 ,
	base : symbolic ,
	domain : ['s'/1, 't'/2, 'u'/3, 'v'/4, 'w'/5, 'x'/6, 'y'/7, 'z'/8, 'aa'/9, 'ab'/10, 'ac'/11, 'ad'/12, 'ae'/13, 'af'/14, 'ag'/15, 'ah'/16, 'ai'/17, 'aj'/18, 'ak'/19, 'al'/20, 'am'/21, 'an'/22, 'ao'/23, 'ap'/24, 'aq'/25, 'ar'/26, 'as'/27, 'at'/28, 'au'/29, 'av'/30, 'aw'/31, 'ax'/32, 'ay'/33, 'az'/34, 'ba'/35, 'bb'/36, 'bc'/37, 'bd'/38, 'be'/39, 'bf'/40] ,
	ordered : yes ] .
xtype [	name : xtype_17 ,
	base : symbolic ,
	domain : ['y'/1, 'z'/2, 'aa'/3, 'ab'/4, 'ac'/5, 'ad'/6, 'ae'/7, 'af'/8, 'ag'/9, 'ah'/10, 'ai'/11, 'aj'/12, 'ak'/13, 'al'/14, 'am'/15, 'an'/16, 'ao'/17, 'ap'/18, 'aq'/19, 'ar'/20, 'as'/21, 'at'/22, 'au'/23, 'av'/24, 'aw'/25, 'ax'/26, 'ay'/27, 'az'/28, 'ba'/29, 'bb'/30, 'bc'/31, 'bd'/32, 'be'/33, 'bf'/34, 'bg'/35, 'bh'/36, 'bi'/37, 'bj'/38, 'bk'/39, 'bl'/40] ,
	ordered : yes ] .
xtype [	name : xtype_18 ,
	base : symbolic ,
	domain : ['n'/1, 'o'/2, 'p'/3, 'q'/4, 'r'/5, 's'/6, 't'/7, 'u'/8, 'v'/9, 'w'/10, 'x'/11, 'y'/12, 'z'/13, 'aa'/14, 'ab'/15, 'ac'/16, 'ad'/17, 'ae'/18, 'af'/19, 'ag'/20, 'ah'/21, 'ai'/22, 'aj'/23, 'ak'/24, 'al'/25, 'am'/26, 'an'/27, 'ao'/28, 'ap'/29, 'aq'/30, 'ar'/31, 'as'/32, 'at'/33, 'au'/34, 'av'/35, 'aw'/36, 'ax'/37, 'ay'/38, 'az'/39, 'ba'/40] ,
	ordered : yes ] .
xtype [	name : xtype_19 ,
	base : symbolic ,
	domain : ['l'/1, 'm'/2, 'n'/3, 'o'/4, 'p'/5, 'q'/6, 'r'/7, 's'/8, 't'/9, 'u'/10, 'v'/11, 'w'/12, 'x'/13, 'y'/14, 'z'/15, 'aa'/16, 'ab'/17, 'ac'/18, 'ad'/19, 'ae'/20, 'af'/21, 'ag'/22, 'ah'/23, 'ai'/24, 'aj'/25, 'ak'/26, 'al'/27, 'am'/28, 'an'/29, 'ao'/30, 'ap'/31, 'aq'/32, 'ar'/33, 'as'/34, 'at'/35, 'au'/36, 'av'/37, 'aw'/38, 'ax'/39, 'ay'/40] ,
	ordered : yes ] .
xattr [	name : xattr_0 ,
	abbrev : xattr_0 ,
	class : simple ,
	type : xtype_4 ] .
xattr [	name : xattr_1 ,
	abbrev : xattr_1 ,
	class : simple ,
	type : xtype_12 ] .
xattr [	name : xattr_2 ,
	abbrev : xattr_2 ,
	class : simple ,
	type : xtype_3 ] .
xattr [	name : xattr_3 ,
	abbrev : xattr_3 ,
	class : simple ,
	type : xtype_0 ] .
xattr [	name : xattr_4 ,
	abbrev : xattr_4 ,
	class : simple ,
	type : xtype_11 ] .
xattr [	name : xattr_5 ,
	abbrev : xattr_5 ,
	class : simple ,
	type : xtype_13 ] .
xattr [	name : xattr_6 ,
	abbrev : xattr_6 ,
	class : simple ,
	type : xtype_12 ] .
xattr [	name : xattr_7 ,
	abbrev : xattr_7 ,
	class : simple ,
	type : xtype_8 ] .
xattr [	name : xattr_8 ,
	abbrev : xattr_8 ,
	class : simple ,
	type : xtype_13 ] .
xattr [	name : xattr_9 ,
	abbrev : xattr_9 ,
	class : simple ,
	type : xtype_6 ] .
xattr [	name : xattr_10 ,
	abbrev : xattr_10 ,
	class : simple ,
	type : xtype_17 ] .
xattr [	name : xattr_11 ,
	abbrev : xattr_11 ,
	class : simple ,
	type : xtype_6 ] .
xattr [	name : xattr_12 ,
	abbrev : xattr_12 ,
	class : simple ,
	type : xtype_7 ] .
xattr [	name : xattr_13 ,
	abbrev : xattr_13 ,
	class : simple ,
	type : xtype_13 ] .
xattr [	name : xattr_14 ,
	abbrev : xattr_14 ,
	class : simple ,
	type : xtype_10 ] .
xattr [	name : xattr_15 ,
	abbrev : xattr_15 ,
	class : simple ,
	type : xtype_0 ] .
xattr [	name : xattr_16 ,
	abbrev : xattr_16 ,
	class : simple ,
	type : xtype_3 ] .
xattr [	name : xattr_17 ,
	abbrev : xattr_17 ,
	class : simple ,
	type : xtype_0 ] .
xattr [	name : xattr_18 ,
	abbrev : xattr_18 ,
	class : simple ,
	type : xtype_9 ] .
xattr [	name : xattr_19 ,
	abbrev : xattr_19 ,
	class : simple ,
	type : xtype_18 ] .
xattr [	name : xattr_20 ,
	abbrev : xattr_20 ,
	class : simple ,
	type : xtype_10 ] .
xattr [	name : xattr_21 ,
	abbrev : xattr_21 ,
	class : simple ,
	type : xtype_8 ] .
xattr [	name : xattr_22 ,
	abbrev : xattr_22 ,
	class : simple ,
	type : xtype_10 ] .
xattr [	name : xattr_23 ,
	abbrev : xattr_23 ,
	class : simple ,
	type : xtype_12 ] .
xattr [	name : xattr_24 ,
	abbrev : xattr_24 ,
	class : simple ,
	type : xtype_3 ] .
xattr [	name : xattr_25 ,
	abbrev : xattr_25 ,
	class : simple ,
	type : xtype_6 ] .
xattr [	name : xattr_26 ,
	abbrev : xattr_26 ,
	class : simple ,
	type : xtype_4 ] .
xattr [	name : xattr_27 ,
	abbrev : xattr_27 ,
	class : simple ,
	type : xtype_9 ] .
xattr [	name : xattr_28 ,
	abbrev : xattr_28 ,
	class : simple ,
	type : xtype_4 ] .
xattr [	name : xattr_29 ,
	abbrev : xattr_29 ,
	class : simple ,
	type : xtype_3 ] .
xattr [	name : xattr_30 ,
	abbrev : xattr_30 ,
	class : simple ,
	type : xtype_11 ] .
xattr [	name : xattr_31 ,
	abbrev : xattr_31 ,
	class : simple ,
	type : xtype_12 ] .
xattr [	name : xattr_32 ,
	abbrev : xattr_32 ,
	class : simple ,
	type : xtype_7 ] .
xattr [	name : xattr_33 ,
	abbrev : xattr_33 ,
	class : simple ,
	type : xtype_8 ] .
xattr [	name : xattr_34 ,
	abbrev : xattr_34 ,
	class : simple ,
	type : xtype_14 ] .
xattr [	name : xattr_35 ,
	abbrev : xattr_35 ,
	class : simple ,
	type : xtype_6 ] .
xattr [	name : xattr_36 ,
	abbrev : xattr_36 ,
	class : simple ,
	type : xtype_2 ] .
xattr [	name : xattr_37 ,
	abbrev : xattr_37 ,
	class : simple ,
	type : xtype_15 ] .
xattr [	name : xattr_38 ,
	abbrev : xattr_38 ,
	class : simple ,
	type : xtype_0 ] .
xattr [	name : xattr_39 ,
	abbrev : xattr_39 ,
	class : simple ,
	type : xtype_8 ] .
xattr [	name : xattr_40 ,
	abbrev : xattr_40 ,
	class : simple ,
	type : xtype_3 ] .
xattr [	name : xattr_41 ,
	abbrev : xattr_41 ,
	class : simple ,
	type : xtype_14 ] .
xattr [	name : xattr_42 ,
	abbrev : xattr_42 ,
	class : simple ,
	type : xtype_19 ] .
xattr [	name : xattr_43 ,
	abbrev : xattr_43 ,
	class : simple ,
	type : xtype_10 ] .
xattr [	name : xattr_44 ,
	abbrev : xattr_44 ,
	class : simple ,
	type : xtype_2 ] .
xattr [	name : xattr_45 ,
	abbrev : xattr_45 ,
	class : simple ,
	type : xtype_2 ] .
xattr [	name : xattr_46 ,
	abbrev : xattr_46 ,
	class : simple ,
	type : xtype_3 ] .
xattr [	name : xattr_47 ,
	abbrev : xattr_47 ,
	class : simple ,
	type : xtype_5 ] .
xattr [	name : xattr_48 ,
	abbrev : xattr_48 ,
	class : simple ,
	type : xtype_8 ] .
xattr [	name : xattr_49 ,
	abbrev : xattr_49 ,
	class : simple ,
	type : xtype_7 ] .
xattr [	name : xattr_50 ,
	abbrev : xattr_50 ,
	class : simple ,
	type : xtype_5 ] .
xattr [	name : xattr_51 ,
	abbrev : xattr_51 ,
	class : simple ,
	type : xtype_18 ] .
xattr [	name : xattr_52 ,
	abbrev : xattr_52 ,
	class : simple ,
	type : xtype_18 ] .
xattr [	name : xattr_53 ,
	abbrev : xattr_53 ,
	class : simple ,
	type : xtype_3 ] .
xattr [	name : xattr_54 ,
	abbrev : xattr_54 ,
	class : simple ,
	type : xtype_7 ] .
xattr [	name : xattr_55 ,
	abbrev : xattr_55 ,
	class : simple ,
	type : xtype_17 ] .
xattr [	name : xattr_56 ,
	abbrev : xattr_56 ,
	class : simple ,
	type : xtype_0 ] .
xattr [	name : xattr_57 ,
	abbrev : xattr_57 ,
	class : simple ,
	type : xtype_16 ] .
xattr [	name : xattr_58 ,
	abbrev : xattr_58 ,
	class : simple ,
	type : xtype_5 ] .
xattr [	name : xattr_59 ,
	abbrev : xattr_59 ,
	class : simple ,
	type : xtype_5 ] .
xattr [	name : xattr_60 ,
	abbrev : xattr_60 ,
	class : simple ,
	type : xtype_8 ] .
xattr [	name : xattr_61 ,
	abbrev : xattr_61 ,
	class : simple ,
	type : xtype_12 ] .
xattr [	name : xattr_62 ,
	abbrev : xattr_62 ,
	class : simple ,
	type : xtype_2 ] .
xattr [	name : xattr_63 ,
	abbrev : xattr_63 ,
	class : simple ,
	type : xtype_1 ] .
xattr [	name : xattr_64 ,
	abbrev : xattr_64 ,
	class : simple ,
	type : xtype_0 ] .
xattr [	name : xattr_65 ,
	abbrev : xattr_65 ,
	class : simple ,
	type : xtype_12 ] .
xattr [	name : xattr_66 ,
	abbrev : xattr_66 ,
	class : simple ,
	type : xtype_9 ] .
xattr [	name : xattr_67 ,
	abbrev : xattr_67 ,
	class : simple ,
	type : xtype_12 ] .
xattr [	name : xattr_68 ,
	abbrev : xattr_68 ,
	class : simple ,
	type : xtype_7 ] .
xattr [	name : xattr_69 ,
	abbrev : xattr_69 ,
	class : simple ,
	type : xtype_13 ] .
xattr [	name : xattr_70 ,
	abbrev : xattr_70 ,
	class : simple ,
	type : xtype_19 ] .
xattr [	name : xattr_71 ,
	abbrev : xattr_71 ,
	class : simple ,
	type : xtype_14 ] .
xattr [	name : xattr_72 ,
	abbrev : xattr_72 ,
	class : simple ,
	type : xtype_8 ] .
xattr [	name : xattr_73 ,
	abbrev : xattr_73 ,
	class : simple ,
	type : xtype_19 ] .
xattr [	name : xattr_74 ,
	abbrev : xattr_74 ,
	class : simple ,
	type : xtype_14 ] .
xattr [	name : xattr_75 ,
	abbrev : xattr_75 ,
	class : simple ,
	type : xtype_12 ] .
xattr [	name : xattr_76 ,
	abbrev : xattr_76 ,
	class : simple ,
	type : xtype_12 ] .
xattr [	name : xattr_77 ,
	abbrev : xattr_77 ,
	class : simple ,
	type : xtype_19 ] .
xattr [	name : xattr_78 ,
	abbrev : xattr_78 ,
	class : simple ,
	type : xtype_13 ] .
xattr [	name : xattr_79 ,
	abbrev : xattr_79 ,
	class : simple ,
	type : xtype_11 ] .
xattr [	name : xattr_80 ,
	abbrev : xattr_80 ,
	class : simple ,
	type : xtype_6 ] .
xattr [	name : xattr_81 ,
	abbrev : xattr_81 ,
	class : simple ,
	type : xtype_5 ] .
xattr [	name : xattr_82 ,
	abbrev : xattr_82 ,
	class : simple ,
	type : xtype_3 ] .
xattr [	name : xattr_83 ,
	abbrev : xattr_83 ,
	class : simple ,
	type : xtype_12 ] .
xattr [	name : xattr_84 ,
	abbrev : xattr_84 ,
	class : simple ,
	type : xtype_1 ] .
xattr [	name : xattr_85 ,
	abbrev : xattr_85 ,
	class : simple ,
	type : xtype_4 ] .
xattr [	name : xattr_86 ,
	abbrev : xattr_86 ,
	class : simple ,
	type : xtype_6 ] .
xattr [	name : xattr_87 ,
	abbrev : xattr_87 ,
	class : simple ,
	type : xtype_3 ] .
xattr [	name : xattr_88 ,
	abbrev : xattr_88 ,
	class : simple ,
	type : xtype_3 ] .
xattr [	name : xattr_89 ,
	abbrev : xattr_89 ,
	class : simple ,
	type : xtype_2 ] .
xattr [	name : xattr_90 ,
	abbrev : xattr_90 ,
	class : simple ,
	type : xtype_15 ] .
xattr [	name : xattr_91 ,
	abbrev : xattr_91 ,
	class : simple ,
	type : xtype_18 ] .
xattr [	name : xattr_92 ,
	abbrev : xattr_92 ,
	class : simple ,
	type : xtype_10 ] .
xattr [	name : xattr_93 ,
	abbrev : xattr_93 ,
	class : simple ,
	type : xtype_11 ] .
xattr [	name : xattr_94 ,
	abbrev : xattr_94 ,
	class : simple ,
	type : xtype_8 ] .
xattr [	name : xattr_95 ,
	abbrev : xattr_95 ,
	class : simple ,
	type : xtype_13 ] .
xattr [	name : xattr_96 ,
	abbrev : xattr_96 ,
	class : simple ,
	type : xtype_15 ] .
xattr [	name : xattr_97 ,
	abbrev : xattr_97 ,
	class : simple ,
	type : xtype_13 ] .
xattr [	name : xattr_98 ,
	abbrev : xattr_98 ,
	class : simple ,
	type : xtype_11 ] .
xattr [	name : xattr_99 ,
	abbrev : xattr_99 ,
	class : simple ,
	type : xtype_0 ] .
xschm xschm_0 :
 [xattr_0, xattr_1, xattr_2, xattr_3] ==> [xattr_4, xattr_5, xattr_6, xattr_7].
xschm xschm_1 :
 [xattr_4, xattr_5, xattr_6, xattr_7] ==> [xattr_8, xattr_9, xattr_10].
xschm xschm_2 :
 [xattr_8, xattr_9, xattr_10] ==> [xattr_11, xattr_12, xattr_13, xattr_14].
xschm xschm_3 :
 [xattr_11, xattr_12, xattr_13, xattr_14] ==> [xattr_15, xattr_16, xattr_17, xattr_18].
xschm xschm_4 :
 [xattr_15, xattr_16, xattr_17, xattr_18] ==> [xattr_19, xattr_20, xattr_21, xattr_22].
xschm xschm_5 :
 [xattr_19, xattr_20, xattr_21, xattr_22] ==> [xattr_23, xattr_24, xattr_25].
xschm xschm_6 :
 [xattr_23, xattr_24, xattr_25] ==> [xattr_26, xattr_27, xattr_28].
xschm xschm_7 :
 [xattr_26, xattr_27, xattr_28] ==> [xattr_29, xattr_30, xattr_31, xattr_32].
xschm xschm_8 :
 [xattr_29, xattr_30, xattr_31, xattr_32] ==> [xattr_33, xattr_34, xattr_35].
xschm xschm_9 :
 [xattr_33, xattr_34, xattr_35] ==> [xattr_36, xattr_37].
xschm xschm_10 :
 [xattr_36, xattr_37] ==> [xattr_38, xattr_39].
xschm xschm_11 :
 [xattr_38, xattr_39] ==> [xattr_40, xattr_41, xattr_42].
xschm xschm_12 :
 [xattr_40, xattr_41, xattr_42] ==> [xattr_43, xattr_44, xattr_45, xattr_46].
xschm xschm_13 :
 [xattr_43, xattr_44, xattr_45, xattr_46] ==> [xattr_47, xattr_48].
xschm xschm_14 :
 [xattr_47, xattr_48] ==> [xattr_49, xattr_50, xattr_51].
xschm xschm_15 :
 [xattr_49, xattr_50, xattr_51] ==> [xattr_52, xattr_53].
xschm xschm_16 :
 [xattr_52, xattr_53] ==> [xattr_54, xattr_55, xattr_56].
xschm xschm_17 :
 [xattr_54, xattr_55, xattr_56] ==> [xattr_57, xattr_58, xattr_59].
xschm xschm_18 :
 [xattr_57, xattr_58, xattr_59] ==> [xattr_60, xattr_61].
xschm xschm_19 :
 [xattr_60, xattr_61] ==> [xattr_62, xattr_63].
xschm xschm_20 :
 [xattr_62, xattr_63] ==> [xattr_64, xattr_65, xattr_66].
xschm xschm_21 :
 [xattr_64, xattr_65, xattr_66] ==> [xattr_67, xattr_68, xattr_69].
xschm xschm_22 :
 [xattr_67, xattr_68, xattr_69] ==> [xattr_70, xattr_71, xattr_72].
xschm xschm_23 :
 [xattr_70, xattr_71, xattr_72] ==> [xattr_73, xattr_74, xattr_75, xattr_76].
xschm xschm_24 :
 [xattr_73, xattr_74, xattr_75, xattr_76] ==> [xattr_77, xattr_78, xattr_79].
xschm xschm_25 :
 [xattr_77, xattr_78, xattr_79] ==> [xattr_80, xattr_81, xattr_82, xattr_83].
xschm xschm_26 :
 [xattr_80, xattr_81, xattr_82, xattr_83] ==> [xattr_84, xattr_85, xattr_86].
xschm xschm_27 :
 [xattr_84, xattr_85, xattr_86] ==> [xattr_87, xattr_88, xattr_89].
xschm xschm_28 :
 [xattr_87, xattr_88, xattr_89] ==> [xattr_90, xattr_91, xattr_92].
xschm xschm_29 :
 [xattr_90, xattr_91, xattr_92] ==> [xattr_93, xattr_94].
xschm xschm_30 :
 [xattr_93, xattr_94] ==> [xattr_95, xattr_96].
xschm xschm_31 :
 [xattr_95, xattr_96] ==> [xattr_97, xattr_98].
xschm xschm_32 :
 [xattr_97, xattr_98] ==> [xattr_99].
xrule xschm_0/0 :
[
xattr_0 in ['x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_1 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_2 in ['m', 'n', 'o', 'p', 'q'] , 
xattr_3 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb'] ]
==>
[
xattr_4 set 'av' , 
xattr_5 set 'ab' , 
xattr_6 set 'w' , 
xattr_7 set 'p' ].

xrule xschm_0/1 :
[
xattr_0 in ['x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_1 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_2 in ['m', 'n', 'o', 'p', 'q'] , 
xattr_3 in ['bc', 'bd', 'be'] ]
==>
[
xattr_4 set 'x' , 
xattr_5 set 'h' , 
xattr_6 set 'ay' , 
xattr_7 set 'k' ].

xrule xschm_0/2 :
[
xattr_0 in ['x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_1 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_2 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_3 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb'] ]
==>
[
xattr_4 set 'v' , 
xattr_5 set 'ad' , 
xattr_6 set 'aw' , 
xattr_7 set 'ak' ].

xrule xschm_0/3 :
[
xattr_0 in ['x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_1 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_2 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_3 in ['bc', 'bd', 'be'] ]
==>
[
xattr_4 set 'bi' , 
xattr_5 set 'l' , 
xattr_6 set 'aa' , 
xattr_7 set 'x' ].

xrule xschm_0/4 :
[
xattr_0 in ['x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_1 in ['av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_2 in ['m', 'n', 'o', 'p', 'q'] , 
xattr_3 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb'] ]
==>
[
xattr_4 set 'ar' , 
xattr_5 set 'q' , 
xattr_6 set 'v' , 
xattr_7 set 'q' ].

xrule xschm_0/5 :
[
xattr_0 in ['x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_1 in ['av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_2 in ['m', 'n', 'o', 'p', 'q'] , 
xattr_3 in ['bc', 'bd', 'be'] ]
==>
[
xattr_4 set 'bf' , 
xattr_5 set 'u' , 
xattr_6 set 'al' , 
xattr_7 set 's' ].

xrule xschm_0/6 :
[
xattr_0 in ['x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_1 in ['av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_2 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_3 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb'] ]
==>
[
xattr_4 set 'aq' , 
xattr_5 set 'i' , 
xattr_6 set 'ai' , 
xattr_7 set 'ak' ].

xrule xschm_0/7 :
[
xattr_0 in ['x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_1 in ['av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_2 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_3 in ['bc', 'bd', 'be'] ]
==>
[
xattr_4 set 'ay' , 
xattr_5 set 'an' , 
xattr_6 set 'ap' , 
xattr_7 set 'ar' ].

xrule xschm_0/8 :
[
xattr_0 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_1 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_2 in ['m', 'n', 'o', 'p', 'q'] , 
xattr_3 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb'] ]
==>
[
xattr_4 set 'av' , 
xattr_5 set 'ao' , 
xattr_6 set 'ad' , 
xattr_7 set 'al' ].

xrule xschm_0/9 :
[
xattr_0 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_1 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_2 in ['m', 'n', 'o', 'p', 'q'] , 
xattr_3 in ['bc', 'bd', 'be'] ]
==>
[
xattr_4 set 'az' , 
xattr_5 set 'x' , 
xattr_6 set 'ba' , 
xattr_7 set 'as' ].

xrule xschm_0/10 :
[
xattr_0 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_1 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_2 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_3 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb'] ]
==>
[
xattr_4 set 'bc' , 
xattr_5 set 'x' , 
xattr_6 set 'am' , 
xattr_7 set 'ai' ].

xrule xschm_0/11 :
[
xattr_0 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_1 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_2 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_3 in ['bc', 'bd', 'be'] ]
==>
[
xattr_4 set 'aq' , 
xattr_5 set 'an' , 
xattr_6 set 'ag' , 
xattr_7 set 'h' ].

xrule xschm_0/12 :
[
xattr_0 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_1 in ['av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_2 in ['m', 'n', 'o', 'p', 'q'] , 
xattr_3 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb'] ]
==>
[
xattr_4 set 'ao' , 
xattr_5 set 'ae' , 
xattr_6 set 'af' , 
xattr_7 set 'n' ].

xrule xschm_0/13 :
[
xattr_0 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_1 in ['av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_2 in ['m', 'n', 'o', 'p', 'q'] , 
xattr_3 in ['bc', 'bd', 'be'] ]
==>
[
xattr_4 set 'y' , 
xattr_5 set 'ac' , 
xattr_6 set 'be' , 
xattr_7 set 'ak' ].

xrule xschm_0/14 :
[
xattr_0 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_1 in ['av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_2 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_3 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb'] ]
==>
[
xattr_4 set 'at' , 
xattr_5 set 'ae' , 
xattr_6 set 'ba' , 
xattr_7 set 'p' ].

xrule xschm_0/15 :
[
xattr_0 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_1 in ['av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_2 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_3 in ['bc', 'bd', 'be'] ]
==>
[
xattr_4 set 'bd' , 
xattr_5 set 'ad' , 
xattr_6 set 'ax' , 
xattr_7 set 'ap' ].

xrule xschm_0/16 :
[
xattr_0 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk'] , 
xattr_1 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_2 in ['m', 'n', 'o', 'p', 'q'] , 
xattr_3 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb'] ]
==>
[
xattr_4 set 'bg' , 
xattr_5 set 'u' , 
xattr_6 set 'ak' , 
xattr_7 set 'v' ].

xrule xschm_0/17 :
[
xattr_0 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk'] , 
xattr_1 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_2 in ['m', 'n', 'o', 'p', 'q'] , 
xattr_3 in ['bc', 'bd', 'be'] ]
==>
[
xattr_4 set 'ba' , 
xattr_5 set 'n' , 
xattr_6 set 'ah' , 
xattr_7 set 'k' ].

xrule xschm_0/18 :
[
xattr_0 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk'] , 
xattr_1 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_2 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_3 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb'] ]
==>
[
xattr_4 set 'ac' , 
xattr_5 set 'p' , 
xattr_6 set 'ad' , 
xattr_7 set 'an' ].

xrule xschm_0/19 :
[
xattr_0 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk'] , 
xattr_1 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_2 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_3 in ['bc', 'bd', 'be'] ]
==>
[
xattr_4 set 'bc' , 
xattr_5 set 'v' , 
xattr_6 set 'az' , 
xattr_7 set 'y' ].

xrule xschm_0/20 :
[
xattr_0 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk'] , 
xattr_1 in ['av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_2 in ['m', 'n', 'o', 'p', 'q'] , 
xattr_3 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb'] ]
==>
[
xattr_4 set 'aj' , 
xattr_5 set 'ae' , 
xattr_6 set 'ab' , 
xattr_7 set 'v' ].

xrule xschm_0/21 :
[
xattr_0 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk'] , 
xattr_1 in ['av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_2 in ['m', 'n', 'o', 'p', 'q'] , 
xattr_3 in ['bc', 'bd', 'be'] ]
==>
[
xattr_4 set 'at' , 
xattr_5 set 'ak' , 
xattr_6 set 'bf' , 
xattr_7 set 'ar' ].

xrule xschm_0/22 :
[
xattr_0 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk'] , 
xattr_1 in ['av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_2 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_3 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb'] ]
==>
[
xattr_4 set 'x' , 
xattr_5 set 'ar' , 
xattr_6 set 'bf' , 
xattr_7 set 'ai' ].

xrule xschm_0/23 :
[
xattr_0 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk'] , 
xattr_1 in ['av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_2 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_3 in ['bc', 'bd', 'be'] ]
==>
[
xattr_4 set 'aj' , 
xattr_5 set 'j' , 
xattr_6 set 'ao' , 
xattr_7 set 'aj' ].

xrule xschm_1/0 :
[
xattr_4 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_5 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_6 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_7 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] ]
==>
[
xattr_8 set 'w' , 
xattr_9 set 'u' , 
xattr_10 set 'az' ].

xrule xschm_1/1 :
[
xattr_4 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_5 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_6 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_7 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae'] ]
==>
[
xattr_8 set 'r' , 
xattr_9 set 'w' , 
xattr_10 set 'an' ].

xrule xschm_1/2 :
[
xattr_4 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_5 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_6 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_7 in ['af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] ]
==>
[
xattr_8 set 'k' , 
xattr_9 set 'o' , 
xattr_10 set 'bc' ].

xrule xschm_1/3 :
[
xattr_4 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_5 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_6 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_7 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] ]
==>
[
xattr_8 set 'p' , 
xattr_9 set 'ar' , 
xattr_10 set 'av' ].

xrule xschm_1/4 :
[
xattr_4 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_5 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_6 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_7 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae'] ]
==>
[
xattr_8 set 'l' , 
xattr_9 set 'aq' , 
xattr_10 set 'al' ].

xrule xschm_1/5 :
[
xattr_4 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_5 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_6 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_7 in ['af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] ]
==>
[
xattr_8 set 'f' , 
xattr_9 set 'q' , 
xattr_10 set 'am' ].

xrule xschm_1/6 :
[
xattr_4 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_5 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_6 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_7 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] ]
==>
[
xattr_8 set 'h' , 
xattr_9 set 't' , 
xattr_10 set 'af' ].

xrule xschm_1/7 :
[
xattr_4 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_5 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_6 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_7 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae'] ]
==>
[
xattr_8 set 'i' , 
xattr_9 set 'h' , 
xattr_10 set 'ae' ].

xrule xschm_1/8 :
[
xattr_4 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_5 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_6 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_7 in ['af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] ]
==>
[
xattr_8 set 'al' , 
xattr_9 set 't' , 
xattr_10 set 'ay' ].

xrule xschm_1/9 :
[
xattr_4 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_5 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_6 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_7 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] ]
==>
[
xattr_8 set 'ao' , 
xattr_9 set 'l' , 
xattr_10 set 'ao' ].

xrule xschm_1/10 :
[
xattr_4 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_5 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_6 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_7 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae'] ]
==>
[
xattr_8 set 'as' , 
xattr_9 set 'ah' , 
xattr_10 set 'ao' ].

xrule xschm_1/11 :
[
xattr_4 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_5 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_6 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_7 in ['af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] ]
==>
[
xattr_8 set 'ar' , 
xattr_9 set 'r' , 
xattr_10 set 'bi' ].

xrule xschm_1/12 :
[
xattr_4 in ['av', 'aw', 'ax', 'ay', 'az', 'ba'] , 
xattr_5 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_6 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_7 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] ]
==>
[
xattr_8 set 'm' , 
xattr_9 set 's' , 
xattr_10 set 'bc' ].

xrule xschm_1/13 :
[
xattr_4 in ['av', 'aw', 'ax', 'ay', 'az', 'ba'] , 
xattr_5 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_6 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_7 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae'] ]
==>
[
xattr_8 set 'q' , 
xattr_9 set 'i' , 
xattr_10 set 'bk' ].

xrule xschm_1/14 :
[
xattr_4 in ['av', 'aw', 'ax', 'ay', 'az', 'ba'] , 
xattr_5 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_6 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_7 in ['af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] ]
==>
[
xattr_8 set 'h' , 
xattr_9 set 'aa' , 
xattr_10 set 'ap' ].

xrule xschm_1/15 :
[
xattr_4 in ['av', 'aw', 'ax', 'ay', 'az', 'ba'] , 
xattr_5 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_6 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_7 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] ]
==>
[
xattr_8 set 'aq' , 
xattr_9 set 'ar' , 
xattr_10 set 'an' ].

xrule xschm_1/16 :
[
xattr_4 in ['av', 'aw', 'ax', 'ay', 'az', 'ba'] , 
xattr_5 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_6 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_7 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae'] ]
==>
[
xattr_8 set 'aj' , 
xattr_9 set 'z' , 
xattr_10 set 'as' ].

xrule xschm_1/17 :
[
xattr_4 in ['av', 'aw', 'ax', 'ay', 'az', 'ba'] , 
xattr_5 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_6 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_7 in ['af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] ]
==>
[
xattr_8 set 'an' , 
xattr_9 set 'aq' , 
xattr_10 set 'af' ].

xrule xschm_1/18 :
[
xattr_4 in ['av', 'aw', 'ax', 'ay', 'az', 'ba'] , 
xattr_5 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_6 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_7 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] ]
==>
[
xattr_8 set 'h' , 
xattr_9 set 't' , 
xattr_10 set 'ak' ].

xrule xschm_1/19 :
[
xattr_4 in ['av', 'aw', 'ax', 'ay', 'az', 'ba'] , 
xattr_5 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_6 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_7 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae'] ]
==>
[
xattr_8 set 'j' , 
xattr_9 set 'ak' , 
xattr_10 set 'as' ].

xrule xschm_1/20 :
[
xattr_4 in ['av', 'aw', 'ax', 'ay', 'az', 'ba'] , 
xattr_5 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_6 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_7 in ['af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] ]
==>
[
xattr_8 set 'aq' , 
xattr_9 set 'w' , 
xattr_10 set 'be' ].

xrule xschm_1/21 :
[
xattr_4 in ['av', 'aw', 'ax', 'ay', 'az', 'ba'] , 
xattr_5 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_6 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_7 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] ]
==>
[
xattr_8 set 'al' , 
xattr_9 set 'v' , 
xattr_10 set 'z' ].

xrule xschm_1/22 :
[
xattr_4 in ['av', 'aw', 'ax', 'ay', 'az', 'ba'] , 
xattr_5 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_6 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_7 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae'] ]
==>
[
xattr_8 set 'n' , 
xattr_9 set 'w' , 
xattr_10 set 'au' ].

xrule xschm_1/23 :
[
xattr_4 in ['av', 'aw', 'ax', 'ay', 'az', 'ba'] , 
xattr_5 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_6 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_7 in ['af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] ]
==>
[
xattr_8 set 'j' , 
xattr_9 set 'ak' , 
xattr_10 set 'ae' ].

xrule xschm_1/24 :
[
xattr_4 in ['bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi'] , 
xattr_5 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_6 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_7 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] ]
==>
[
xattr_8 set 'ad' , 
xattr_9 set 'an' , 
xattr_10 set 'ap' ].

xrule xschm_1/25 :
[
xattr_4 in ['bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi'] , 
xattr_5 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_6 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_7 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae'] ]
==>
[
xattr_8 set 'ar' , 
xattr_9 set 'aj' , 
xattr_10 set 'ak' ].

xrule xschm_1/26 :
[
xattr_4 in ['bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi'] , 
xattr_5 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_6 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_7 in ['af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] ]
==>
[
xattr_8 set 'm' , 
xattr_9 set 'ap' , 
xattr_10 set 'az' ].

xrule xschm_1/27 :
[
xattr_4 in ['bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi'] , 
xattr_5 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_6 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_7 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] ]
==>
[
xattr_8 set 'ad' , 
xattr_9 set 'q' , 
xattr_10 set 'ae' ].

xrule xschm_1/28 :
[
xattr_4 in ['bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi'] , 
xattr_5 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_6 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_7 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae'] ]
==>
[
xattr_8 set 'ai' , 
xattr_9 set 'as' , 
xattr_10 set 'ba' ].

xrule xschm_1/29 :
[
xattr_4 in ['bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi'] , 
xattr_5 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_6 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_7 in ['af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] ]
==>
[
xattr_8 set 'f' , 
xattr_9 set 'i' , 
xattr_10 set 'ba' ].

xrule xschm_1/30 :
[
xattr_4 in ['bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi'] , 
xattr_5 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_6 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_7 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] ]
==>
[
xattr_8 set 'x' , 
xattr_9 set 'an' , 
xattr_10 set 'y' ].

xrule xschm_1/31 :
[
xattr_4 in ['bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi'] , 
xattr_5 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_6 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_7 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae'] ]
==>
[
xattr_8 set 'i' , 
xattr_9 set 'ao' , 
xattr_10 set 'aq' ].

xrule xschm_1/32 :
[
xattr_4 in ['bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi'] , 
xattr_5 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_6 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_7 in ['af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] ]
==>
[
xattr_8 set 'r' , 
xattr_9 set 'y' , 
xattr_10 set 'ak' ].

xrule xschm_1/33 :
[
xattr_4 in ['bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi'] , 
xattr_5 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_6 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_7 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] ]
==>
[
xattr_8 set 'ak' , 
xattr_9 set 'o' , 
xattr_10 set 'bf' ].

xrule xschm_1/34 :
[
xattr_4 in ['bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi'] , 
xattr_5 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_6 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_7 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae'] ]
==>
[
xattr_8 set 'y' , 
xattr_9 set 'ah' , 
xattr_10 set 'aq' ].

xrule xschm_1/35 :
[
xattr_4 in ['bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi'] , 
xattr_5 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_6 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_7 in ['af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] ]
==>
[
xattr_8 set 'y' , 
xattr_9 set 'w' , 
xattr_10 set 'at' ].

xrule xschm_2/0 :
[
xattr_8 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] , 
xattr_9 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_10 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk', 'bl'] ]
==>
[
xattr_11 set 'f' , 
xattr_12 set 'ap' , 
xattr_13 set 'aq' , 
xattr_14 set 'd' ].

xrule xschm_2/1 :
[
xattr_8 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] , 
xattr_9 in ['ap', 'aq', 'ar', 'as'] , 
xattr_10 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk', 'bl'] ]
==>
[
xattr_11 set 'ab' , 
xattr_12 set 'ao' , 
xattr_13 set 'j' , 
xattr_14 set 'g' ].

xrule xschm_2/2 :
[
xattr_8 in ['an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_9 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_10 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk', 'bl'] ]
==>
[
xattr_11 set 'ah' , 
xattr_12 set 'aa' , 
xattr_13 set 'ap' , 
xattr_14 set 'ah' ].

xrule xschm_2/3 :
[
xattr_8 in ['an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_9 in ['ap', 'aq', 'ar', 'as'] , 
xattr_10 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk', 'bl'] ]
==>
[
xattr_11 set 'an' , 
xattr_12 set 'am' , 
xattr_13 set 'as' , 
xattr_14 set 'ah' ].

xrule xschm_3/0 :
[
xattr_11 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_12 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_13 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_14 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] ]
==>
[
xattr_15 set 't' , 
xattr_16 set 'o' , 
xattr_17 set 'au' , 
xattr_18 set 'd' ].

xrule xschm_3/1 :
[
xattr_11 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_12 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_13 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_14 in ['am', 'an', 'ao'] ]
==>
[
xattr_15 set 'r' , 
xattr_16 set 'as' , 
xattr_17 set 'ac' , 
xattr_18 set 'an' ].

xrule xschm_3/2 :
[
xattr_11 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_12 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_13 eq 'ab' , 
xattr_14 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] ]
==>
[
xattr_15 set 'bc' , 
xattr_16 set 'n' , 
xattr_17 set 'r' , 
xattr_18 set 'am' ].

xrule xschm_3/3 :
[
xattr_11 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_12 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_13 eq 'ab' , 
xattr_14 in ['am', 'an', 'ao'] ]
==>
[
xattr_15 set 'x' , 
xattr_16 set 'ah' , 
xattr_17 set 'am' , 
xattr_18 set 'k' ].

xrule xschm_3/4 :
[
xattr_11 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_12 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_13 in ['ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_14 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] ]
==>
[
xattr_15 set 'ak' , 
xattr_16 set 'aj' , 
xattr_17 set 'v' , 
xattr_18 set 'ao' ].

xrule xschm_3/5 :
[
xattr_11 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_12 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_13 in ['ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_14 in ['am', 'an', 'ao'] ]
==>
[
xattr_15 set 'bb' , 
xattr_16 set 'as' , 
xattr_17 set 'aa' , 
xattr_18 set 'ae' ].

xrule xschm_3/6 :
[
xattr_11 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_12 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_13 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_14 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] ]
==>
[
xattr_15 set 'av' , 
xattr_16 set 'w' , 
xattr_17 set 'an' , 
xattr_18 set 'g' ].

xrule xschm_3/7 :
[
xattr_11 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_12 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_13 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_14 in ['am', 'an', 'ao'] ]
==>
[
xattr_15 set 'ak' , 
xattr_16 set 'ag' , 
xattr_17 set 'ak' , 
xattr_18 set 'ae' ].

xrule xschm_3/8 :
[
xattr_11 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_12 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_13 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_14 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] ]
==>
[
xattr_15 set 'ap' , 
xattr_16 set 'at' , 
xattr_17 set 'ab' , 
xattr_18 set 'i' ].

xrule xschm_3/9 :
[
xattr_11 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_12 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_13 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_14 in ['am', 'an', 'ao'] ]
==>
[
xattr_15 set 'y' , 
xattr_16 set 'ax' , 
xattr_17 set 'aq' , 
xattr_18 set 'al' ].

xrule xschm_3/10 :
[
xattr_11 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_12 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_13 eq 'ab' , 
xattr_14 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] ]
==>
[
xattr_15 set 'ae' , 
xattr_16 set 'ay' , 
xattr_17 set 'ap' , 
xattr_18 set 'ap' ].

xrule xschm_3/11 :
[
xattr_11 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_12 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_13 eq 'ab' , 
xattr_14 in ['am', 'an', 'ao'] ]
==>
[
xattr_15 set 'an' , 
xattr_16 set 'w' , 
xattr_17 set 't' , 
xattr_18 set 't' ].

xrule xschm_3/12 :
[
xattr_11 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_12 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_13 in ['ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_14 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] ]
==>
[
xattr_15 set 'bb' , 
xattr_16 set 'an' , 
xattr_17 set 'r' , 
xattr_18 set 'ak' ].

xrule xschm_3/13 :
[
xattr_11 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_12 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_13 in ['ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_14 in ['am', 'an', 'ao'] ]
==>
[
xattr_15 set 'y' , 
xattr_16 set 'ao' , 
xattr_17 set 'ay' , 
xattr_18 set 'r' ].

xrule xschm_3/14 :
[
xattr_11 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_12 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_13 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_14 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] ]
==>
[
xattr_15 set 'bd' , 
xattr_16 set 'ay' , 
xattr_17 set 'bd' , 
xattr_18 set 'o' ].

xrule xschm_3/15 :
[
xattr_11 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_12 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_13 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_14 in ['am', 'an', 'ao'] ]
==>
[
xattr_15 set 'ak' , 
xattr_16 set 'x' , 
xattr_17 set 'aq' , 
xattr_18 set 'ah' ].

xrule xschm_3/16 :
[
xattr_11 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_12 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_13 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_14 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] ]
==>
[
xattr_15 set 'bc' , 
xattr_16 set 's' , 
xattr_17 set 'bd' , 
xattr_18 set 'ap' ].

xrule xschm_3/17 :
[
xattr_11 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_12 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_13 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_14 in ['am', 'an', 'ao'] ]
==>
[
xattr_15 set 'ag' , 
xattr_16 set 'r' , 
xattr_17 set 't' , 
xattr_18 set 'r' ].

xrule xschm_3/18 :
[
xattr_11 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_12 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_13 eq 'ab' , 
xattr_14 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] ]
==>
[
xattr_15 set 'av' , 
xattr_16 set 'r' , 
xattr_17 set 'ah' , 
xattr_18 set 'e' ].

xrule xschm_3/19 :
[
xattr_11 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_12 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_13 eq 'ab' , 
xattr_14 in ['am', 'an', 'ao'] ]
==>
[
xattr_15 set 'r' , 
xattr_16 set 'as' , 
xattr_17 set 't' , 
xattr_18 set 'g' ].

xrule xschm_3/20 :
[
xattr_11 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_12 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_13 in ['ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_14 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] ]
==>
[
xattr_15 set 'ag' , 
xattr_16 set 'z' , 
xattr_17 set 'am' , 
xattr_18 set 'f' ].

xrule xschm_3/21 :
[
xattr_11 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_12 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_13 in ['ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_14 in ['am', 'an', 'ao'] ]
==>
[
xattr_15 set 'ad' , 
xattr_16 set 'ax' , 
xattr_17 set 'bc' , 
xattr_18 set 'aj' ].

xrule xschm_3/22 :
[
xattr_11 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_12 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_13 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_14 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] ]
==>
[
xattr_15 set 'ai' , 
xattr_16 set 'ax' , 
xattr_17 set 'aw' , 
xattr_18 set 'z' ].

xrule xschm_3/23 :
[
xattr_11 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_12 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_13 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_14 in ['am', 'an', 'ao'] ]
==>
[
xattr_15 set 'aw' , 
xattr_16 set 'z' , 
xattr_17 set 'av' , 
xattr_18 set 'z' ].

xrule xschm_3/24 :
[
xattr_11 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_12 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_13 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_14 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] ]
==>
[
xattr_15 set 'an' , 
xattr_16 set 'aw' , 
xattr_17 set 'at' , 
xattr_18 set 'ao' ].

xrule xschm_3/25 :
[
xattr_11 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_12 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_13 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_14 in ['am', 'an', 'ao'] ]
==>
[
xattr_15 set 'ak' , 
xattr_16 set 'ap' , 
xattr_17 set 'ar' , 
xattr_18 set 'w' ].

xrule xschm_3/26 :
[
xattr_11 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_12 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_13 eq 'ab' , 
xattr_14 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] ]
==>
[
xattr_15 set 'u' , 
xattr_16 set 'o' , 
xattr_17 set 'v' , 
xattr_18 set 'm' ].

xrule xschm_3/27 :
[
xattr_11 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_12 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_13 eq 'ab' , 
xattr_14 in ['am', 'an', 'ao'] ]
==>
[
xattr_15 set 'av' , 
xattr_16 set 'af' , 
xattr_17 set 'ac' , 
xattr_18 set 'y' ].

xrule xschm_3/28 :
[
xattr_11 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_12 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_13 in ['ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_14 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] ]
==>
[
xattr_15 set 'aw' , 
xattr_16 set 'af' , 
xattr_17 set 'an' , 
xattr_18 set 'ag' ].

xrule xschm_3/29 :
[
xattr_11 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_12 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_13 in ['ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_14 in ['am', 'an', 'ao'] ]
==>
[
xattr_15 set 'z' , 
xattr_16 set 'u' , 
xattr_17 set 'as' , 
xattr_18 set 'ap' ].

xrule xschm_3/30 :
[
xattr_11 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_12 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_13 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_14 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] ]
==>
[
xattr_15 set 'ab' , 
xattr_16 set 'ap' , 
xattr_17 set 'x' , 
xattr_18 set 'ai' ].

xrule xschm_3/31 :
[
xattr_11 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_12 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_13 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_14 in ['am', 'an', 'ao'] ]
==>
[
xattr_15 set 'aq' , 
xattr_16 set 'ah' , 
xattr_17 set 'al' , 
xattr_18 set 'k' ].

xrule xschm_3/32 :
[
xattr_11 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_12 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_13 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_14 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] ]
==>
[
xattr_15 set 'x' , 
xattr_16 set 'aa' , 
xattr_17 set 'ar' , 
xattr_18 set 'd' ].

xrule xschm_3/33 :
[
xattr_11 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_12 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_13 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_14 in ['am', 'an', 'ao'] ]
==>
[
xattr_15 set 'ao' , 
xattr_16 set 'as' , 
xattr_17 set 'av' , 
xattr_18 set 'ah' ].

xrule xschm_3/34 :
[
xattr_11 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_12 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_13 eq 'ab' , 
xattr_14 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] ]
==>
[
xattr_15 set 'aa' , 
xattr_16 set 'al' , 
xattr_17 set 'aw' , 
xattr_18 set 'd' ].

xrule xschm_3/35 :
[
xattr_11 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_12 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_13 eq 'ab' , 
xattr_14 in ['am', 'an', 'ao'] ]
==>
[
xattr_15 set 'am' , 
xattr_16 set 'ai' , 
xattr_17 set 'aq' , 
xattr_18 set 'f' ].

xrule xschm_3/36 :
[
xattr_11 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_12 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_13 in ['ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_14 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] ]
==>
[
xattr_15 set 'ay' , 
xattr_16 set 't' , 
xattr_17 set 'ax' , 
xattr_18 set 'h' ].

xrule xschm_3/37 :
[
xattr_11 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_12 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_13 in ['ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_14 in ['am', 'an', 'ao'] ]
==>
[
xattr_15 set 'w' , 
xattr_16 set 'ak' , 
xattr_17 set 'af' , 
xattr_18 set 'am' ].

xrule xschm_3/38 :
[
xattr_11 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_12 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_13 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_14 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] ]
==>
[
xattr_15 set 'am' , 
xattr_16 set 'ac' , 
xattr_17 set 'be' , 
xattr_18 set 'ad' ].

xrule xschm_3/39 :
[
xattr_11 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_12 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_13 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_14 in ['am', 'an', 'ao'] ]
==>
[
xattr_15 set 'aw' , 
xattr_16 set 'm' , 
xattr_17 set 'ai' , 
xattr_18 set 'q' ].

xrule xschm_3/40 :
[
xattr_11 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_12 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_13 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_14 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] ]
==>
[
xattr_15 set 'ar' , 
xattr_16 set 'w' , 
xattr_17 set 'az' , 
xattr_18 set 'ac' ].

xrule xschm_3/41 :
[
xattr_11 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_12 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_13 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_14 in ['am', 'an', 'ao'] ]
==>
[
xattr_15 set 'y' , 
xattr_16 set 'o' , 
xattr_17 set 's' , 
xattr_18 set 'am' ].

xrule xschm_3/42 :
[
xattr_11 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_12 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_13 eq 'ab' , 
xattr_14 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] ]
==>
[
xattr_15 set 'ba' , 
xattr_16 set 'ax' , 
xattr_17 set 'be' , 
xattr_18 set 'h' ].

xrule xschm_3/43 :
[
xattr_11 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_12 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_13 eq 'ab' , 
xattr_14 in ['am', 'an', 'ao'] ]
==>
[
xattr_15 set 'az' , 
xattr_16 set 'ax' , 
xattr_17 set 'x' , 
xattr_18 set 'ao' ].

xrule xschm_3/44 :
[
xattr_11 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_12 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_13 in ['ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_14 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] ]
==>
[
xattr_15 set 'ap' , 
xattr_16 set 'az' , 
xattr_17 set 'y' , 
xattr_18 set 'q' ].

xrule xschm_3/45 :
[
xattr_11 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_12 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_13 in ['ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_14 in ['am', 'an', 'ao'] ]
==>
[
xattr_15 set 'ax' , 
xattr_16 set 'am' , 
xattr_17 set 'az' , 
xattr_18 set 'af' ].

xrule xschm_3/46 :
[
xattr_11 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_12 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_13 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_14 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] ]
==>
[
xattr_15 set 'z' , 
xattr_16 set 'av' , 
xattr_17 set 'af' , 
xattr_18 set 'o' ].

xrule xschm_3/47 :
[
xattr_11 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_12 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_13 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_14 in ['am', 'an', 'ao'] ]
==>
[
xattr_15 set 'ak' , 
xattr_16 set 'al' , 
xattr_17 set 'aq' , 
xattr_18 set 'w' ].

xrule xschm_4/0 :
[
xattr_15 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] ]
==>
[
xattr_19 set 'ab' , 
xattr_20 set 'ab' , 
xattr_21 set 'ag' , 
xattr_22 set 'h' ].

xrule xschm_4/1 :
[
xattr_15 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_19 set 'ak' , 
xattr_20 set 'j' , 
xattr_21 set 'p' , 
xattr_22 set 'w' ].

xrule xschm_4/2 :
[
xattr_15 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_19 set 'ar' , 
xattr_20 set 'f' , 
xattr_21 set 'al' , 
xattr_22 set 'ao' ].

xrule xschm_4/3 :
[
xattr_15 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['ap', 'aq'] ]
==>
[
xattr_19 set 'n' , 
xattr_20 set 'w' , 
xattr_21 set 'af' , 
xattr_22 set 'e' ].

xrule xschm_4/4 :
[
xattr_15 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 eq 'be' , 
xattr_18 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] ]
==>
[
xattr_19 set 'al' , 
xattr_20 set 'g' , 
xattr_21 set 'am' , 
xattr_22 set 'e' ].

xrule xschm_4/5 :
[
xattr_15 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 eq 'be' , 
xattr_18 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_19 set 't' , 
xattr_20 set 'k' , 
xattr_21 set 'ab' , 
xattr_22 set 'am' ].

xrule xschm_4/6 :
[
xattr_15 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 eq 'be' , 
xattr_18 in ['aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_19 set 'ah' , 
xattr_20 set 'aj' , 
xattr_21 set 'al' , 
xattr_22 set 'af' ].

xrule xschm_4/7 :
[
xattr_15 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 eq 'be' , 
xattr_18 in ['ap', 'aq'] ]
==>
[
xattr_19 set 'aw' , 
xattr_20 set 'ae' , 
xattr_21 set 'v' , 
xattr_22 set 'o' ].

xrule xschm_4/8 :
[
xattr_15 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] ]
==>
[
xattr_19 set 'ai' , 
xattr_20 set 'e' , 
xattr_21 set 'r' , 
xattr_22 set 'l' ].

xrule xschm_4/9 :
[
xattr_15 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_19 set 'q' , 
xattr_20 set 'ae' , 
xattr_21 set 'n' , 
xattr_22 set 'k' ].

xrule xschm_4/10 :
[
xattr_15 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_19 set 'aj' , 
xattr_20 set 'k' , 
xattr_21 set 'y' , 
xattr_22 set 'q' ].

xrule xschm_4/11 :
[
xattr_15 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['ap', 'aq'] ]
==>
[
xattr_19 set 'p' , 
xattr_20 set 's' , 
xattr_21 set 'r' , 
xattr_22 set 'r' ].

xrule xschm_4/12 :
[
xattr_15 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 eq 'be' , 
xattr_18 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] ]
==>
[
xattr_19 set 'ac' , 
xattr_20 set 'h' , 
xattr_21 set 'aa' , 
xattr_22 set 'd' ].

xrule xschm_4/13 :
[
xattr_15 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 eq 'be' , 
xattr_18 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_19 set 'au' , 
xattr_20 set 'ai' , 
xattr_21 set 'at' , 
xattr_22 set 'k' ].

xrule xschm_4/14 :
[
xattr_15 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 eq 'be' , 
xattr_18 in ['aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_19 set 'ad' , 
xattr_20 set 'ai' , 
xattr_21 set 'm' , 
xattr_22 set 'r' ].

xrule xschm_4/15 :
[
xattr_15 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 eq 'be' , 
xattr_18 in ['ap', 'aq'] ]
==>
[
xattr_19 set 'an' , 
xattr_20 set 'p' , 
xattr_21 set 'v' , 
xattr_22 set 'c' ].

xrule xschm_4/16 :
[
xattr_15 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] ]
==>
[
xattr_19 set 'ag' , 
xattr_20 set 'k' , 
xattr_21 set 'ap' , 
xattr_22 set 'c' ].

xrule xschm_4/17 :
[
xattr_15 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_19 set 'ab' , 
xattr_20 set 'an' , 
xattr_21 set 'o' , 
xattr_22 set 'm' ].

xrule xschm_4/18 :
[
xattr_15 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_19 set 'aj' , 
xattr_20 set 'i' , 
xattr_21 set 's' , 
xattr_22 set 'q' ].

xrule xschm_4/19 :
[
xattr_15 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['ap', 'aq'] ]
==>
[
xattr_19 set 'aj' , 
xattr_20 set 'h' , 
xattr_21 set 'ao' , 
xattr_22 set 'i' ].

xrule xschm_4/20 :
[
xattr_15 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 eq 'be' , 
xattr_18 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] ]
==>
[
xattr_19 set 'ar' , 
xattr_20 set 'p' , 
xattr_21 set 'p' , 
xattr_22 set 'x' ].

xrule xschm_4/21 :
[
xattr_15 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 eq 'be' , 
xattr_18 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_19 set 'ac' , 
xattr_20 set 'ao' , 
xattr_21 set 'w' , 
xattr_22 set 'o' ].

xrule xschm_4/22 :
[
xattr_15 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 eq 'be' , 
xattr_18 in ['aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_19 set 'aa' , 
xattr_20 set 'ad' , 
xattr_21 set 'm' , 
xattr_22 set 'v' ].

xrule xschm_4/23 :
[
xattr_15 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 eq 'be' , 
xattr_18 in ['ap', 'aq'] ]
==>
[
xattr_19 set 'ao' , 
xattr_20 set 'ac' , 
xattr_21 set 'ar' , 
xattr_22 set 'aa' ].

xrule xschm_4/24 :
[
xattr_15 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] ]
==>
[
xattr_19 set 'aa' , 
xattr_20 set 'ah' , 
xattr_21 set 'v' , 
xattr_22 set 'ao' ].

xrule xschm_4/25 :
[
xattr_15 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_19 set 'aj' , 
xattr_20 set 'af' , 
xattr_21 set 'ae' , 
xattr_22 set 'x' ].

xrule xschm_4/26 :
[
xattr_15 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_19 set 'av' , 
xattr_20 set 'm' , 
xattr_21 set 'z' , 
xattr_22 set 'ak' ].

xrule xschm_4/27 :
[
xattr_15 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['ap', 'aq'] ]
==>
[
xattr_19 set 'az' , 
xattr_20 set 'q' , 
xattr_21 set 'af' , 
xattr_22 set 'am' ].

xrule xschm_4/28 :
[
xattr_15 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 eq 'be' , 
xattr_18 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] ]
==>
[
xattr_19 set 'z' , 
xattr_20 set 's' , 
xattr_21 set 'r' , 
xattr_22 set 'ad' ].

xrule xschm_4/29 :
[
xattr_15 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 eq 'be' , 
xattr_18 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_19 set 'av' , 
xattr_20 set 'an' , 
xattr_21 set 'h' , 
xattr_22 set 'o' ].

xrule xschm_4/30 :
[
xattr_15 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 eq 'be' , 
xattr_18 in ['aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_19 set 'av' , 
xattr_20 set 'i' , 
xattr_21 set 'aq' , 
xattr_22 set 'o' ].

xrule xschm_4/31 :
[
xattr_15 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 eq 'be' , 
xattr_18 in ['ap', 'aq'] ]
==>
[
xattr_19 set 'ah' , 
xattr_20 set 'g' , 
xattr_21 set 'aj' , 
xattr_22 set 'd' ].

xrule xschm_4/32 :
[
xattr_15 in ['ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] ]
==>
[
xattr_19 set 'as' , 
xattr_20 set 'v' , 
xattr_21 set 'ae' , 
xattr_22 set 'k' ].

xrule xschm_4/33 :
[
xattr_15 in ['ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_19 set 'u' , 
xattr_20 set 'k' , 
xattr_21 set 'r' , 
xattr_22 set 'ag' ].

xrule xschm_4/34 :
[
xattr_15 in ['ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_19 set 'v' , 
xattr_20 set 't' , 
xattr_21 set 'an' , 
xattr_22 set 'ah' ].

xrule xschm_4/35 :
[
xattr_15 in ['ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['ap', 'aq'] ]
==>
[
xattr_19 set 'at' , 
xattr_20 set 'z' , 
xattr_21 set 'aa' , 
xattr_22 set 'o' ].

xrule xschm_4/36 :
[
xattr_15 in ['ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 eq 'be' , 
xattr_18 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] ]
==>
[
xattr_19 set 'aw' , 
xattr_20 set 'ak' , 
xattr_21 set 'as' , 
xattr_22 set 'h' ].

xrule xschm_4/37 :
[
xattr_15 in ['ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 eq 'be' , 
xattr_18 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_19 set 'al' , 
xattr_20 set 'ak' , 
xattr_21 set 'ae' , 
xattr_22 set 'v' ].

xrule xschm_4/38 :
[
xattr_15 in ['ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 eq 'be' , 
xattr_18 in ['aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_19 set 'at' , 
xattr_20 set 'an' , 
xattr_21 set 'j' , 
xattr_22 set 'ai' ].

xrule xschm_4/39 :
[
xattr_15 in ['ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 eq 'be' , 
xattr_18 in ['ap', 'aq'] ]
==>
[
xattr_19 set 'ba' , 
xattr_20 set 'm' , 
xattr_21 set 'aa' , 
xattr_22 set 'y' ].

xrule xschm_4/40 :
[
xattr_15 in ['ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] ]
==>
[
xattr_19 set 'ax' , 
xattr_20 set 'aa' , 
xattr_21 set 'ab' , 
xattr_22 set 'l' ].

xrule xschm_4/41 :
[
xattr_15 in ['ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_19 set 'q' , 
xattr_20 set 'n' , 
xattr_21 set 'as' , 
xattr_22 set 'aj' ].

xrule xschm_4/42 :
[
xattr_15 in ['ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_19 set 'ae' , 
xattr_20 set 'af' , 
xattr_21 set 'ah' , 
xattr_22 set 'ah' ].

xrule xschm_4/43 :
[
xattr_15 in ['ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['ap', 'aq'] ]
==>
[
xattr_19 set 'ak' , 
xattr_20 set 'p' , 
xattr_21 set 'al' , 
xattr_22 set 'o' ].

xrule xschm_4/44 :
[
xattr_15 in ['ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 eq 'be' , 
xattr_18 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] ]
==>
[
xattr_19 set 'as' , 
xattr_20 set 'j' , 
xattr_21 set 'u' , 
xattr_22 set 't' ].

xrule xschm_4/45 :
[
xattr_15 in ['ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 eq 'be' , 
xattr_18 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_19 set 'ao' , 
xattr_20 set 'k' , 
xattr_21 set 'au' , 
xattr_22 set 'i' ].

xrule xschm_4/46 :
[
xattr_15 in ['ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 eq 'be' , 
xattr_18 in ['aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_19 set 'ab' , 
xattr_20 set 'al' , 
xattr_21 set 'p' , 
xattr_22 set 'ak' ].

xrule xschm_4/47 :
[
xattr_15 in ['ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 eq 'be' , 
xattr_18 in ['ap', 'aq'] ]
==>
[
xattr_19 set 'an' , 
xattr_20 set 'p' , 
xattr_21 set 'l' , 
xattr_22 set 'o' ].

xrule xschm_4/48 :
[
xattr_15 in ['bd', 'be'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] ]
==>
[
xattr_19 set 'n' , 
xattr_20 set 'e' , 
xattr_21 set 'x' , 
xattr_22 set 'w' ].

xrule xschm_4/49 :
[
xattr_15 in ['bd', 'be'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_19 set 'as' , 
xattr_20 set 'u' , 
xattr_21 set 'aa' , 
xattr_22 set 'l' ].

xrule xschm_4/50 :
[
xattr_15 in ['bd', 'be'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_19 set 'y' , 
xattr_20 set 'f' , 
xattr_21 set 'ab' , 
xattr_22 set 'k' ].

xrule xschm_4/51 :
[
xattr_15 in ['bd', 'be'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['ap', 'aq'] ]
==>
[
xattr_19 set 'ar' , 
xattr_20 set 'ae' , 
xattr_21 set 'as' , 
xattr_22 set 'y' ].

xrule xschm_4/52 :
[
xattr_15 in ['bd', 'be'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 eq 'be' , 
xattr_18 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] ]
==>
[
xattr_19 set 'ay' , 
xattr_20 set 'f' , 
xattr_21 set 'ac' , 
xattr_22 set 'ak' ].

xrule xschm_4/53 :
[
xattr_15 in ['bd', 'be'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 eq 'be' , 
xattr_18 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_19 set 'ae' , 
xattr_20 set 'ad' , 
xattr_21 set 'aj' , 
xattr_22 set 'ab' ].

xrule xschm_4/54 :
[
xattr_15 in ['bd', 'be'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 eq 'be' , 
xattr_18 in ['aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_19 set 'ar' , 
xattr_20 set 'i' , 
xattr_21 set 'ae' , 
xattr_22 set 'b' ].

xrule xschm_4/55 :
[
xattr_15 in ['bd', 'be'] , 
xattr_16 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] , 
xattr_17 eq 'be' , 
xattr_18 in ['ap', 'aq'] ]
==>
[
xattr_19 set 'n' , 
xattr_20 set 'ad' , 
xattr_21 set 's' , 
xattr_22 set 'z' ].

xrule xschm_4/56 :
[
xattr_15 in ['bd', 'be'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] ]
==>
[
xattr_19 set 'ae' , 
xattr_20 set 'o' , 
xattr_21 set 'ad' , 
xattr_22 set 'z' ].

xrule xschm_4/57 :
[
xattr_15 in ['bd', 'be'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_19 set 'u' , 
xattr_20 set 'b' , 
xattr_21 set 'ag' , 
xattr_22 set 'f' ].

xrule xschm_4/58 :
[
xattr_15 in ['bd', 'be'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_19 set 'an' , 
xattr_20 set 'r' , 
xattr_21 set 'i' , 
xattr_22 set 'p' ].

xrule xschm_4/59 :
[
xattr_15 in ['bd', 'be'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] , 
xattr_18 in ['ap', 'aq'] ]
==>
[
xattr_19 set 'q' , 
xattr_20 set 'q' , 
xattr_21 set 'u' , 
xattr_22 set 'ag' ].

xrule xschm_4/60 :
[
xattr_15 in ['bd', 'be'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 eq 'be' , 
xattr_18 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] ]
==>
[
xattr_19 set 'at' , 
xattr_20 set 'd' , 
xattr_21 set 'ap' , 
xattr_22 set 'ae' ].

xrule xschm_4/61 :
[
xattr_15 in ['bd', 'be'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 eq 'be' , 
xattr_18 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_19 set 's' , 
xattr_20 set 'ai' , 
xattr_21 set 'z' , 
xattr_22 set 'ah' ].

xrule xschm_4/62 :
[
xattr_15 in ['bd', 'be'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 eq 'be' , 
xattr_18 in ['aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_19 set 'ap' , 
xattr_20 set 's' , 
xattr_21 set 'l' , 
xattr_22 set 'aj' ].

xrule xschm_4/63 :
[
xattr_15 in ['bd', 'be'] , 
xattr_16 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_17 eq 'be' , 
xattr_18 in ['ap', 'aq'] ]
==>
[
xattr_19 set 'ai' , 
xattr_20 set 'b' , 
xattr_21 set 'l' , 
xattr_22 set 'e' ].

xrule xschm_5/0 :
[
xattr_19 in ['n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_20 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_21 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't'] , 
xattr_22 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'] ]
==>
[
xattr_23 set 'ai' , 
xattr_24 set 'ax' , 
xattr_25 set 'ag' ].

xrule xschm_5/1 :
[
xattr_19 in ['n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_20 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_21 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't'] , 
xattr_22 in ['aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_23 set 'y' , 
xattr_24 set 't' , 
xattr_25 set 'g' ].

xrule xschm_5/2 :
[
xattr_19 in ['n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_20 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_21 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_22 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'] ]
==>
[
xattr_23 set 'ab' , 
xattr_24 set 'ao' , 
xattr_25 set 'ae' ].

xrule xschm_5/3 :
[
xattr_19 in ['n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_20 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_21 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_22 in ['aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_23 set 'be' , 
xattr_24 set 'ar' , 
xattr_25 set 'y' ].

xrule xschm_5/4 :
[
xattr_19 in ['n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_20 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_21 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't'] , 
xattr_22 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'] ]
==>
[
xattr_23 set 'aj' , 
xattr_24 set 'ae' , 
xattr_25 set 'ae' ].

xrule xschm_5/5 :
[
xattr_19 in ['n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_20 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_21 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't'] , 
xattr_22 in ['aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_23 set 'ak' , 
xattr_24 set 'ah' , 
xattr_25 set 'ad' ].

xrule xschm_5/6 :
[
xattr_19 in ['n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_20 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_21 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_22 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'] ]
==>
[
xattr_23 set 'ai' , 
xattr_24 set 'aw' , 
xattr_25 set 'af' ].

xrule xschm_5/7 :
[
xattr_19 in ['n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_20 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_21 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_22 in ['aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_23 set 'bb' , 
xattr_24 set 'aq' , 
xattr_25 set 'aj' ].

xrule xschm_5/8 :
[
xattr_19 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba'] , 
xattr_20 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_21 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't'] , 
xattr_22 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'] ]
==>
[
xattr_23 set 'bd' , 
xattr_24 set 'ak' , 
xattr_25 set 'y' ].

xrule xschm_5/9 :
[
xattr_19 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba'] , 
xattr_20 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_21 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't'] , 
xattr_22 in ['aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_23 set 'ah' , 
xattr_24 set 'ax' , 
xattr_25 set 'ak' ].

xrule xschm_5/10 :
[
xattr_19 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba'] , 
xattr_20 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_21 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_22 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'] ]
==>
[
xattr_23 set 'w' , 
xattr_24 set 'x' , 
xattr_25 set 'ah' ].

xrule xschm_5/11 :
[
xattr_19 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba'] , 
xattr_20 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_21 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_22 in ['aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_23 set 'af' , 
xattr_24 set 'p' , 
xattr_25 set 'r' ].

xrule xschm_5/12 :
[
xattr_19 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba'] , 
xattr_20 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_21 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't'] , 
xattr_22 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'] ]
==>
[
xattr_23 set 'v' , 
xattr_24 set 'am' , 
xattr_25 set 'g' ].

xrule xschm_5/13 :
[
xattr_19 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba'] , 
xattr_20 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_21 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't'] , 
xattr_22 in ['aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_23 set 'av' , 
xattr_24 set 'z' , 
xattr_25 set 'p' ].

xrule xschm_5/14 :
[
xattr_19 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba'] , 
xattr_20 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_21 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_22 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'] ]
==>
[
xattr_23 set 'aa' , 
xattr_24 set 'y' , 
xattr_25 set 'ap' ].

xrule xschm_5/15 :
[
xattr_19 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba'] , 
xattr_20 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_21 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_22 in ['aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_23 set 'ac' , 
xattr_24 set 'aq' , 
xattr_25 set 'z' ].

xrule xschm_6/0 :
[
xattr_23 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_24 in ['m', 'n', 'o', 'p', 'q', 'r', 's'] , 
xattr_25 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] ]
==>
[
xattr_26 set 'x' , 
xattr_27 set 'aj' , 
xattr_28 set 'ai' ].

xrule xschm_6/1 :
[
xattr_23 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_24 in ['m', 'n', 'o', 'p', 'q', 'r', 's'] , 
xattr_25 in ['aq', 'ar', 'as'] ]
==>
[
xattr_26 set 'az' , 
xattr_27 set 'ah' , 
xattr_28 set 'bk' ].

xrule xschm_6/2 :
[
xattr_23 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_24 in ['t', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_25 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] ]
==>
[
xattr_26 set 'aq' , 
xattr_27 set 'ab' , 
xattr_28 set 'ak' ].

xrule xschm_6/3 :
[
xattr_23 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_24 in ['t', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_25 in ['aq', 'ar', 'as'] ]
==>
[
xattr_26 set 'bb' , 
xattr_27 set 'ai' , 
xattr_28 set 'ag' ].

xrule xschm_7/0 :
[
xattr_26 in ['x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_27 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'] , 
xattr_28 in ['x', 'y', 'z', 'aa', 'ab', 'ac'] ]
==>
[
xattr_29 set 'an' , 
xattr_30 set 'ah' , 
xattr_31 set 'be' , 
xattr_32 set 'av' ].

xrule xschm_7/1 :
[
xattr_26 in ['x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_27 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'] , 
xattr_28 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg'] ]
==>
[
xattr_29 set 'aa' , 
xattr_30 set 'be' , 
xattr_31 set 'aj' , 
xattr_32 set 'av' ].

xrule xschm_7/2 :
[
xattr_26 in ['x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_27 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'] , 
xattr_28 in ['bh', 'bi', 'bj', 'bk'] ]
==>
[
xattr_29 set 'ar' , 
xattr_30 set 'ab' , 
xattr_31 set 'af' , 
xattr_32 set 'af' ].

xrule xschm_7/3 :
[
xattr_26 in ['x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_27 in ['o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_28 in ['x', 'y', 'z', 'aa', 'ab', 'ac'] ]
==>
[
xattr_29 set 'al' , 
xattr_30 set 'ba' , 
xattr_31 set 'az' , 
xattr_32 set 'ao' ].

xrule xschm_7/4 :
[
xattr_26 in ['x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_27 in ['o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_28 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg'] ]
==>
[
xattr_29 set 'ak' , 
xattr_30 set 'an' , 
xattr_31 set 'be' , 
xattr_32 set 'ak' ].

xrule xschm_7/5 :
[
xattr_26 in ['x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_27 in ['o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_28 in ['bh', 'bi', 'bj', 'bk'] ]
==>
[
xattr_29 set 'q' , 
xattr_30 set 'as' , 
xattr_31 set 'ao' , 
xattr_32 set 'n' ].

xrule xschm_7/6 :
[
xattr_26 in ['x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_27 in ['al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_28 in ['x', 'y', 'z', 'aa', 'ab', 'ac'] ]
==>
[
xattr_29 set 'ao' , 
xattr_30 set 'v' , 
xattr_31 set 'ag' , 
xattr_32 set 'u' ].

xrule xschm_7/7 :
[
xattr_26 in ['x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_27 in ['al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_28 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg'] ]
==>
[
xattr_29 set 'ab' , 
xattr_30 set 'ad' , 
xattr_31 set 'bd' , 
xattr_32 set 'ap' ].

xrule xschm_7/8 :
[
xattr_26 in ['x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_27 in ['al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_28 in ['bh', 'bi', 'bj', 'bk'] ]
==>
[
xattr_29 set 'r' , 
xattr_30 set 'z' , 
xattr_31 set 'ax' , 
xattr_32 set 'm' ].

xrule xschm_7/9 :
[
xattr_26 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_27 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'] , 
xattr_28 in ['x', 'y', 'z', 'aa', 'ab', 'ac'] ]
==>
[
xattr_29 set 'r' , 
xattr_30 set 'ae' , 
xattr_31 set 'v' , 
xattr_32 set 'af' ].

xrule xschm_7/10 :
[
xattr_26 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_27 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'] , 
xattr_28 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg'] ]
==>
[
xattr_29 set 'ac' , 
xattr_30 set 'am' , 
xattr_31 set 'ax' , 
xattr_32 set 'ao' ].

xrule xschm_7/11 :
[
xattr_26 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_27 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'] , 
xattr_28 in ['bh', 'bi', 'bj', 'bk'] ]
==>
[
xattr_29 set 'w' , 
xattr_30 set 'y' , 
xattr_31 set 'aw' , 
xattr_32 set 'au' ].

xrule xschm_7/12 :
[
xattr_26 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_27 in ['o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_28 in ['x', 'y', 'z', 'aa', 'ab', 'ac'] ]
==>
[
xattr_29 set 'v' , 
xattr_30 set 'az' , 
xattr_31 set 'ap' , 
xattr_32 set 'av' ].

xrule xschm_7/13 :
[
xattr_26 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_27 in ['o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_28 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg'] ]
==>
[
xattr_29 set 'ap' , 
xattr_30 set 'an' , 
xattr_31 set 'ax' , 
xattr_32 set 'ab' ].

xrule xschm_7/14 :
[
xattr_26 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_27 in ['o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_28 in ['bh', 'bi', 'bj', 'bk'] ]
==>
[
xattr_29 set 'as' , 
xattr_30 set 'ao' , 
xattr_31 set 'ab' , 
xattr_32 set 'ax' ].

xrule xschm_7/15 :
[
xattr_26 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_27 in ['al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_28 in ['x', 'y', 'z', 'aa', 'ab', 'ac'] ]
==>
[
xattr_29 set 'az' , 
xattr_30 set 'bd' , 
xattr_31 set 'as' , 
xattr_32 set 'ak' ].

xrule xschm_7/16 :
[
xattr_26 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_27 in ['al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_28 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg'] ]
==>
[
xattr_29 set 'aq' , 
xattr_30 set 'av' , 
xattr_31 set 'az' , 
xattr_32 set 'ay' ].

xrule xschm_7/17 :
[
xattr_26 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_27 in ['al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_28 in ['bh', 'bi', 'bj', 'bk'] ]
==>
[
xattr_29 set 'au' , 
xattr_30 set 'ah' , 
xattr_31 set 'al' , 
xattr_32 set 'ap' ].

xrule xschm_7/18 :
[
xattr_26 in ['az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk'] , 
xattr_27 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'] , 
xattr_28 in ['x', 'y', 'z', 'aa', 'ab', 'ac'] ]
==>
[
xattr_29 set 'm' , 
xattr_30 set 'bb' , 
xattr_31 set 'av' , 
xattr_32 set 'am' ].

xrule xschm_7/19 :
[
xattr_26 in ['az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk'] , 
xattr_27 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'] , 
xattr_28 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg'] ]
==>
[
xattr_29 set 'x' , 
xattr_30 set 'am' , 
xattr_31 set 'u' , 
xattr_32 set 't' ].

xrule xschm_7/20 :
[
xattr_26 in ['az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk'] , 
xattr_27 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'] , 
xattr_28 in ['bh', 'bi', 'bj', 'bk'] ]
==>
[
xattr_29 set 'ae' , 
xattr_30 set 'bd' , 
xattr_31 set 'af' , 
xattr_32 set 'm' ].

xrule xschm_7/21 :
[
xattr_26 in ['az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk'] , 
xattr_27 in ['o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_28 in ['x', 'y', 'z', 'aa', 'ab', 'ac'] ]
==>
[
xattr_29 set 'u' , 
xattr_30 set 'be' , 
xattr_31 set 's' , 
xattr_32 set 'av' ].

xrule xschm_7/22 :
[
xattr_26 in ['az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk'] , 
xattr_27 in ['o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_28 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg'] ]
==>
[
xattr_29 set 'ap' , 
xattr_30 set 'aw' , 
xattr_31 set 'ac' , 
xattr_32 set 'am' ].

xrule xschm_7/23 :
[
xattr_26 in ['az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk'] , 
xattr_27 in ['o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_28 in ['bh', 'bi', 'bj', 'bk'] ]
==>
[
xattr_29 set 'au' , 
xattr_30 set 'al' , 
xattr_31 set 'z' , 
xattr_32 set 'ai' ].

xrule xschm_7/24 :
[
xattr_26 in ['az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk'] , 
xattr_27 in ['al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_28 in ['x', 'y', 'z', 'aa', 'ab', 'ac'] ]
==>
[
xattr_29 set 'am' , 
xattr_30 set 'bd' , 
xattr_31 set 'ba' , 
xattr_32 set 'al' ].

xrule xschm_7/25 :
[
xattr_26 in ['az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk'] , 
xattr_27 in ['al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_28 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg'] ]
==>
[
xattr_29 set 'y' , 
xattr_30 set 'v' , 
xattr_31 set 'ao' , 
xattr_32 set 't' ].

xrule xschm_7/26 :
[
xattr_26 in ['az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk'] , 
xattr_27 in ['al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_28 in ['bh', 'bi', 'bj', 'bk'] ]
==>
[
xattr_29 set 'y' , 
xattr_30 set 'az' , 
xattr_31 set 'u' , 
xattr_32 set 'w' ].

xrule xschm_8/0 :
[
xattr_29 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_30 in ['v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_31 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_32 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_33 set 'q' , 
xattr_34 set 'ae' , 
xattr_35 set 'af' ].

xrule xschm_8/1 :
[
xattr_29 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_30 in ['v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_31 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_32 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_33 set 'ah' , 
xattr_34 set 'aj' , 
xattr_35 set 'j' ].

xrule xschm_8/2 :
[
xattr_29 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_30 in ['v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_31 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_32 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_33 set 'z' , 
xattr_34 set 'ag' , 
xattr_35 set 'am' ].

xrule xschm_8/3 :
[
xattr_29 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_30 in ['v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_31 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_32 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_33 set 'am' , 
xattr_34 set 'ao' , 
xattr_35 set 'g' ].

xrule xschm_8/4 :
[
xattr_29 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_30 in ['v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_31 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_32 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_33 set 'ag' , 
xattr_34 set 'o' , 
xattr_35 set 'ah' ].

xrule xschm_8/5 :
[
xattr_29 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_30 in ['v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_31 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_32 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_33 set 'aa' , 
xattr_34 set 'w' , 
xattr_35 set 'ar' ].

xrule xschm_8/6 :
[
xattr_29 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_30 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_31 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_32 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_33 set 's' , 
xattr_34 set 'u' , 
xattr_35 set 'aa' ].

xrule xschm_8/7 :
[
xattr_29 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_30 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_31 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_32 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_33 set 'i' , 
xattr_34 set 'h' , 
xattr_35 set 's' ].

xrule xschm_8/8 :
[
xattr_29 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_30 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_31 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_32 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_33 set 'ak' , 
xattr_34 set 'd' , 
xattr_35 set 'ad' ].

xrule xschm_8/9 :
[
xattr_29 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_30 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_31 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_32 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_33 set 'u' , 
xattr_34 set 'aj' , 
xattr_35 set 'm' ].

xrule xschm_8/10 :
[
xattr_29 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_30 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_31 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_32 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_33 set 'ah' , 
xattr_34 set 'aj' , 
xattr_35 set 'u' ].

xrule xschm_8/11 :
[
xattr_29 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_30 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_31 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_32 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_33 set 'y' , 
xattr_34 set 'e' , 
xattr_35 set 'j' ].

xrule xschm_8/12 :
[
xattr_29 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_30 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_31 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_32 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_33 set 'q' , 
xattr_34 set 'af' , 
xattr_35 set 'z' ].

xrule xschm_8/13 :
[
xattr_29 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_30 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_31 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_32 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_33 set 'ac' , 
xattr_34 set 'f' , 
xattr_35 set 'an' ].

xrule xschm_8/14 :
[
xattr_29 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_30 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_31 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_32 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_33 set 't' , 
xattr_34 set 'f' , 
xattr_35 set 'ac' ].

xrule xschm_8/15 :
[
xattr_29 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_30 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_31 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_32 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_33 set 'aa' , 
xattr_34 set 'am' , 
xattr_35 set 'ag' ].

xrule xschm_8/16 :
[
xattr_29 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_30 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_31 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_32 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_33 set 'h' , 
xattr_34 set 'c' , 
xattr_35 set 'w' ].

xrule xschm_8/17 :
[
xattr_29 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_30 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_31 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_32 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_33 set 'au' , 
xattr_34 set 'n' , 
xattr_35 set 'h' ].

xrule xschm_8/18 :
[
xattr_29 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_30 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_31 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_32 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_33 set 'am' , 
xattr_34 set 'x' , 
xattr_35 set 'g' ].

xrule xschm_8/19 :
[
xattr_29 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_30 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_31 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_32 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_33 set 'aa' , 
xattr_34 set 'g' , 
xattr_35 set 'w' ].

xrule xschm_8/20 :
[
xattr_29 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_30 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_31 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_32 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_33 set 'q' , 
xattr_34 set 'k' , 
xattr_35 set 'an' ].

xrule xschm_8/21 :
[
xattr_29 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_30 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_31 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_32 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_33 set 'an' , 
xattr_34 set 'u' , 
xattr_35 set 'ai' ].

xrule xschm_8/22 :
[
xattr_29 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_30 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_31 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_32 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_33 set 'ad' , 
xattr_34 set 'e' , 
xattr_35 set 't' ].

xrule xschm_8/23 :
[
xattr_29 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_30 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_31 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_32 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_33 set 's' , 
xattr_34 set 'ao' , 
xattr_35 set 'y' ].

xrule xschm_8/24 :
[
xattr_29 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_30 eq 'bi' , 
xattr_31 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_32 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_33 set 'ai' , 
xattr_34 set 'k' , 
xattr_35 set 'ad' ].

xrule xschm_8/25 :
[
xattr_29 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_30 eq 'bi' , 
xattr_31 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_32 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_33 set 'v' , 
xattr_34 set 'n' , 
xattr_35 set 'ao' ].

xrule xschm_8/26 :
[
xattr_29 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_30 eq 'bi' , 
xattr_31 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_32 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_33 set 'u' , 
xattr_34 set 'q' , 
xattr_35 set 'ao' ].

xrule xschm_8/27 :
[
xattr_29 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_30 eq 'bi' , 
xattr_31 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_32 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_33 set 'ah' , 
xattr_34 set 'g' , 
xattr_35 set 'i' ].

xrule xschm_8/28 :
[
xattr_29 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_30 eq 'bi' , 
xattr_31 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_32 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_33 set 'x' , 
xattr_34 set 'ak' , 
xattr_35 set 'h' ].

xrule xschm_8/29 :
[
xattr_29 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_30 eq 'bi' , 
xattr_31 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_32 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_33 set 'u' , 
xattr_34 set 'p' , 
xattr_35 set 'h' ].

xrule xschm_8/30 :
[
xattr_29 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_30 in ['v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_31 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_32 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_33 set 'au' , 
xattr_34 set 't' , 
xattr_35 set 'g' ].

xrule xschm_8/31 :
[
xattr_29 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_30 in ['v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_31 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_32 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_33 set 'u' , 
xattr_34 set 'x' , 
xattr_35 set 'aa' ].

xrule xschm_8/32 :
[
xattr_29 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_30 in ['v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_31 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_32 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_33 set 'ad' , 
xattr_34 set 'e' , 
xattr_35 set 'p' ].

xrule xschm_8/33 :
[
xattr_29 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_30 in ['v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_31 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_32 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_33 set 'q' , 
xattr_34 set 'am' , 
xattr_35 set 'ae' ].

xrule xschm_8/34 :
[
xattr_29 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_30 in ['v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_31 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_32 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_33 set 'ar' , 
xattr_34 set 'f' , 
xattr_35 set 'j' ].

xrule xschm_8/35 :
[
xattr_29 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_30 in ['v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_31 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_32 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_33 set 'l' , 
xattr_34 set 'am' , 
xattr_35 set 'm' ].

xrule xschm_8/36 :
[
xattr_29 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_30 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_31 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_32 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_33 set 'y' , 
xattr_34 set 'ai' , 
xattr_35 set 'ao' ].

xrule xschm_8/37 :
[
xattr_29 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_30 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_31 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_32 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_33 set 'r' , 
xattr_34 set 'ai' , 
xattr_35 set 'ak' ].

xrule xschm_8/38 :
[
xattr_29 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_30 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_31 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_32 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_33 set 'i' , 
xattr_34 set 'l' , 
xattr_35 set 'v' ].

xrule xschm_8/39 :
[
xattr_29 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_30 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_31 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_32 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_33 set 'l' , 
xattr_34 set 'am' , 
xattr_35 set 'i' ].

xrule xschm_8/40 :
[
xattr_29 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_30 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_31 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_32 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_33 set 'aj' , 
xattr_34 set 'u' , 
xattr_35 set 'ar' ].

xrule xschm_8/41 :
[
xattr_29 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_30 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_31 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_32 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_33 set 'ae' , 
xattr_34 set 'ao' , 
xattr_35 set 'al' ].

xrule xschm_8/42 :
[
xattr_29 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_30 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_31 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_32 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_33 set 'o' , 
xattr_34 set 'al' , 
xattr_35 set 'ab' ].

xrule xschm_8/43 :
[
xattr_29 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_30 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_31 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_32 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_33 set 'ac' , 
xattr_34 set 'l' , 
xattr_35 set 'i' ].

xrule xschm_8/44 :
[
xattr_29 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_30 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_31 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_32 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_33 set 'x' , 
xattr_34 set 'ae' , 
xattr_35 set 'ag' ].

xrule xschm_8/45 :
[
xattr_29 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_30 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_31 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_32 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_33 set 'ah' , 
xattr_34 set 'al' , 
xattr_35 set 'ap' ].

xrule xschm_8/46 :
[
xattr_29 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_30 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_31 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_32 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_33 set 'u' , 
xattr_34 set 'c' , 
xattr_35 set 'aa' ].

xrule xschm_8/47 :
[
xattr_29 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_30 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_31 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_32 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_33 set 'ae' , 
xattr_34 set 'ac' , 
xattr_35 set 'ad' ].

xrule xschm_8/48 :
[
xattr_29 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_30 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_31 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_32 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_33 set 't' , 
xattr_34 set 'p' , 
xattr_35 set 'r' ].

xrule xschm_8/49 :
[
xattr_29 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_30 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_31 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_32 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_33 set 'u' , 
xattr_34 set 'j' , 
xattr_35 set 'ap' ].

xrule xschm_8/50 :
[
xattr_29 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_30 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_31 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_32 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_33 set 'q' , 
xattr_34 set 'aj' , 
xattr_35 set 'ao' ].

xrule xschm_8/51 :
[
xattr_29 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_30 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_31 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_32 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_33 set 'at' , 
xattr_34 set 'ac' , 
xattr_35 set 'g' ].

xrule xschm_8/52 :
[
xattr_29 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_30 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_31 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_32 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_33 set 'r' , 
xattr_34 set 'ap' , 
xattr_35 set 'aq' ].

xrule xschm_8/53 :
[
xattr_29 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_30 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_31 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_32 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_33 set 'an' , 
xattr_34 set 'ao' , 
xattr_35 set 'af' ].

xrule xschm_8/54 :
[
xattr_29 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_30 eq 'bi' , 
xattr_31 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_32 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_33 set 'af' , 
xattr_34 set 'q' , 
xattr_35 set 'r' ].

xrule xschm_8/55 :
[
xattr_29 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_30 eq 'bi' , 
xattr_31 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_32 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_33 set 'ae' , 
xattr_34 set 'q' , 
xattr_35 set 'j' ].

xrule xschm_8/56 :
[
xattr_29 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_30 eq 'bi' , 
xattr_31 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_32 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_33 set 's' , 
xattr_34 set 'al' , 
xattr_35 set 'ak' ].

xrule xschm_8/57 :
[
xattr_29 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_30 eq 'bi' , 
xattr_31 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_32 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_33 set 'o' , 
xattr_34 set 'f' , 
xattr_35 set 'y' ].

xrule xschm_8/58 :
[
xattr_29 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_30 eq 'bi' , 
xattr_31 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_32 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] ]
==>
[
xattr_33 set 'am' , 
xattr_34 set 'ah' , 
xattr_35 set 'an' ].

xrule xschm_8/59 :
[
xattr_29 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_30 eq 'bi' , 
xattr_31 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_32 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_33 set 'n' , 
xattr_34 set 'c' , 
xattr_35 set 'ag' ].

xrule xschm_9/0 :
[
xattr_33 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_34 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l'] , 
xattr_35 in ['f', 'g'] ]
==>
[
xattr_36 set 'aj' , 
xattr_37 set 'ad' ].

xrule xschm_9/1 :
[
xattr_33 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_34 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l'] , 
xattr_35 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag'] ]
==>
[
xattr_36 set 'n' , 
xattr_37 set 'ah' ].

xrule xschm_9/2 :
[
xattr_33 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_34 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l'] , 
xattr_35 in ['ah', 'ai', 'aj'] ]
==>
[
xattr_36 set 'ag' , 
xattr_37 set 'ay' ].

xrule xschm_9/3 :
[
xattr_33 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_34 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l'] , 
xattr_35 in ['ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] ]
==>
[
xattr_36 set 'e' , 
xattr_37 set 'ag' ].

xrule xschm_9/4 :
[
xattr_33 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_34 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_35 in ['f', 'g'] ]
==>
[
xattr_36 set 'n' , 
xattr_37 set 'as' ].

xrule xschm_9/5 :
[
xattr_33 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_34 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_35 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag'] ]
==>
[
xattr_36 set 't' , 
xattr_37 set 'ab' ].

xrule xschm_9/6 :
[
xattr_33 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_34 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_35 in ['ah', 'ai', 'aj'] ]
==>
[
xattr_36 set 'n' , 
xattr_37 set 'bh' ].

xrule xschm_9/7 :
[
xattr_33 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_34 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_35 in ['ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] ]
==>
[
xattr_36 set 'al' , 
xattr_37 set 'ay' ].

xrule xschm_9/8 :
[
xattr_33 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_34 in ['ao', 'ap'] , 
xattr_35 in ['f', 'g'] ]
==>
[
xattr_36 set 'h' , 
xattr_37 set 'bb' ].

xrule xschm_9/9 :
[
xattr_33 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_34 in ['ao', 'ap'] , 
xattr_35 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag'] ]
==>
[
xattr_36 set 'l' , 
xattr_37 set 'ai' ].

xrule xschm_9/10 :
[
xattr_33 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_34 in ['ao', 'ap'] , 
xattr_35 in ['ah', 'ai', 'aj'] ]
==>
[
xattr_36 set 't' , 
xattr_37 set 'ay' ].

xrule xschm_9/11 :
[
xattr_33 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_34 in ['ao', 'ap'] , 
xattr_35 in ['ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] ]
==>
[
xattr_36 set 'af' , 
xattr_37 set 'bb' ].

xrule xschm_9/12 :
[
xattr_33 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_34 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l'] , 
xattr_35 in ['f', 'g'] ]
==>
[
xattr_36 set 'ah' , 
xattr_37 set 'bd' ].

xrule xschm_9/13 :
[
xattr_33 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_34 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l'] , 
xattr_35 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag'] ]
==>
[
xattr_36 set 'h' , 
xattr_37 set 'x' ].

xrule xschm_9/14 :
[
xattr_33 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_34 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l'] , 
xattr_35 in ['ah', 'ai', 'aj'] ]
==>
[
xattr_36 set 'ah' , 
xattr_37 set 'ae' ].

xrule xschm_9/15 :
[
xattr_33 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_34 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l'] , 
xattr_35 in ['ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] ]
==>
[
xattr_36 set 'u' , 
xattr_37 set 'at' ].

xrule xschm_9/16 :
[
xattr_33 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_34 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_35 in ['f', 'g'] ]
==>
[
xattr_36 set 'f' , 
xattr_37 set 'ae' ].

xrule xschm_9/17 :
[
xattr_33 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_34 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_35 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag'] ]
==>
[
xattr_36 set 'ai' , 
xattr_37 set 'ac' ].

xrule xschm_9/18 :
[
xattr_33 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_34 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_35 in ['ah', 'ai', 'aj'] ]
==>
[
xattr_36 set 'c' , 
xattr_37 set 'bi' ].

xrule xschm_9/19 :
[
xattr_33 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_34 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_35 in ['ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] ]
==>
[
xattr_36 set 's' , 
xattr_37 set 'ba' ].

xrule xschm_9/20 :
[
xattr_33 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_34 in ['ao', 'ap'] , 
xattr_35 in ['f', 'g'] ]
==>
[
xattr_36 set 'f' , 
xattr_37 set 'ae' ].

xrule xschm_9/21 :
[
xattr_33 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_34 in ['ao', 'ap'] , 
xattr_35 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag'] ]
==>
[
xattr_36 set 'd' , 
xattr_37 set 'af' ].

xrule xschm_9/22 :
[
xattr_33 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_34 in ['ao', 'ap'] , 
xattr_35 in ['ah', 'ai', 'aj'] ]
==>
[
xattr_36 set 'd' , 
xattr_37 set 'bd' ].

xrule xschm_9/23 :
[
xattr_33 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_34 in ['ao', 'ap'] , 
xattr_35 in ['ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] ]
==>
[
xattr_36 set 'ag' , 
xattr_37 set 'ax' ].

xrule xschm_10/0 :
[
xattr_36 in ['c', 'd', 'e'] , 
xattr_37 in ['w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] ]
==>
[
xattr_38 set 'be' , 
xattr_39 set 'v' ].

xrule xschm_10/1 :
[
xattr_36 in ['c', 'd', 'e'] , 
xattr_37 in ['bi', 'bj'] ]
==>
[
xattr_38 set 's' , 
xattr_39 set 'w' ].

xrule xschm_10/2 :
[
xattr_36 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] , 
xattr_37 in ['w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] ]
==>
[
xattr_38 set 'r' , 
xattr_39 set 't' ].

xrule xschm_10/3 :
[
xattr_36 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] , 
xattr_37 in ['bi', 'bj'] ]
==>
[
xattr_38 set 'ad' , 
xattr_39 set 'ab' ].

xrule xschm_10/4 :
[
xattr_36 in ['an', 'ao', 'ap'] , 
xattr_37 in ['w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] ]
==>
[
xattr_38 set 'bc' , 
xattr_39 set 'ar' ].

xrule xschm_10/5 :
[
xattr_36 in ['an', 'ao', 'ap'] , 
xattr_37 in ['bi', 'bj'] ]
==>
[
xattr_38 set 'ap' , 
xattr_39 set 'aq' ].

xrule xschm_11/0 :
[
xattr_38 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_39 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab'] ]
==>
[
xattr_40 set 'aa' , 
xattr_41 set 'x' , 
xattr_42 set 's' ].

xrule xschm_11/1 :
[
xattr_38 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] , 
xattr_39 in ['ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] ]
==>
[
xattr_40 set 's' , 
xattr_41 set 'ap' , 
xattr_42 set 'av' ].

xrule xschm_11/2 :
[
xattr_38 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba'] , 
xattr_39 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab'] ]
==>
[
xattr_40 set 'an' , 
xattr_41 set 'ao' , 
xattr_42 set 'ah' ].

xrule xschm_11/3 :
[
xattr_38 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba'] , 
xattr_39 in ['ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] ]
==>
[
xattr_40 set 'o' , 
xattr_41 set 'ak' , 
xattr_42 set 'as' ].

xrule xschm_11/4 :
[
xattr_38 in ['bb', 'bc', 'bd', 'be'] , 
xattr_39 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab'] ]
==>
[
xattr_40 set 'ac' , 
xattr_41 set 't' , 
xattr_42 set 'u' ].

xrule xschm_11/5 :
[
xattr_38 in ['bb', 'bc', 'bd', 'be'] , 
xattr_39 in ['ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] ]
==>
[
xattr_40 set 's' , 
xattr_41 set 'ad' , 
xattr_42 set 'af' ].

xrule xschm_12/0 :
[
xattr_40 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj'] , 
xattr_41 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p'] , 
xattr_42 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y'] ]
==>
[
xattr_43 set 'o' , 
xattr_44 set 'y' , 
xattr_45 set 'q' , 
xattr_46 set 'an' ].

xrule xschm_12/1 :
[
xattr_40 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj'] , 
xattr_41 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p'] , 
xattr_42 in ['z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax'] ]
==>
[
xattr_43 set 'j' , 
xattr_44 set 'ab' , 
xattr_45 set 'an' , 
xattr_46 set 'u' ].

xrule xschm_12/2 :
[
xattr_40 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj'] , 
xattr_41 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p'] , 
xattr_42 eq 'ay' ]
==>
[
xattr_43 set 'c' , 
xattr_44 set 'ak' , 
xattr_45 set 'aj' , 
xattr_46 set 'ad' ].

xrule xschm_12/3 :
[
xattr_40 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj'] , 
xattr_41 in ['q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_42 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y'] ]
==>
[
xattr_43 set 'n' , 
xattr_44 set 'ab' , 
xattr_45 set 'o' , 
xattr_46 set 'au' ].

xrule xschm_12/4 :
[
xattr_40 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj'] , 
xattr_41 in ['q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_42 in ['z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax'] ]
==>
[
xattr_43 set 'ai' , 
xattr_44 set 'u' , 
xattr_45 set 'ak' , 
xattr_46 set 'ai' ].

xrule xschm_12/5 :
[
xattr_40 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj'] , 
xattr_41 in ['q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_42 eq 'ay' ]
==>
[
xattr_43 set 'k' , 
xattr_44 set 'ah' , 
xattr_45 set 'n' , 
xattr_46 set 'am' ].

xrule xschm_12/6 :
[
xattr_40 in ['ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_41 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p'] , 
xattr_42 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y'] ]
==>
[
xattr_43 set 'ab' , 
xattr_44 set 'j' , 
xattr_45 set 'af' , 
xattr_46 set 'aq' ].

xrule xschm_12/7 :
[
xattr_40 in ['ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_41 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p'] , 
xattr_42 in ['z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax'] ]
==>
[
xattr_43 set 'u' , 
xattr_44 set 't' , 
xattr_45 set 'i' , 
xattr_46 set 'v' ].

xrule xschm_12/8 :
[
xattr_40 in ['ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_41 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p'] , 
xattr_42 eq 'ay' ]
==>
[
xattr_43 set 'ac' , 
xattr_44 set 'l' , 
xattr_45 set 'ag' , 
xattr_46 set 'u' ].

xrule xschm_12/9 :
[
xattr_40 in ['ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_41 in ['q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_42 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y'] ]
==>
[
xattr_43 set 'al' , 
xattr_44 set 'i' , 
xattr_45 set 'ad' , 
xattr_46 set 'o' ].

xrule xschm_12/10 :
[
xattr_40 in ['ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_41 in ['q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_42 in ['z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax'] ]
==>
[
xattr_43 set 'w' , 
xattr_44 set 'an' , 
xattr_45 set 'r' , 
xattr_46 set 'an' ].

xrule xschm_12/11 :
[
xattr_40 in ['ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_41 in ['q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_42 eq 'ay' ]
==>
[
xattr_43 set 'p' , 
xattr_44 set 'q' , 
xattr_45 set 'am' , 
xattr_46 set 'an' ].

xrule xschm_13/0 :
[
xattr_43 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_44 in ['c', 'd', 'e', 'f', 'g', 'h', 'i'] , 
xattr_45 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'be' , 
xattr_48 set 'q' ].

xrule xschm_13/1 :
[
xattr_43 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_44 in ['c', 'd', 'e', 'f', 'g', 'h', 'i'] , 
xattr_45 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'ac' , 
xattr_48 set 'r' ].

xrule xschm_13/2 :
[
xattr_43 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_44 in ['c', 'd', 'e', 'f', 'g', 'h', 'i'] , 
xattr_45 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'y' , 
xattr_48 set 'l' ].

xrule xschm_13/3 :
[
xattr_43 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_44 in ['c', 'd', 'e', 'f', 'g', 'h', 'i'] , 
xattr_45 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'an' , 
xattr_48 set 'k' ].

xrule xschm_13/4 :
[
xattr_43 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_44 in ['c', 'd', 'e', 'f', 'g', 'h', 'i'] , 
xattr_45 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'y' , 
xattr_48 set 'l' ].

xrule xschm_13/5 :
[
xattr_43 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_44 in ['c', 'd', 'e', 'f', 'g', 'h', 'i'] , 
xattr_45 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'bf' , 
xattr_48 set 'r' ].

xrule xschm_13/6 :
[
xattr_43 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_44 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_45 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'bf' , 
xattr_48 set 'u' ].

xrule xschm_13/7 :
[
xattr_43 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_44 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_45 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'ap' , 
xattr_48 set 'ai' ].

xrule xschm_13/8 :
[
xattr_43 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_44 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_45 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'ap' , 
xattr_48 set 'ao' ].

xrule xschm_13/9 :
[
xattr_43 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_44 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_45 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'ao' , 
xattr_48 set 'q' ].

xrule xschm_13/10 :
[
xattr_43 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_44 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_45 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'v' , 
xattr_48 set 'q' ].

xrule xschm_13/11 :
[
xattr_43 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_44 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_45 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'bh' , 
xattr_48 set 'o' ].

xrule xschm_13/12 :
[
xattr_43 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_44 in ['ao', 'ap'] , 
xattr_45 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'aj' , 
xattr_48 set 'j' ].

xrule xschm_13/13 :
[
xattr_43 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_44 in ['ao', 'ap'] , 
xattr_45 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'an' , 
xattr_48 set 'al' ].

xrule xschm_13/14 :
[
xattr_43 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_44 in ['ao', 'ap'] , 
xattr_45 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'at' , 
xattr_48 set 'at' ].

xrule xschm_13/15 :
[
xattr_43 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_44 in ['ao', 'ap'] , 
xattr_45 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'bd' , 
xattr_48 set 't' ].

xrule xschm_13/16 :
[
xattr_43 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_44 in ['ao', 'ap'] , 
xattr_45 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'bg' , 
xattr_48 set 'at' ].

xrule xschm_13/17 :
[
xattr_43 in ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_44 in ['ao', 'ap'] , 
xattr_45 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'ax' , 
xattr_48 set 'ac' ].

xrule xschm_13/18 :
[
xattr_43 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y'] , 
xattr_44 in ['c', 'd', 'e', 'f', 'g', 'h', 'i'] , 
xattr_45 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'bc' , 
xattr_48 set 'z' ].

xrule xschm_13/19 :
[
xattr_43 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y'] , 
xattr_44 in ['c', 'd', 'e', 'f', 'g', 'h', 'i'] , 
xattr_45 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'y' , 
xattr_48 set 'q' ].

xrule xschm_13/20 :
[
xattr_43 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y'] , 
xattr_44 in ['c', 'd', 'e', 'f', 'g', 'h', 'i'] , 
xattr_45 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'bg' , 
xattr_48 set 'ac' ].

xrule xschm_13/21 :
[
xattr_43 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y'] , 
xattr_44 in ['c', 'd', 'e', 'f', 'g', 'h', 'i'] , 
xattr_45 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'ai' , 
xattr_48 set 'r' ].

xrule xschm_13/22 :
[
xattr_43 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y'] , 
xattr_44 in ['c', 'd', 'e', 'f', 'g', 'h', 'i'] , 
xattr_45 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'am' , 
xattr_48 set 'ae' ].

xrule xschm_13/23 :
[
xattr_43 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y'] , 
xattr_44 in ['c', 'd', 'e', 'f', 'g', 'h', 'i'] , 
xattr_45 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'bf' , 
xattr_48 set 'p' ].

xrule xschm_13/24 :
[
xattr_43 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y'] , 
xattr_44 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_45 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'aj' , 
xattr_48 set 'h' ].

xrule xschm_13/25 :
[
xattr_43 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y'] , 
xattr_44 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_45 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'am' , 
xattr_48 set 'ah' ].

xrule xschm_13/26 :
[
xattr_43 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y'] , 
xattr_44 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_45 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'al' , 
xattr_48 set 'al' ].

xrule xschm_13/27 :
[
xattr_43 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y'] , 
xattr_44 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_45 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'z' , 
xattr_48 set 'aq' ].

xrule xschm_13/28 :
[
xattr_43 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y'] , 
xattr_44 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_45 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'bb' , 
xattr_48 set 't' ].

xrule xschm_13/29 :
[
xattr_43 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y'] , 
xattr_44 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_45 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'ad' , 
xattr_48 set 'am' ].

xrule xschm_13/30 :
[
xattr_43 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y'] , 
xattr_44 in ['ao', 'ap'] , 
xattr_45 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'v' , 
xattr_48 set 'au' ].

xrule xschm_13/31 :
[
xattr_43 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y'] , 
xattr_44 in ['ao', 'ap'] , 
xattr_45 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'ax' , 
xattr_48 set 'ag' ].

xrule xschm_13/32 :
[
xattr_43 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y'] , 
xattr_44 in ['ao', 'ap'] , 
xattr_45 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'bb' , 
xattr_48 set 'ak' ].

xrule xschm_13/33 :
[
xattr_43 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y'] , 
xattr_44 in ['ao', 'ap'] , 
xattr_45 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'ay' , 
xattr_48 set 'ah' ].

xrule xschm_13/34 :
[
xattr_43 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y'] , 
xattr_44 in ['ao', 'ap'] , 
xattr_45 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'ar' , 
xattr_48 set 'ab' ].

xrule xschm_13/35 :
[
xattr_43 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y'] , 
xattr_44 in ['ao', 'ap'] , 
xattr_45 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'aj' , 
xattr_48 set 'h' ].

xrule xschm_13/36 :
[
xattr_43 in ['z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] , 
xattr_44 in ['c', 'd', 'e', 'f', 'g', 'h', 'i'] , 
xattr_45 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'bc' , 
xattr_48 set 'm' ].

xrule xschm_13/37 :
[
xattr_43 in ['z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] , 
xattr_44 in ['c', 'd', 'e', 'f', 'g', 'h', 'i'] , 
xattr_45 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'af' , 
xattr_48 set 'y' ].

xrule xschm_13/38 :
[
xattr_43 in ['z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] , 
xattr_44 in ['c', 'd', 'e', 'f', 'g', 'h', 'i'] , 
xattr_45 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'ab' , 
xattr_48 set 'aa' ].

xrule xschm_13/39 :
[
xattr_43 in ['z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] , 
xattr_44 in ['c', 'd', 'e', 'f', 'g', 'h', 'i'] , 
xattr_45 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'al' , 
xattr_48 set 'z' ].

xrule xschm_13/40 :
[
xattr_43 in ['z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] , 
xattr_44 in ['c', 'd', 'e', 'f', 'g', 'h', 'i'] , 
xattr_45 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'y' , 
xattr_48 set 'ar' ].

xrule xschm_13/41 :
[
xattr_43 in ['z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] , 
xattr_44 in ['c', 'd', 'e', 'f', 'g', 'h', 'i'] , 
xattr_45 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'ah' , 
xattr_48 set 'af' ].

xrule xschm_13/42 :
[
xattr_43 in ['z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] , 
xattr_44 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_45 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'bc' , 
xattr_48 set 's' ].

xrule xschm_13/43 :
[
xattr_43 in ['z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] , 
xattr_44 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_45 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'ba' , 
xattr_48 set 'r' ].

xrule xschm_13/44 :
[
xattr_43 in ['z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] , 
xattr_44 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_45 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'at' , 
xattr_48 set 'ad' ].

xrule xschm_13/45 :
[
xattr_43 in ['z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] , 
xattr_44 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_45 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'aq' , 
xattr_48 set 'ag' ].

xrule xschm_13/46 :
[
xattr_43 in ['z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] , 
xattr_44 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_45 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'af' , 
xattr_48 set 's' ].

xrule xschm_13/47 :
[
xattr_43 in ['z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] , 
xattr_44 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_45 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'ap' , 
xattr_48 set 'at' ].

xrule xschm_13/48 :
[
xattr_43 in ['z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] , 
xattr_44 in ['ao', 'ap'] , 
xattr_45 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'w' , 
xattr_48 set 'ap' ].

xrule xschm_13/49 :
[
xattr_43 in ['z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] , 
xattr_44 in ['ao', 'ap'] , 
xattr_45 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'z' , 
xattr_48 set 'al' ].

xrule xschm_13/50 :
[
xattr_43 in ['z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] , 
xattr_44 in ['ao', 'ap'] , 
xattr_45 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'ax' , 
xattr_48 set 'w' ].

xrule xschm_13/51 :
[
xattr_43 in ['z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] , 
xattr_44 in ['ao', 'ap'] , 
xattr_45 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'af' , 
xattr_48 set 'l' ].

xrule xschm_13/52 :
[
xattr_43 in ['z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] , 
xattr_44 in ['ao', 'ap'] , 
xattr_45 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'ap' , 
xattr_48 set 'p' ].

xrule xschm_13/53 :
[
xattr_43 in ['z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] , 
xattr_44 in ['ao', 'ap'] , 
xattr_45 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'ab' , 
xattr_48 set 'aa' ].

xrule xschm_13/54 :
[
xattr_43 in ['am', 'an', 'ao'] , 
xattr_44 in ['c', 'd', 'e', 'f', 'g', 'h', 'i'] , 
xattr_45 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'ar' , 
xattr_48 set 'u' ].

xrule xschm_13/55 :
[
xattr_43 in ['am', 'an', 'ao'] , 
xattr_44 in ['c', 'd', 'e', 'f', 'g', 'h', 'i'] , 
xattr_45 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'aa' , 
xattr_48 set 'at' ].

xrule xschm_13/56 :
[
xattr_43 in ['am', 'an', 'ao'] , 
xattr_44 in ['c', 'd', 'e', 'f', 'g', 'h', 'i'] , 
xattr_45 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'bc' , 
xattr_48 set 'p' ].

xrule xschm_13/57 :
[
xattr_43 in ['am', 'an', 'ao'] , 
xattr_44 in ['c', 'd', 'e', 'f', 'g', 'h', 'i'] , 
xattr_45 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'w' , 
xattr_48 set 'au' ].

xrule xschm_13/58 :
[
xattr_43 in ['am', 'an', 'ao'] , 
xattr_44 in ['c', 'd', 'e', 'f', 'g', 'h', 'i'] , 
xattr_45 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'ab' , 
xattr_48 set 'j' ].

xrule xschm_13/59 :
[
xattr_43 in ['am', 'an', 'ao'] , 
xattr_44 in ['c', 'd', 'e', 'f', 'g', 'h', 'i'] , 
xattr_45 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'ao' , 
xattr_48 set 'an' ].

xrule xschm_13/60 :
[
xattr_43 in ['am', 'an', 'ao'] , 
xattr_44 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_45 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'bb' , 
xattr_48 set 'al' ].

xrule xschm_13/61 :
[
xattr_43 in ['am', 'an', 'ao'] , 
xattr_44 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_45 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'ao' , 
xattr_48 set 'ao' ].

xrule xschm_13/62 :
[
xattr_43 in ['am', 'an', 'ao'] , 
xattr_44 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_45 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'au' , 
xattr_48 set 'ae' ].

xrule xschm_13/63 :
[
xattr_43 in ['am', 'an', 'ao'] , 
xattr_44 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_45 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'ar' , 
xattr_48 set 'n' ].

xrule xschm_13/64 :
[
xattr_43 in ['am', 'an', 'ao'] , 
xattr_44 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_45 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'bf' , 
xattr_48 set 'j' ].

xrule xschm_13/65 :
[
xattr_43 in ['am', 'an', 'ao'] , 
xattr_44 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_45 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'ar' , 
xattr_48 set 'ar' ].

xrule xschm_13/66 :
[
xattr_43 in ['am', 'an', 'ao'] , 
xattr_44 in ['ao', 'ap'] , 
xattr_45 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'ae' , 
xattr_48 set 'w' ].

xrule xschm_13/67 :
[
xattr_43 in ['am', 'an', 'ao'] , 
xattr_44 in ['ao', 'ap'] , 
xattr_45 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'al' , 
xattr_48 set 'q' ].

xrule xschm_13/68 :
[
xattr_43 in ['am', 'an', 'ao'] , 
xattr_44 in ['ao', 'ap'] , 
xattr_45 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'af' , 
xattr_48 set 'ap' ].

xrule xschm_13/69 :
[
xattr_43 in ['am', 'an', 'ao'] , 
xattr_44 in ['ao', 'ap'] , 
xattr_45 in ['p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'as' , 
xattr_48 set 'ak' ].

xrule xschm_13/70 :
[
xattr_43 in ['am', 'an', 'ao'] , 
xattr_44 in ['ao', 'ap'] , 
xattr_45 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_46 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] ]
==>
[
xattr_47 set 'ai' , 
xattr_48 set 'x' ].

xrule xschm_13/71 :
[
xattr_43 in ['am', 'an', 'ao'] , 
xattr_44 in ['ao', 'ap'] , 
xattr_45 in ['ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_46 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_47 set 'ab' , 
xattr_48 set 'an' ].

xrule xschm_14/0 :
[
xattr_47 in ['u', 'v', 'w', 'x'] , 
xattr_48 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] ]
==>
[
xattr_49 set 'am' , 
xattr_50 set 'ac' , 
xattr_51 set 'ao' ].

xrule xschm_14/1 :
[
xattr_47 in ['u', 'v', 'w', 'x'] , 
xattr_48 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah'] ]
==>
[
xattr_49 set 'ak' , 
xattr_50 set 'ap' , 
xattr_51 set 'u' ].

xrule xschm_14/2 :
[
xattr_47 in ['u', 'v', 'w', 'x'] , 
xattr_48 in ['ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] ]
==>
[
xattr_49 set 'r' , 
xattr_50 set 'at' , 
xattr_51 set 's' ].

xrule xschm_14/3 :
[
xattr_47 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be'] , 
xattr_48 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] ]
==>
[
xattr_49 set 'ar' , 
xattr_50 set 'ac' , 
xattr_51 set 'p' ].

xrule xschm_14/4 :
[
xattr_47 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be'] , 
xattr_48 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah'] ]
==>
[
xattr_49 set 'v' , 
xattr_50 set 'ai' , 
xattr_51 set 'al' ].

xrule xschm_14/5 :
[
xattr_47 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be'] , 
xattr_48 in ['ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] ]
==>
[
xattr_49 set 'ay' , 
xattr_50 set 'z' , 
xattr_51 set 'u' ].

xrule xschm_14/6 :
[
xattr_47 in ['bf', 'bg', 'bh'] , 
xattr_48 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'] ]
==>
[
xattr_49 set 'ac' , 
xattr_50 set 'at' , 
xattr_51 set 'am' ].

xrule xschm_14/7 :
[
xattr_47 in ['bf', 'bg', 'bh'] , 
xattr_48 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah'] ]
==>
[
xattr_49 set 'aj' , 
xattr_50 set 'ba' , 
xattr_51 set 'an' ].

xrule xschm_14/8 :
[
xattr_47 in ['bf', 'bg', 'bh'] , 
xattr_48 in ['ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] ]
==>
[
xattr_49 set 'aj' , 
xattr_50 set 'bh' , 
xattr_51 set 'w' ].

xrule xschm_15/0 :
[
xattr_49 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw'] , 
xattr_50 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be'] , 
xattr_51 in ['n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] ]
==>
[
xattr_52 set 'p' , 
xattr_53 set 'as' ].

xrule xschm_15/1 :
[
xattr_49 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw'] , 
xattr_50 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be'] , 
xattr_51 in ['at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba'] ]
==>
[
xattr_52 set 'am' , 
xattr_53 set 'ad' ].

xrule xschm_15/2 :
[
xattr_49 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw'] , 
xattr_50 in ['bf', 'bg', 'bh'] , 
xattr_51 in ['n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] ]
==>
[
xattr_52 set 'ap' , 
xattr_53 set 'ag' ].

xrule xschm_15/3 :
[
xattr_49 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw'] , 
xattr_50 in ['bf', 'bg', 'bh'] , 
xattr_51 in ['at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba'] ]
==>
[
xattr_52 set 'v' , 
xattr_53 set 'ax' ].

xrule xschm_15/4 :
[
xattr_49 in ['ax', 'ay'] , 
xattr_50 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be'] , 
xattr_51 in ['n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] ]
==>
[
xattr_52 set 's' , 
xattr_53 set 'ab' ].

xrule xschm_15/5 :
[
xattr_49 in ['ax', 'ay'] , 
xattr_50 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be'] , 
xattr_51 in ['at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba'] ]
==>
[
xattr_52 set 'n' , 
xattr_53 set 'am' ].

xrule xschm_15/6 :
[
xattr_49 in ['ax', 'ay'] , 
xattr_50 in ['bf', 'bg', 'bh'] , 
xattr_51 in ['n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] ]
==>
[
xattr_52 set 'n' , 
xattr_53 set 'af' ].

xrule xschm_15/7 :
[
xattr_49 in ['ax', 'ay'] , 
xattr_50 in ['bf', 'bg', 'bh'] , 
xattr_51 in ['at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba'] ]
==>
[
xattr_52 set 't' , 
xattr_53 set 's' ].

xrule xschm_16/0 :
[
xattr_52 in ['n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_53 in ['m', 'n', 'o', 'p'] ]
==>
[
xattr_54 set 'l' , 
xattr_55 set 'be' , 
xattr_56 set 'w' ].

xrule xschm_16/1 :
[
xattr_52 in ['n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_53 in ['q', 'r', 's', 't', 'u'] ]
==>
[
xattr_54 set 'an' , 
xattr_55 set 'ar' , 
xattr_56 set 'z' ].

xrule xschm_16/2 :
[
xattr_52 in ['n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_53 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj'] ]
==>
[
xattr_54 set 'aa' , 
xattr_55 set 'at' , 
xattr_56 set 'y' ].

xrule xschm_16/3 :
[
xattr_52 in ['n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_53 in ['ak', 'al', 'am', 'an', 'ao', 'ap'] ]
==>
[
xattr_54 set 'z' , 
xattr_55 set 'bj' , 
xattr_56 set 'ai' ].

xrule xschm_16/4 :
[
xattr_52 in ['n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_53 in ['aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_54 set 'y' , 
xattr_55 set 'av' , 
xattr_56 set 'ak' ].

xrule xschm_16/5 :
[
xattr_52 in ['av', 'aw', 'ax', 'ay', 'az', 'ba'] , 
xattr_53 in ['m', 'n', 'o', 'p'] ]
==>
[
xattr_54 set 'ax' , 
xattr_55 set 'af' , 
xattr_56 set 'ai' ].

xrule xschm_16/6 :
[
xattr_52 in ['av', 'aw', 'ax', 'ay', 'az', 'ba'] , 
xattr_53 in ['q', 'r', 's', 't', 'u'] ]
==>
[
xattr_54 set 'ae' , 
xattr_55 set 'av' , 
xattr_56 set 'av' ].

xrule xschm_16/7 :
[
xattr_52 in ['av', 'aw', 'ax', 'ay', 'az', 'ba'] , 
xattr_53 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj'] ]
==>
[
xattr_54 set 'u' , 
xattr_55 set 'ba' , 
xattr_56 set 'am' ].

xrule xschm_16/8 :
[
xattr_52 in ['av', 'aw', 'ax', 'ay', 'az', 'ba'] , 
xattr_53 in ['ak', 'al', 'am', 'an', 'ao', 'ap'] ]
==>
[
xattr_54 set 'av' , 
xattr_55 set 'bf' , 
xattr_56 set 'bd' ].

xrule xschm_16/9 :
[
xattr_52 in ['av', 'aw', 'ax', 'ay', 'az', 'ba'] , 
xattr_53 in ['aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_54 set 'am' , 
xattr_55 set 'az' , 
xattr_56 set 'w' ].

xrule xschm_17/0 :
[
xattr_54 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_55 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_56 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] ]
==>
[
xattr_57 set 'bd' , 
xattr_58 set 'af' , 
xattr_59 set 'ak' ].

xrule xschm_17/1 :
[
xattr_54 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_55 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_56 in ['an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] ]
==>
[
xattr_57 set 'aw' , 
xattr_58 set 'u' , 
xattr_59 set 'aa' ].

xrule xschm_17/2 :
[
xattr_54 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_55 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_56 eq 'be' ]
==>
[
xattr_57 set 'aq' , 
xattr_58 set 'ba' , 
xattr_59 set 'z' ].

xrule xschm_17/3 :
[
xattr_54 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_55 in ['av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk', 'bl'] , 
xattr_56 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] ]
==>
[
xattr_57 set 't' , 
xattr_58 set 'ah' , 
xattr_59 set 'bg' ].

xrule xschm_17/4 :
[
xattr_54 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_55 in ['av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk', 'bl'] , 
xattr_56 in ['an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] ]
==>
[
xattr_57 set 'at' , 
xattr_58 set 'x' , 
xattr_59 set 'aw' ].

xrule xschm_17/5 :
[
xattr_54 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_55 in ['av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk', 'bl'] , 
xattr_56 eq 'be' ]
==>
[
xattr_57 set 'aw' , 
xattr_58 set 'av' , 
xattr_59 set 'au' ].

xrule xschm_17/6 :
[
xattr_54 in ['av', 'aw', 'ax', 'ay'] , 
xattr_55 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_56 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] ]
==>
[
xattr_57 set 'z' , 
xattr_58 set 'ay' , 
xattr_59 set 'av' ].

xrule xschm_17/7 :
[
xattr_54 in ['av', 'aw', 'ax', 'ay'] , 
xattr_55 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_56 in ['an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] ]
==>
[
xattr_57 set 'y' , 
xattr_58 set 'w' , 
xattr_59 set 'bf' ].

xrule xschm_17/8 :
[
xattr_54 in ['av', 'aw', 'ax', 'ay'] , 
xattr_55 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_56 eq 'be' ]
==>
[
xattr_57 set 'ai' , 
xattr_58 set 'ah' , 
xattr_59 set 'ao' ].

xrule xschm_17/9 :
[
xattr_54 in ['av', 'aw', 'ax', 'ay'] , 
xattr_55 in ['av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk', 'bl'] , 
xattr_56 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] ]
==>
[
xattr_57 set 'ax' , 
xattr_58 set 'ac' , 
xattr_59 set 'aa' ].

xrule xschm_17/10 :
[
xattr_54 in ['av', 'aw', 'ax', 'ay'] , 
xattr_55 in ['av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk', 'bl'] , 
xattr_56 in ['an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd'] ]
==>
[
xattr_57 set 'bc' , 
xattr_58 set 'u' , 
xattr_59 set 'am' ].

xrule xschm_17/11 :
[
xattr_54 in ['av', 'aw', 'ax', 'ay'] , 
xattr_55 in ['av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk', 'bl'] , 
xattr_56 eq 'be' ]
==>
[
xattr_57 set 'ab' , 
xattr_58 set 'ap' , 
xattr_59 set 'w' ].

xrule xschm_18/0 :
[
xattr_57 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_58 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_59 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_60 set 'ah' , 
xattr_61 set 'ad' ].

xrule xschm_18/1 :
[
xattr_57 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_58 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_59 in ['az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] ]
==>
[
xattr_60 set 'af' , 
xattr_61 set 'ak' ].

xrule xschm_18/2 :
[
xattr_57 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_58 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_59 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_60 set 'u' , 
xattr_61 set 'ah' ].

xrule xschm_18/3 :
[
xattr_57 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_58 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_59 in ['az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] ]
==>
[
xattr_60 set 't' , 
xattr_61 set 'w' ].

xrule xschm_18/4 :
[
xattr_57 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_58 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_59 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_60 set 'ak' , 
xattr_61 set 'aa' ].

xrule xschm_18/5 :
[
xattr_57 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_58 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_59 in ['az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] ]
==>
[
xattr_60 set 'ak' , 
xattr_61 set 'bf' ].

xrule xschm_18/6 :
[
xattr_57 in ['ad', 'ae', 'af', 'ag'] , 
xattr_58 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_59 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_60 set 'ag' , 
xattr_61 set 'v' ].

xrule xschm_18/7 :
[
xattr_57 in ['ad', 'ae', 'af', 'ag'] , 
xattr_58 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_59 in ['az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] ]
==>
[
xattr_60 set 'v' , 
xattr_61 set 'as' ].

xrule xschm_18/8 :
[
xattr_57 in ['ad', 'ae', 'af', 'ag'] , 
xattr_58 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_59 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_60 set 'as' , 
xattr_61 set 'az' ].

xrule xschm_18/9 :
[
xattr_57 in ['ad', 'ae', 'af', 'ag'] , 
xattr_58 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_59 in ['az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] ]
==>
[
xattr_60 set 't' , 
xattr_61 set 'be' ].

xrule xschm_18/10 :
[
xattr_57 in ['ad', 'ae', 'af', 'ag'] , 
xattr_58 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_59 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_60 set 'y' , 
xattr_61 set 'bb' ].

xrule xschm_18/11 :
[
xattr_57 in ['ad', 'ae', 'af', 'ag'] , 
xattr_58 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_59 in ['az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] ]
==>
[
xattr_60 set 'x' , 
xattr_61 set 'ao' ].

xrule xschm_18/12 :
[
xattr_57 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_58 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_59 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_60 set 'an' , 
xattr_61 set 'be' ].

xrule xschm_18/13 :
[
xattr_57 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_58 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_59 in ['az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] ]
==>
[
xattr_60 set 'an' , 
xattr_61 set 'ay' ].

xrule xschm_18/14 :
[
xattr_57 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_58 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_59 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_60 set 'af' , 
xattr_61 set 'ag' ].

xrule xschm_18/15 :
[
xattr_57 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_58 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_59 in ['az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] ]
==>
[
xattr_60 set 'ab' , 
xattr_61 set 'aa' ].

xrule xschm_18/16 :
[
xattr_57 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_58 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_59 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_60 set 'ao' , 
xattr_61 set 'y' ].

xrule xschm_18/17 :
[
xattr_57 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_58 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_59 in ['az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] ]
==>
[
xattr_60 set 'an' , 
xattr_61 set 'ak' ].

xrule xschm_18/18 :
[
xattr_57 in ['ap', 'aq', 'ar', 'as'] , 
xattr_58 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_59 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_60 set 's' , 
xattr_61 set 'av' ].

xrule xschm_18/19 :
[
xattr_57 in ['ap', 'aq', 'ar', 'as'] , 
xattr_58 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_59 in ['az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] ]
==>
[
xattr_60 set 'ai' , 
xattr_61 set 'bc' ].

xrule xschm_18/20 :
[
xattr_57 in ['ap', 'aq', 'ar', 'as'] , 
xattr_58 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_59 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_60 set 'p' , 
xattr_61 set 'bc' ].

xrule xschm_18/21 :
[
xattr_57 in ['ap', 'aq', 'ar', 'as'] , 
xattr_58 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_59 in ['az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] ]
==>
[
xattr_60 set 'ah' , 
xattr_61 set 'y' ].

xrule xschm_18/22 :
[
xattr_57 in ['ap', 'aq', 'ar', 'as'] , 
xattr_58 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_59 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_60 set 'k' , 
xattr_61 set 'am' ].

xrule xschm_18/23 :
[
xattr_57 in ['ap', 'aq', 'ar', 'as'] , 
xattr_58 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_59 in ['az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] ]
==>
[
xattr_60 set 'ag' , 
xattr_61 set 'u' ].

xrule xschm_18/24 :
[
xattr_57 in ['at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_58 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_59 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_60 set 'ab' , 
xattr_61 set 'an' ].

xrule xschm_18/25 :
[
xattr_57 in ['at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_58 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad'] , 
xattr_59 in ['az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] ]
==>
[
xattr_60 set 'w' , 
xattr_61 set 'ag' ].

xrule xschm_18/26 :
[
xattr_57 in ['at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_58 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_59 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_60 set 'r' , 
xattr_61 set 'ar' ].

xrule xschm_18/27 :
[
xattr_57 in ['at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_58 in ['ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_59 in ['az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] ]
==>
[
xattr_60 set 'ac' , 
xattr_61 set 'aq' ].

xrule xschm_18/28 :
[
xattr_57 in ['at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_58 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_59 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] ]
==>
[
xattr_60 set 's' , 
xattr_61 set 'aa' ].

xrule xschm_18/29 :
[
xattr_57 in ['at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_58 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_59 in ['az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] ]
==>
[
xattr_60 set 't' , 
xattr_61 set 'az' ].

xrule xschm_19/0 :
[
xattr_60 in ['h', 'i', 'j'] , 
xattr_61 in ['s', 't', 'u'] ]
==>
[
xattr_62 set 'n' , 
xattr_63 set 'bc' ].

xrule xschm_19/1 :
[
xattr_60 in ['h', 'i', 'j'] , 
xattr_61 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] ]
==>
[
xattr_62 set 'ai' , 
xattr_63 set 'ak' ].

xrule xschm_19/2 :
[
xattr_60 in ['h', 'i', 'j'] , 
xattr_61 in ['au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_62 set 'ag' , 
xattr_63 set 'am' ].

xrule xschm_19/3 :
[
xattr_60 in ['h', 'i', 'j'] , 
xattr_61 in ['ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_62 set 'g' , 
xattr_63 set 'at' ].

xrule xschm_19/4 :
[
xattr_60 in ['k', 'l', 'm', 'n', 'o', 'p', 'q'] , 
xattr_61 in ['s', 't', 'u'] ]
==>
[
xattr_62 set 'al' , 
xattr_63 set 'bl' ].

xrule xschm_19/5 :
[
xattr_60 in ['k', 'l', 'm', 'n', 'o', 'p', 'q'] , 
xattr_61 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] ]
==>
[
xattr_62 set 'n' , 
xattr_63 set 'ad' ].

xrule xschm_19/6 :
[
xattr_60 in ['k', 'l', 'm', 'n', 'o', 'p', 'q'] , 
xattr_61 in ['au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_62 set 'aa' , 
xattr_63 set 'bc' ].

xrule xschm_19/7 :
[
xattr_60 in ['k', 'l', 'm', 'n', 'o', 'p', 'q'] , 
xattr_61 in ['ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_62 set 'c' , 
xattr_63 set 'ab' ].

xrule xschm_19/8 :
[
xattr_60 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'] , 
xattr_61 in ['s', 't', 'u'] ]
==>
[
xattr_62 set 'am' , 
xattr_63 set 'ar' ].

xrule xschm_19/9 :
[
xattr_60 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'] , 
xattr_61 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] ]
==>
[
xattr_62 set 'v' , 
xattr_63 set 'ab' ].

xrule xschm_19/10 :
[
xattr_60 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'] , 
xattr_61 in ['au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_62 set 'h' , 
xattr_63 set 'be' ].

xrule xschm_19/11 :
[
xattr_60 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'] , 
xattr_61 in ['ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_62 set 'ab' , 
xattr_63 set 'ba' ].

xrule xschm_19/12 :
[
xattr_60 in ['aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] , 
xattr_61 in ['s', 't', 'u'] ]
==>
[
xattr_62 set 'j' , 
xattr_63 set 'bd' ].

xrule xschm_19/13 :
[
xattr_60 in ['aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] , 
xattr_61 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] ]
==>
[
xattr_62 set 'y' , 
xattr_63 set 'ae' ].

xrule xschm_19/14 :
[
xattr_60 in ['aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] , 
xattr_61 in ['au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_62 set 'p' , 
xattr_63 set 'ap' ].

xrule xschm_19/15 :
[
xattr_60 in ['aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] , 
xattr_61 in ['ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_62 set 'ak' , 
xattr_63 set 'bn' ].

xrule xschm_19/16 :
[
xattr_60 in ['an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_61 in ['s', 't', 'u'] ]
==>
[
xattr_62 set 'ah' , 
xattr_63 set 'bg' ].

xrule xschm_19/17 :
[
xattr_60 in ['an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_61 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] ]
==>
[
xattr_62 set 'an' , 
xattr_63 set 'be' ].

xrule xschm_19/18 :
[
xattr_60 in ['an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_61 in ['au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_62 set 'k' , 
xattr_63 set 'aq' ].

xrule xschm_19/19 :
[
xattr_60 in ['an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_61 in ['ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_62 set 'k' , 
xattr_63 set 'ak' ].

xrule xschm_19/20 :
[
xattr_60 eq 'au' , 
xattr_61 in ['s', 't', 'u'] ]
==>
[
xattr_62 set 'e' , 
xattr_63 set 'an' ].

xrule xschm_19/21 :
[
xattr_60 eq 'au' , 
xattr_61 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] ]
==>
[
xattr_62 set 'ak' , 
xattr_63 set 'bk' ].

xrule xschm_19/22 :
[
xattr_60 eq 'au' , 
xattr_61 in ['au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_62 set 'c' , 
xattr_63 set 'am' ].

xrule xschm_19/23 :
[
xattr_60 eq 'au' , 
xattr_61 in ['ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_62 set 'al' , 
xattr_63 set 'as' ].

xrule xschm_20/0 :
[
xattr_62 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_63 in ['aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] ]
==>
[
xattr_64 set 'r' , 
xattr_65 set 'aw' , 
xattr_66 set 'ac' ].

xrule xschm_20/1 :
[
xattr_62 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_63 in ['an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax'] ]
==>
[
xattr_64 set 'as' , 
xattr_65 set 'am' , 
xattr_66 set 'aq' ].

xrule xschm_20/2 :
[
xattr_62 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] , 
xattr_63 in ['ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk', 'bl', 'bm', 'bn'] ]
==>
[
xattr_64 set 'at' , 
xattr_65 set 'an' , 
xattr_66 set 'x' ].

xrule xschm_20/3 :
[
xattr_62 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_63 in ['aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] ]
==>
[
xattr_64 set 'ab' , 
xattr_65 set 'ai' , 
xattr_66 set 'o' ].

xrule xschm_20/4 :
[
xattr_62 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_63 in ['an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax'] ]
==>
[
xattr_64 set 'aa' , 
xattr_65 set 'ai' , 
xattr_66 set 'k' ].

xrule xschm_20/5 :
[
xattr_62 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] , 
xattr_63 in ['ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk', 'bl', 'bm', 'bn'] ]
==>
[
xattr_64 set 'ae' , 
xattr_65 set 'ay' , 
xattr_66 set 'k' ].

xrule xschm_21/0 :
[
xattr_64 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_65 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] , 
xattr_66 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's'] ]
==>
[
xattr_67 set 'aa' , 
xattr_68 set 'aq' , 
xattr_69 set 'w' ].

xrule xschm_21/1 :
[
xattr_64 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_65 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] , 
xattr_66 in ['t', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] ]
==>
[
xattr_67 set 'ax' , 
xattr_68 set 'ab' , 
xattr_69 set 'i' ].

xrule xschm_21/2 :
[
xattr_64 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_65 in ['bd', 'be'] , 
xattr_66 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's'] ]
==>
[
xattr_67 set 'ad' , 
xattr_68 set 'l' , 
xattr_69 set 'y' ].

xrule xschm_21/3 :
[
xattr_64 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_65 in ['bd', 'be'] , 
xattr_66 in ['t', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] ]
==>
[
xattr_67 set 'bb' , 
xattr_68 set 'aw' , 
xattr_69 set 'ac' ].

xrule xschm_21/4 :
[
xattr_64 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_65 eq 'bf' , 
xattr_66 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's'] ]
==>
[
xattr_67 set 'ax' , 
xattr_68 set 'ah' , 
xattr_69 set 'z' ].

xrule xschm_21/5 :
[
xattr_64 in ['r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] , 
xattr_65 eq 'bf' , 
xattr_66 in ['t', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] ]
==>
[
xattr_67 set 'ap' , 
xattr_68 set 'ap' , 
xattr_69 set 'j' ].

xrule xschm_21/6 :
[
xattr_64 in ['av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be'] , 
xattr_65 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] , 
xattr_66 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's'] ]
==>
[
xattr_67 set 'au' , 
xattr_68 set 'x' , 
xattr_69 set 't' ].

xrule xschm_21/7 :
[
xattr_64 in ['av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be'] , 
xattr_65 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] , 
xattr_66 in ['t', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] ]
==>
[
xattr_67 set 'av' , 
xattr_68 set 'q' , 
xattr_69 set 'q' ].

xrule xschm_21/8 :
[
xattr_64 in ['av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be'] , 
xattr_65 in ['bd', 'be'] , 
xattr_66 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's'] ]
==>
[
xattr_67 set 'be' , 
xattr_68 set 'o' , 
xattr_69 set 'j' ].

xrule xschm_21/9 :
[
xattr_64 in ['av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be'] , 
xattr_65 in ['bd', 'be'] , 
xattr_66 in ['t', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] ]
==>
[
xattr_67 set 'ao' , 
xattr_68 set 'ai' , 
xattr_69 set 'n' ].

xrule xschm_21/10 :
[
xattr_64 in ['av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be'] , 
xattr_65 eq 'bf' , 
xattr_66 in ['d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's'] ]
==>
[
xattr_67 set 'u' , 
xattr_68 set 'ae' , 
xattr_69 set 'ah' ].

xrule xschm_21/11 :
[
xattr_64 in ['av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be'] , 
xattr_65 eq 'bf' , 
xattr_66 in ['t', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] ]
==>
[
xattr_67 set 'aj' , 
xattr_68 set 'y' , 
xattr_69 set 'ap' ].

xrule xschm_22/0 :
[
xattr_67 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_68 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae'] , 
xattr_69 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] ]
==>
[
xattr_70 set 't' , 
xattr_71 set 'u' , 
xattr_72 set 'ao' ].

xrule xschm_22/1 :
[
xattr_67 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_68 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae'] , 
xattr_69 eq 'as' ]
==>
[
xattr_70 set 'u' , 
xattr_71 set 'n' , 
xattr_72 set 'ar' ].

xrule xschm_22/2 :
[
xattr_67 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_68 in ['af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_69 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] ]
==>
[
xattr_70 set 'am' , 
xattr_71 set 'aj' , 
xattr_72 set 'x' ].

xrule xschm_22/3 :
[
xattr_67 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_68 in ['af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_69 eq 'as' ]
==>
[
xattr_70 set 'ac' , 
xattr_71 set 'u' , 
xattr_72 set 'x' ].

xrule xschm_22/4 :
[
xattr_67 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_68 in ['at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_69 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] ]
==>
[
xattr_70 set 'z' , 
xattr_71 set 'aa' , 
xattr_72 set 'ah' ].

xrule xschm_22/5 :
[
xattr_67 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_68 in ['at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_69 eq 'as' ]
==>
[
xattr_70 set 'at' , 
xattr_71 set 't' , 
xattr_72 set 'ab' ].

xrule xschm_22/6 :
[
xattr_67 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_68 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae'] , 
xattr_69 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] ]
==>
[
xattr_70 set 'z' , 
xattr_71 set 'ao' , 
xattr_72 set 'ak' ].

xrule xschm_22/7 :
[
xattr_67 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_68 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae'] , 
xattr_69 eq 'as' ]
==>
[
xattr_70 set 'ai' , 
xattr_71 set 'u' , 
xattr_72 set 'ad' ].

xrule xschm_22/8 :
[
xattr_67 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_68 in ['af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_69 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] ]
==>
[
xattr_70 set 'r' , 
xattr_71 set 'o' , 
xattr_72 set 'ap' ].

xrule xschm_22/9 :
[
xattr_67 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_68 in ['af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_69 eq 'as' ]
==>
[
xattr_70 set 'al' , 
xattr_71 set 'q' , 
xattr_72 set 'ag' ].

xrule xschm_22/10 :
[
xattr_67 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_68 in ['at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_69 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar'] ]
==>
[
xattr_70 set 'at' , 
xattr_71 set 'ae' , 
xattr_72 set 'ab' ].

xrule xschm_22/11 :
[
xattr_67 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_68 in ['at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_69 eq 'as' ]
==>
[
xattr_70 set 'au' , 
xattr_71 set 'u' , 
xattr_72 set 'ad' ].

xrule xschm_23/0 :
[
xattr_70 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_71 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_72 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] ]
==>
[
xattr_73 set 'ao' , 
xattr_74 set 'o' , 
xattr_75 set 'ag' , 
xattr_76 set 'ah' ].

xrule xschm_23/1 :
[
xattr_70 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_71 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_72 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] ]
==>
[
xattr_73 set 'v' , 
xattr_74 set 'ap' , 
xattr_75 set 'aa' , 
xattr_76 set 'bd' ].

xrule xschm_23/2 :
[
xattr_70 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_71 eq 'ap' , 
xattr_72 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] ]
==>
[
xattr_73 set 'ax' , 
xattr_74 set 'o' , 
xattr_75 set 'ac' , 
xattr_76 set 'x' ].

xrule xschm_23/3 :
[
xattr_70 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_71 eq 'ap' , 
xattr_72 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] ]
==>
[
xattr_73 set 'v' , 
xattr_74 set 'j' , 
xattr_75 set 'au' , 
xattr_76 set 'aw' ].

xrule xschm_23/4 :
[
xattr_70 in ['au', 'av'] , 
xattr_71 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_72 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] ]
==>
[
xattr_73 set 'aa' , 
xattr_74 set 'v' , 
xattr_75 set 'ag' , 
xattr_76 set 'ad' ].

xrule xschm_23/5 :
[
xattr_70 in ['au', 'av'] , 
xattr_71 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_72 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] ]
==>
[
xattr_73 set 't' , 
xattr_74 set 'ae' , 
xattr_75 set 'bb' , 
xattr_76 set 'ah' ].

xrule xschm_23/6 :
[
xattr_70 in ['au', 'av'] , 
xattr_71 eq 'ap' , 
xattr_72 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] ]
==>
[
xattr_73 set 'as' , 
xattr_74 set 'u' , 
xattr_75 set 'ak' , 
xattr_76 set 'ac' ].

xrule xschm_23/7 :
[
xattr_70 in ['au', 'av'] , 
xattr_71 eq 'ap' , 
xattr_72 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] ]
==>
[
xattr_73 set 'aq' , 
xattr_74 set 't' , 
xattr_75 set 'ar' , 
xattr_76 set 'ak' ].

xrule xschm_23/8 :
[
xattr_70 in ['aw', 'ax', 'ay'] , 
xattr_71 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_72 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] ]
==>
[
xattr_73 set 'z' , 
xattr_74 set 'i' , 
xattr_75 set 'aj' , 
xattr_76 set 'av' ].

xrule xschm_23/9 :
[
xattr_70 in ['aw', 'ax', 'ay'] , 
xattr_71 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_72 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] ]
==>
[
xattr_73 set 'ac' , 
xattr_74 set 'ae' , 
xattr_75 set 't' , 
xattr_76 set 'ac' ].

xrule xschm_23/10 :
[
xattr_70 in ['aw', 'ax', 'ay'] , 
xattr_71 eq 'ap' , 
xattr_72 in ['h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac'] ]
==>
[
xattr_73 set 'as' , 
xattr_74 set 'n' , 
xattr_75 set 'ah' , 
xattr_76 set 's' ].

xrule xschm_23/11 :
[
xattr_70 in ['aw', 'ax', 'ay'] , 
xattr_71 eq 'ap' , 
xattr_72 in ['ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] ]
==>
[
xattr_73 set 'ao' , 
xattr_74 set 'm' , 
xattr_75 set 'ac' , 
xattr_76 set 'y' ].

xrule xschm_24/0 :
[
xattr_73 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_74 in ['c', 'd'] , 
xattr_75 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] , 
xattr_76 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] ]
==>
[
xattr_77 set 'o' , 
xattr_78 set 'j' , 
xattr_79 set 'ai' ].

xrule xschm_24/1 :
[
xattr_73 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_74 in ['c', 'd'] , 
xattr_75 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] , 
xattr_76 in ['an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_77 set 'at' , 
xattr_78 set 'n' , 
xattr_79 set 'bh' ].

xrule xschm_24/2 :
[
xattr_73 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_74 in ['c', 'd'] , 
xattr_75 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] , 
xattr_76 in ['ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_77 set 'l' , 
xattr_78 set 'aa' , 
xattr_79 set 'bd' ].

xrule xschm_24/3 :
[
xattr_73 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_74 in ['c', 'd'] , 
xattr_75 in ['an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_76 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] ]
==>
[
xattr_77 set 'ab' , 
xattr_78 set 'am' , 
xattr_79 set 'aw' ].

xrule xschm_24/4 :
[
xattr_73 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_74 in ['c', 'd'] , 
xattr_75 in ['an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_76 in ['an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_77 set 'ai' , 
xattr_78 set 'an' , 
xattr_79 set 'ag' ].

xrule xschm_24/5 :
[
xattr_73 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_74 in ['c', 'd'] , 
xattr_75 in ['an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_76 in ['ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_77 set 'ab' , 
xattr_78 set 'w' , 
xattr_79 set 'ar' ].

xrule xschm_24/6 :
[
xattr_73 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_74 in ['c', 'd'] , 
xattr_75 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_76 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] ]
==>
[
xattr_77 set 'ar' , 
xattr_78 set 'l' , 
xattr_79 set 'bb' ].

xrule xschm_24/7 :
[
xattr_73 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_74 in ['c', 'd'] , 
xattr_75 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_76 in ['an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_77 set 'am' , 
xattr_78 set 'ad' , 
xattr_79 set 'ax' ].

xrule xschm_24/8 :
[
xattr_73 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_74 in ['c', 'd'] , 
xattr_75 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_76 in ['ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_77 set 'av' , 
xattr_78 set 'x' , 
xattr_79 set 'ar' ].

xrule xschm_24/9 :
[
xattr_73 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_74 in ['e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_75 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] , 
xattr_76 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] ]
==>
[
xattr_77 set 't' , 
xattr_78 set 'l' , 
xattr_79 set 'ae' ].

xrule xschm_24/10 :
[
xattr_73 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_74 in ['e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_75 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] , 
xattr_76 in ['an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_77 set 'au' , 
xattr_78 set 'z' , 
xattr_79 set 'af' ].

xrule xschm_24/11 :
[
xattr_73 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_74 in ['e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_75 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] , 
xattr_76 in ['ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_77 set 'av' , 
xattr_78 set 'h' , 
xattr_79 set 'ab' ].

xrule xschm_24/12 :
[
xattr_73 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_74 in ['e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_75 in ['an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_76 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] ]
==>
[
xattr_77 set 'an' , 
xattr_78 set 'ah' , 
xattr_79 set 'ba' ].

xrule xschm_24/13 :
[
xattr_73 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_74 in ['e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_75 in ['an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_76 in ['an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_77 set 'at' , 
xattr_78 set 'i' , 
xattr_79 set 'bb' ].

xrule xschm_24/14 :
[
xattr_73 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_74 in ['e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_75 in ['an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_76 in ['ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_77 set 'aw' , 
xattr_78 set 'ao' , 
xattr_79 set 'aj' ].

xrule xschm_24/15 :
[
xattr_73 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_74 in ['e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_75 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_76 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] ]
==>
[
xattr_77 set 'ai' , 
xattr_78 set 'p' , 
xattr_79 set 'ab' ].

xrule xschm_24/16 :
[
xattr_73 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_74 in ['e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_75 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_76 in ['an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_77 set 'ad' , 
xattr_78 set 'ad' , 
xattr_79 set 'bc' ].

xrule xschm_24/17 :
[
xattr_73 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_74 in ['e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_75 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_76 in ['ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_77 set 's' , 
xattr_78 set 'aa' , 
xattr_79 set 'w' ].

xrule xschm_24/18 :
[
xattr_73 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_74 eq 'ap' , 
xattr_75 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] , 
xattr_76 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] ]
==>
[
xattr_77 set 'x' , 
xattr_78 set 'ai' , 
xattr_79 set 'av' ].

xrule xschm_24/19 :
[
xattr_73 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_74 eq 'ap' , 
xattr_75 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] , 
xattr_76 in ['an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_77 set 'au' , 
xattr_78 set 'ag' , 
xattr_79 set 'ad' ].

xrule xschm_24/20 :
[
xattr_73 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_74 eq 'ap' , 
xattr_75 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] , 
xattr_76 in ['ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_77 set 'as' , 
xattr_78 set 'm' , 
xattr_79 set 'aa' ].

xrule xschm_24/21 :
[
xattr_73 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_74 eq 'ap' , 
xattr_75 in ['an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_76 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] ]
==>
[
xattr_77 set 'o' , 
xattr_78 set 'x' , 
xattr_79 set 'an' ].

xrule xschm_24/22 :
[
xattr_73 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_74 eq 'ap' , 
xattr_75 in ['an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_76 in ['an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_77 set 'n' , 
xattr_78 set 'ah' , 
xattr_79 set 'ab' ].

xrule xschm_24/23 :
[
xattr_73 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_74 eq 'ap' , 
xattr_75 in ['an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_76 in ['ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_77 set 'o' , 
xattr_78 set 'ah' , 
xattr_79 set 'w' ].

xrule xschm_24/24 :
[
xattr_73 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_74 eq 'ap' , 
xattr_75 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_76 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] ]
==>
[
xattr_77 set 'am' , 
xattr_78 set 'k' , 
xattr_79 set 'al' ].

xrule xschm_24/25 :
[
xattr_73 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_74 eq 'ap' , 
xattr_75 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_76 in ['an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_77 set 'af' , 
xattr_78 set 'h' , 
xattr_79 set 'ac' ].

xrule xschm_24/26 :
[
xattr_73 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x'] , 
xattr_74 eq 'ap' , 
xattr_75 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_76 in ['ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_77 set 'p' , 
xattr_78 set 'u' , 
xattr_79 set 'ar' ].

xrule xschm_24/27 :
[
xattr_73 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_74 in ['c', 'd'] , 
xattr_75 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] , 
xattr_76 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] ]
==>
[
xattr_77 set 'as' , 
xattr_78 set 'ar' , 
xattr_79 set 'be' ].

xrule xschm_24/28 :
[
xattr_73 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_74 in ['c', 'd'] , 
xattr_75 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] , 
xattr_76 in ['an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_77 set 'w' , 
xattr_78 set 'x' , 
xattr_79 set 'aj' ].

xrule xschm_24/29 :
[
xattr_73 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_74 in ['c', 'd'] , 
xattr_75 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] , 
xattr_76 in ['ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_77 set 'ad' , 
xattr_78 set 'f' , 
xattr_79 set 'bf' ].

xrule xschm_24/30 :
[
xattr_73 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_74 in ['c', 'd'] , 
xattr_75 in ['an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_76 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] ]
==>
[
xattr_77 set 'ac' , 
xattr_78 set 'ai' , 
xattr_79 set 'bd' ].

xrule xschm_24/31 :
[
xattr_73 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_74 in ['c', 'd'] , 
xattr_75 in ['an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_76 in ['an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_77 set 'ak' , 
xattr_78 set 'ap' , 
xattr_79 set 'ba' ].

xrule xschm_24/32 :
[
xattr_73 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_74 in ['c', 'd'] , 
xattr_75 in ['an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_76 in ['ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_77 set 'ay' , 
xattr_78 set 'as' , 
xattr_79 set 'ae' ].

xrule xschm_24/33 :
[
xattr_73 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_74 in ['c', 'd'] , 
xattr_75 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_76 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] ]
==>
[
xattr_77 set 'aj' , 
xattr_78 set 'r' , 
xattr_79 set 'az' ].

xrule xschm_24/34 :
[
xattr_73 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_74 in ['c', 'd'] , 
xattr_75 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_76 in ['an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_77 set 'm' , 
xattr_78 set 'ai' , 
xattr_79 set 'ag' ].

xrule xschm_24/35 :
[
xattr_73 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_74 in ['c', 'd'] , 
xattr_75 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_76 in ['ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_77 set 'w' , 
xattr_78 set 't' , 
xattr_79 set 'bg' ].

xrule xschm_24/36 :
[
xattr_73 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_74 in ['e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_75 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] , 
xattr_76 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] ]
==>
[
xattr_77 set 'ao' , 
xattr_78 set 's' , 
xattr_79 set 'ag' ].

xrule xschm_24/37 :
[
xattr_73 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_74 in ['e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_75 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] , 
xattr_76 in ['an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_77 set 'aq' , 
xattr_78 set 'v' , 
xattr_79 set 'ax' ].

xrule xschm_24/38 :
[
xattr_73 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_74 in ['e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_75 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] , 
xattr_76 in ['ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_77 set 'ae' , 
xattr_78 set 'm' , 
xattr_79 set 'bg' ].

xrule xschm_24/39 :
[
xattr_73 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_74 in ['e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_75 in ['an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_76 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] ]
==>
[
xattr_77 set 'ak' , 
xattr_78 set 'i' , 
xattr_79 set 'bi' ].

xrule xschm_24/40 :
[
xattr_73 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_74 in ['e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_75 in ['an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_76 in ['an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_77 set 'ab' , 
xattr_78 set 'am' , 
xattr_79 set 'bg' ].

xrule xschm_24/41 :
[
xattr_73 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_74 in ['e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_75 in ['an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_76 in ['ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_77 set 'al' , 
xattr_78 set 'aq' , 
xattr_79 set 'ag' ].

xrule xschm_24/42 :
[
xattr_73 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_74 in ['e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_75 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_76 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] ]
==>
[
xattr_77 set 'ad' , 
xattr_78 set 'r' , 
xattr_79 set 'ax' ].

xrule xschm_24/43 :
[
xattr_73 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_74 in ['e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_75 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_76 in ['an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_77 set 'ak' , 
xattr_78 set 'i' , 
xattr_79 set 'aa' ].

xrule xschm_24/44 :
[
xattr_73 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_74 in ['e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_75 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_76 in ['ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_77 set 'l' , 
xattr_78 set 'ao' , 
xattr_79 set 'be' ].

xrule xschm_24/45 :
[
xattr_73 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_74 eq 'ap' , 
xattr_75 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] , 
xattr_76 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] ]
==>
[
xattr_77 set 'p' , 
xattr_78 set 'o' , 
xattr_79 set 'az' ].

xrule xschm_24/46 :
[
xattr_73 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_74 eq 'ap' , 
xattr_75 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] , 
xattr_76 in ['an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_77 set 'y' , 
xattr_78 set 'ad' , 
xattr_79 set 'bf' ].

xrule xschm_24/47 :
[
xattr_73 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_74 eq 'ap' , 
xattr_75 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] , 
xattr_76 in ['ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_77 set 'x' , 
xattr_78 set 'ai' , 
xattr_79 set 'an' ].

xrule xschm_24/48 :
[
xattr_73 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_74 eq 'ap' , 
xattr_75 in ['an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_76 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] ]
==>
[
xattr_77 set 'x' , 
xattr_78 set 'aq' , 
xattr_79 set 'ab' ].

xrule xschm_24/49 :
[
xattr_73 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_74 eq 'ap' , 
xattr_75 in ['an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_76 in ['an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_77 set 'r' , 
xattr_78 set 'n' , 
xattr_79 set 'z' ].

xrule xschm_24/50 :
[
xattr_73 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_74 eq 'ap' , 
xattr_75 in ['an', 'ao', 'ap', 'aq', 'ar'] , 
xattr_76 in ['ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_77 set 'ar' , 
xattr_78 set 'ad' , 
xattr_79 set 'ae' ].

xrule xschm_24/51 :
[
xattr_73 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_74 eq 'ap' , 
xattr_75 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_76 in ['s', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am'] ]
==>
[
xattr_77 set 'p' , 
xattr_78 set 'ao' , 
xattr_79 set 'z' ].

xrule xschm_24/52 :
[
xattr_73 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_74 eq 'ap' , 
xattr_75 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_76 in ['an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] ]
==>
[
xattr_77 set 'q' , 
xattr_78 set 'am' , 
xattr_79 set 'ax' ].

xrule xschm_24/53 :
[
xattr_73 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_74 eq 'ap' , 
xattr_75 in ['as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] , 
xattr_76 in ['ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_77 set 'r' , 
xattr_78 set 'k' , 
xattr_79 set 'aw' ].

xrule xschm_25/0 :
[
xattr_77 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_78 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_79 in ['v', 'w', 'x', 'y'] ]
==>
[
xattr_80 set 'y' , 
xattr_81 set 'bh' , 
xattr_82 set 'aj' , 
xattr_83 set 'w' ].

xrule xschm_25/1 :
[
xattr_77 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_78 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_79 in ['z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_80 set 'ak' , 
xattr_81 set 'bb' , 
xattr_82 set 'n' , 
xattr_83 set 'bb' ].

xrule xschm_25/2 :
[
xattr_77 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_78 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_79 in ['bg', 'bh', 'bi'] ]
==>
[
xattr_80 set 'aj' , 
xattr_81 set 'am' , 
xattr_82 set 'p' , 
xattr_83 set 'ar' ].

xrule xschm_25/3 :
[
xattr_77 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_78 in ['ap', 'aq', 'ar', 'as'] , 
xattr_79 in ['v', 'w', 'x', 'y'] ]
==>
[
xattr_80 set 'ai' , 
xattr_81 set 'be' , 
xattr_82 set 'ar' , 
xattr_83 set 'az' ].

xrule xschm_25/4 :
[
xattr_77 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_78 in ['ap', 'aq', 'ar', 'as'] , 
xattr_79 in ['z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_80 set 'ae' , 
xattr_81 set 'ah' , 
xattr_82 set 'm' , 
xattr_83 set 'bd' ].

xrule xschm_25/5 :
[
xattr_77 in ['l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_78 in ['ap', 'aq', 'ar', 'as'] , 
xattr_79 in ['bg', 'bh', 'bi'] ]
==>
[
xattr_80 set 'ak' , 
xattr_81 set 'bc' , 
xattr_82 set 'y' , 
xattr_83 set 'al' ].

xrule xschm_25/6 :
[
xattr_77 in ['ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_78 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_79 in ['v', 'w', 'x', 'y'] ]
==>
[
xattr_80 set 'aj' , 
xattr_81 set 'ai' , 
xattr_82 set 'u' , 
xattr_83 set 'y' ].

xrule xschm_25/7 :
[
xattr_77 in ['ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_78 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_79 in ['z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_80 set 'ak' , 
xattr_81 set 'ac' , 
xattr_82 set 't' , 
xattr_83 set 't' ].

xrule xschm_25/8 :
[
xattr_77 in ['ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_78 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] , 
xattr_79 in ['bg', 'bh', 'bi'] ]
==>
[
xattr_80 set 'ab' , 
xattr_81 set 'x' , 
xattr_82 set 'ah' , 
xattr_83 set 'be' ].

xrule xschm_25/9 :
[
xattr_77 in ['ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_78 in ['ap', 'aq', 'ar', 'as'] , 
xattr_79 in ['v', 'w', 'x', 'y'] ]
==>
[
xattr_80 set 'v' , 
xattr_81 set 'u' , 
xattr_82 set 'ai' , 
xattr_83 set 'x' ].

xrule xschm_25/10 :
[
xattr_77 in ['ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_78 in ['ap', 'aq', 'ar', 'as'] , 
xattr_79 in ['z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_80 set 'ad' , 
xattr_81 set 'ai' , 
xattr_82 set 'ai' , 
xattr_83 set 'aa' ].

xrule xschm_25/11 :
[
xattr_77 in ['ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay'] , 
xattr_78 in ['ap', 'aq', 'ar', 'as'] , 
xattr_79 in ['bg', 'bh', 'bi'] ]
==>
[
xattr_80 set 'f' , 
xattr_81 set 'al' , 
xattr_82 set 'ap' , 
xattr_83 set 'al' ].

xrule xschm_26/0 :
[
xattr_80 eq 'f' , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_83 in ['s', 't', 'u', 'v', 'w', 'x'] ]
==>
[
xattr_84 set 'aq' , 
xattr_85 set 'bk' , 
xattr_86 set 'am' ].

xrule xschm_26/1 :
[
xattr_80 eq 'f' , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_83 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] ]
==>
[
xattr_84 set 'bh' , 
xattr_85 set 'bj' , 
xattr_86 set 'm' ].

xrule xschm_26/2 :
[
xattr_80 eq 'f' , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_83 in ['ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] ]
==>
[
xattr_84 set 'ay' , 
xattr_85 set 'be' , 
xattr_86 set 'i' ].

xrule xschm_26/3 :
[
xattr_80 eq 'f' , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_83 in ['bd', 'be', 'bf'] ]
==>
[
xattr_84 set 'bd' , 
xattr_85 set 'x' , 
xattr_86 set 'as' ].

xrule xschm_26/4 :
[
xattr_80 eq 'f' , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 eq 'ag' , 
xattr_83 in ['s', 't', 'u', 'v', 'w', 'x'] ]
==>
[
xattr_84 set 'be' , 
xattr_85 set 'bd' , 
xattr_86 set 's' ].

xrule xschm_26/5 :
[
xattr_80 eq 'f' , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 eq 'ag' , 
xattr_83 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] ]
==>
[
xattr_84 set 'bf' , 
xattr_85 set 'am' , 
xattr_86 set 'ai' ].

xrule xschm_26/6 :
[
xattr_80 eq 'f' , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 eq 'ag' , 
xattr_83 in ['ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] ]
==>
[
xattr_84 set 'bn' , 
xattr_85 set 'ba' , 
xattr_86 set 'h' ].

xrule xschm_26/7 :
[
xattr_80 eq 'f' , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 eq 'ag' , 
xattr_83 in ['bd', 'be', 'bf'] ]
==>
[
xattr_84 set 'au' , 
xattr_85 set 'ab' , 
xattr_86 set 'ad' ].

xrule xschm_26/8 :
[
xattr_80 eq 'f' , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_83 in ['s', 't', 'u', 'v', 'w', 'x'] ]
==>
[
xattr_84 set 'av' , 
xattr_85 set 'x' , 
xattr_86 set 'g' ].

xrule xschm_26/9 :
[
xattr_80 eq 'f' , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_83 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] ]
==>
[
xattr_84 set 'bl' , 
xattr_85 set 'an' , 
xattr_86 set 'aq' ].

xrule xschm_26/10 :
[
xattr_80 eq 'f' , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_83 in ['ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] ]
==>
[
xattr_84 set 'az' , 
xattr_85 set 'bk' , 
xattr_86 set 'al' ].

xrule xschm_26/11 :
[
xattr_80 eq 'f' , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_83 in ['bd', 'be', 'bf'] ]
==>
[
xattr_84 set 'az' , 
xattr_85 set 'ae' , 
xattr_86 set 'ap' ].

xrule xschm_26/12 :
[
xattr_80 eq 'f' , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_83 in ['s', 't', 'u', 'v', 'w', 'x'] ]
==>
[
xattr_84 set 'ai' , 
xattr_85 set 'ab' , 
xattr_86 set 'l' ].

xrule xschm_26/13 :
[
xattr_80 eq 'f' , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_83 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] ]
==>
[
xattr_84 set 'ac' , 
xattr_85 set 'al' , 
xattr_86 set 'ah' ].

xrule xschm_26/14 :
[
xattr_80 eq 'f' , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_83 in ['ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] ]
==>
[
xattr_84 set 'ad' , 
xattr_85 set 'x' , 
xattr_86 set 'k' ].

xrule xschm_26/15 :
[
xattr_80 eq 'f' , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_83 in ['bd', 'be', 'bf'] ]
==>
[
xattr_84 set 'ar' , 
xattr_85 set 'aq' , 
xattr_86 set 'x' ].

xrule xschm_26/16 :
[
xattr_80 eq 'f' , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 eq 'ag' , 
xattr_83 in ['s', 't', 'u', 'v', 'w', 'x'] ]
==>
[
xattr_84 set 'al' , 
xattr_85 set 'av' , 
xattr_86 set 'aj' ].

xrule xschm_26/17 :
[
xattr_80 eq 'f' , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 eq 'ag' , 
xattr_83 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] ]
==>
[
xattr_84 set 'bd' , 
xattr_85 set 'y' , 
xattr_86 set 'y' ].

xrule xschm_26/18 :
[
xattr_80 eq 'f' , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 eq 'ag' , 
xattr_83 in ['ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] ]
==>
[
xattr_84 set 'aq' , 
xattr_85 set 'x' , 
xattr_86 set 'h' ].

xrule xschm_26/19 :
[
xattr_80 eq 'f' , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 eq 'ag' , 
xattr_83 in ['bd', 'be', 'bf'] ]
==>
[
xattr_84 set 'an' , 
xattr_85 set 'af' , 
xattr_86 set 'i' ].

xrule xschm_26/20 :
[
xattr_80 eq 'f' , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_83 in ['s', 't', 'u', 'v', 'w', 'x'] ]
==>
[
xattr_84 set 'as' , 
xattr_85 set 'ar' , 
xattr_86 set 'ac' ].

xrule xschm_26/21 :
[
xattr_80 eq 'f' , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_83 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] ]
==>
[
xattr_84 set 'az' , 
xattr_85 set 'ac' , 
xattr_86 set 'u' ].

xrule xschm_26/22 :
[
xattr_80 eq 'f' , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_83 in ['ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] ]
==>
[
xattr_84 set 'bm' , 
xattr_85 set 'y' , 
xattr_86 set 'p' ].

xrule xschm_26/23 :
[
xattr_80 eq 'f' , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_83 in ['bd', 'be', 'bf'] ]
==>
[
xattr_84 set 'bm' , 
xattr_85 set 'bh' , 
xattr_86 set 'ag' ].

xrule xschm_26/24 :
[
xattr_80 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_83 in ['s', 't', 'u', 'v', 'w', 'x'] ]
==>
[
xattr_84 set 'ac' , 
xattr_85 set 'an' , 
xattr_86 set 'ao' ].

xrule xschm_26/25 :
[
xattr_80 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_83 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] ]
==>
[
xattr_84 set 'af' , 
xattr_85 set 'an' , 
xattr_86 set 'ac' ].

xrule xschm_26/26 :
[
xattr_80 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_83 in ['ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] ]
==>
[
xattr_84 set 'bm' , 
xattr_85 set 'ba' , 
xattr_86 set 'ar' ].

xrule xschm_26/27 :
[
xattr_80 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_83 in ['bd', 'be', 'bf'] ]
==>
[
xattr_84 set 'bh' , 
xattr_85 set 'ao' , 
xattr_86 set 'ae' ].

xrule xschm_26/28 :
[
xattr_80 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 eq 'ag' , 
xattr_83 in ['s', 't', 'u', 'v', 'w', 'x'] ]
==>
[
xattr_84 set 'bh' , 
xattr_85 set 'af' , 
xattr_86 set 'k' ].

xrule xschm_26/29 :
[
xattr_80 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 eq 'ag' , 
xattr_83 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] ]
==>
[
xattr_84 set 'bi' , 
xattr_85 set 'bg' , 
xattr_86 set 'o' ].

xrule xschm_26/30 :
[
xattr_80 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 eq 'ag' , 
xattr_83 in ['ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] ]
==>
[
xattr_84 set 'ah' , 
xattr_85 set 'bk' , 
xattr_86 set 'ag' ].

xrule xschm_26/31 :
[
xattr_80 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 eq 'ag' , 
xattr_83 in ['bd', 'be', 'bf'] ]
==>
[
xattr_84 set 'ak' , 
xattr_85 set 'ai' , 
xattr_86 set 't' ].

xrule xschm_26/32 :
[
xattr_80 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_83 in ['s', 't', 'u', 'v', 'w', 'x'] ]
==>
[
xattr_84 set 'al' , 
xattr_85 set 'aa' , 
xattr_86 set 'x' ].

xrule xschm_26/33 :
[
xattr_80 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_83 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] ]
==>
[
xattr_84 set 'an' , 
xattr_85 set 'bb' , 
xattr_86 set 'ac' ].

xrule xschm_26/34 :
[
xattr_80 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_83 in ['ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] ]
==>
[
xattr_84 set 'an' , 
xattr_85 set 'bk' , 
xattr_86 set 'j' ].

xrule xschm_26/35 :
[
xattr_80 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_83 in ['bd', 'be', 'bf'] ]
==>
[
xattr_84 set 'av' , 
xattr_85 set 'ac' , 
xattr_86 set 'af' ].

xrule xschm_26/36 :
[
xattr_80 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_83 in ['s', 't', 'u', 'v', 'w', 'x'] ]
==>
[
xattr_84 set 'ad' , 
xattr_85 set 'ad' , 
xattr_86 set 'w' ].

xrule xschm_26/37 :
[
xattr_80 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_83 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] ]
==>
[
xattr_84 set 'af' , 
xattr_85 set 'ay' , 
xattr_86 set 'ag' ].

xrule xschm_26/38 :
[
xattr_80 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_83 in ['ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] ]
==>
[
xattr_84 set 'bb' , 
xattr_85 set 'at' , 
xattr_86 set 'l' ].

xrule xschm_26/39 :
[
xattr_80 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_83 in ['bd', 'be', 'bf'] ]
==>
[
xattr_84 set 'ah' , 
xattr_85 set 'x' , 
xattr_86 set 'o' ].

xrule xschm_26/40 :
[
xattr_80 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 eq 'ag' , 
xattr_83 in ['s', 't', 'u', 'v', 'w', 'x'] ]
==>
[
xattr_84 set 'ad' , 
xattr_85 set 'bk' , 
xattr_86 set 'ae' ].

xrule xschm_26/41 :
[
xattr_80 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 eq 'ag' , 
xattr_83 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] ]
==>
[
xattr_84 set 'bd' , 
xattr_85 set 'ad' , 
xattr_86 set 'y' ].

xrule xschm_26/42 :
[
xattr_80 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 eq 'ag' , 
xattr_83 in ['ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] ]
==>
[
xattr_84 set 'ac' , 
xattr_85 set 'ao' , 
xattr_86 set 'k' ].

xrule xschm_26/43 :
[
xattr_80 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 eq 'ag' , 
xattr_83 in ['bd', 'be', 'bf'] ]
==>
[
xattr_84 set 'bc' , 
xattr_85 set 'am' , 
xattr_86 set 'ap' ].

xrule xschm_26/44 :
[
xattr_80 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_83 in ['s', 't', 'u', 'v', 'w', 'x'] ]
==>
[
xattr_84 set 'az' , 
xattr_85 set 'as' , 
xattr_86 set 'ab' ].

xrule xschm_26/45 :
[
xattr_80 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_83 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] ]
==>
[
xattr_84 set 'ac' , 
xattr_85 set 'bc' , 
xattr_86 set 't' ].

xrule xschm_26/46 :
[
xattr_80 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_83 in ['ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] ]
==>
[
xattr_84 set 'an' , 
xattr_85 set 'bf' , 
xattr_86 set 'af' ].

xrule xschm_26/47 :
[
xattr_80 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_83 in ['bd', 'be', 'bf'] ]
==>
[
xattr_84 set 'bb' , 
xattr_85 set 'aw' , 
xattr_86 set 'r' ].

xrule xschm_26/48 :
[
xattr_80 in ['ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_83 in ['s', 't', 'u', 'v', 'w', 'x'] ]
==>
[
xattr_84 set 'ap' , 
xattr_85 set 'aa' , 
xattr_86 set 'al' ].

xrule xschm_26/49 :
[
xattr_80 in ['ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_83 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] ]
==>
[
xattr_84 set 'bg' , 
xattr_85 set 'as' , 
xattr_86 set 'z' ].

xrule xschm_26/50 :
[
xattr_80 in ['ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_83 in ['ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] ]
==>
[
xattr_84 set 'bb' , 
xattr_85 set 'ax' , 
xattr_86 set 'm' ].

xrule xschm_26/51 :
[
xattr_80 in ['ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_83 in ['bd', 'be', 'bf'] ]
==>
[
xattr_84 set 'bk' , 
xattr_85 set 'bh' , 
xattr_86 set 'g' ].

xrule xschm_26/52 :
[
xattr_80 in ['ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 eq 'ag' , 
xattr_83 in ['s', 't', 'u', 'v', 'w', 'x'] ]
==>
[
xattr_84 set 'bn' , 
xattr_85 set 'aa' , 
xattr_86 set 'ad' ].

xrule xschm_26/53 :
[
xattr_80 in ['ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 eq 'ag' , 
xattr_83 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] ]
==>
[
xattr_84 set 'ag' , 
xattr_85 set 'af' , 
xattr_86 set 'z' ].

xrule xschm_26/54 :
[
xattr_80 in ['ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 eq 'ag' , 
xattr_83 in ['ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] ]
==>
[
xattr_84 set 'aq' , 
xattr_85 set 'bf' , 
xattr_86 set 'al' ].

xrule xschm_26/55 :
[
xattr_80 in ['ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 eq 'ag' , 
xattr_83 in ['bd', 'be', 'bf'] ]
==>
[
xattr_84 set 'as' , 
xattr_85 set 'ah' , 
xattr_86 set 'am' ].

xrule xschm_26/56 :
[
xattr_80 in ['ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_83 in ['s', 't', 'u', 'v', 'w', 'x'] ]
==>
[
xattr_84 set 'ai' , 
xattr_85 set 'am' , 
xattr_86 set 'ac' ].

xrule xschm_26/57 :
[
xattr_80 in ['ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_83 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] ]
==>
[
xattr_84 set 'ac' , 
xattr_85 set 'ax' , 
xattr_86 set 'al' ].

xrule xschm_26/58 :
[
xattr_80 in ['ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_83 in ['ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] ]
==>
[
xattr_84 set 'az' , 
xattr_85 set 'x' , 
xattr_86 set 'l' ].

xrule xschm_26/59 :
[
xattr_80 in ['ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_81 in ['u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai'] , 
xattr_82 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_83 in ['bd', 'be', 'bf'] ]
==>
[
xattr_84 set 'bk' , 
xattr_85 set 'bg' , 
xattr_86 set 'ag' ].

xrule xschm_26/60 :
[
xattr_80 in ['ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_83 in ['s', 't', 'u', 'v', 'w', 'x'] ]
==>
[
xattr_84 set 'ar' , 
xattr_85 set 'ai' , 
xattr_86 set 'aq' ].

xrule xschm_26/61 :
[
xattr_80 in ['ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_83 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] ]
==>
[
xattr_84 set 'ah' , 
xattr_85 set 'ax' , 
xattr_86 set 'ag' ].

xrule xschm_26/62 :
[
xattr_80 in ['ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_83 in ['ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] ]
==>
[
xattr_84 set 'ba' , 
xattr_85 set 'bk' , 
xattr_86 set 'x' ].

xrule xschm_26/63 :
[
xattr_80 in ['ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af'] , 
xattr_83 in ['bd', 'be', 'bf'] ]
==>
[
xattr_84 set 'bm' , 
xattr_85 set 'av' , 
xattr_86 set 'p' ].

xrule xschm_26/64 :
[
xattr_80 in ['ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 eq 'ag' , 
xattr_83 in ['s', 't', 'u', 'v', 'w', 'x'] ]
==>
[
xattr_84 set 'aa' , 
xattr_85 set 'av' , 
xattr_86 set 'i' ].

xrule xschm_26/65 :
[
xattr_80 in ['ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 eq 'ag' , 
xattr_83 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] ]
==>
[
xattr_84 set 'bj' , 
xattr_85 set 'at' , 
xattr_86 set 'j' ].

xrule xschm_26/66 :
[
xattr_80 in ['ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 eq 'ag' , 
xattr_83 in ['ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] ]
==>
[
xattr_84 set 'aw' , 
xattr_85 set 'bj' , 
xattr_86 set 'q' ].

xrule xschm_26/67 :
[
xattr_80 in ['ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 eq 'ag' , 
xattr_83 in ['bd', 'be', 'bf'] ]
==>
[
xattr_84 set 'af' , 
xattr_85 set 'au' , 
xattr_86 set 'ab' ].

xrule xschm_26/68 :
[
xattr_80 in ['ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_83 in ['s', 't', 'u', 'v', 'w', 'x'] ]
==>
[
xattr_84 set 'be' , 
xattr_85 set 'ah' , 
xattr_86 set 'aj' ].

xrule xschm_26/69 :
[
xattr_80 in ['ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_83 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an'] ]
==>
[
xattr_84 set 'ae' , 
xattr_85 set 'ab' , 
xattr_86 set 'ao' ].

xrule xschm_26/70 :
[
xattr_80 in ['ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_83 in ['ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc'] ]
==>
[
xattr_84 set 'ak' , 
xattr_85 set 'aq' , 
xattr_86 set 't' ].

xrule xschm_26/71 :
[
xattr_80 in ['ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_81 in ['aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh'] , 
xattr_82 in ['ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_83 in ['bd', 'be', 'bf'] ]
==>
[
xattr_84 set 'aa' , 
xattr_85 set 'an' , 
xattr_86 set 'ab' ].

xrule xschm_27/0 :
[
xattr_84 in ['aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj'] , 
xattr_85 in ['x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb'] , 
xattr_86 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj'] ]
==>
[
xattr_87 set 'au' , 
xattr_88 set 'al' , 
xattr_89 set 's' ].

xrule xschm_27/1 :
[
xattr_84 in ['aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj'] , 
xattr_85 in ['x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb'] , 
xattr_86 in ['ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] ]
==>
[
xattr_87 set 'w' , 
xattr_88 set 'x' , 
xattr_89 set 'k' ].

xrule xschm_27/2 :
[
xattr_84 in ['aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj'] , 
xattr_85 in ['bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk'] , 
xattr_86 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj'] ]
==>
[
xattr_87 set 'aa' , 
xattr_88 set 'ac' , 
xattr_89 set 'aa' ].

xrule xschm_27/3 :
[
xattr_84 in ['aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj'] , 
xattr_85 in ['bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk'] , 
xattr_86 in ['ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] ]
==>
[
xattr_87 set 'av' , 
xattr_88 set 'o' , 
xattr_89 set 'ac' ].

xrule xschm_27/4 :
[
xattr_84 in ['bk', 'bl', 'bm', 'bn'] , 
xattr_85 in ['x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb'] , 
xattr_86 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj'] ]
==>
[
xattr_87 set 'ar' , 
xattr_88 set 'z' , 
xattr_89 set 'h' ].

xrule xschm_27/5 :
[
xattr_84 in ['bk', 'bl', 'bm', 'bn'] , 
xattr_85 in ['x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb'] , 
xattr_86 in ['ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] ]
==>
[
xattr_87 set 'ar' , 
xattr_88 set 'y' , 
xattr_89 set 'o' ].

xrule xschm_27/6 :
[
xattr_84 in ['bk', 'bl', 'bm', 'bn'] , 
xattr_85 in ['bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk'] , 
xattr_86 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj'] ]
==>
[
xattr_87 set 'ad' , 
xattr_88 set 'am' , 
xattr_89 set 'e' ].

xrule xschm_27/7 :
[
xattr_84 in ['bk', 'bl', 'bm', 'bn'] , 
xattr_85 in ['bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk'] , 
xattr_86 in ['ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] ]
==>
[
xattr_87 set 'ap' , 
xattr_88 set 'an' , 
xattr_89 set 'p' ].

xrule xschm_28/0 :
[
xattr_87 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] , 
xattr_88 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj'] , 
xattr_89 in ['c', 'd'] ]
==>
[
xattr_90 set 'bg' , 
xattr_91 set 'aw' , 
xattr_92 set 'y' ].

xrule xschm_28/1 :
[
xattr_87 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] , 
xattr_88 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj'] , 
xattr_89 in ['e', 'f', 'g', 'h', 'i'] ]
==>
[
xattr_90 set 'ap' , 
xattr_91 set 'al' , 
xattr_92 set 'y' ].

xrule xschm_28/2 :
[
xattr_87 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] , 
xattr_88 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj'] , 
xattr_89 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] ]
==>
[
xattr_90 set 'bc' , 
xattr_91 set 'x' , 
xattr_92 set 'j' ].

xrule xschm_28/3 :
[
xattr_87 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] , 
xattr_88 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj'] , 
xattr_89 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] ]
==>
[
xattr_90 set 'bd' , 
xattr_91 set 'ag' , 
xattr_92 set 'q' ].

xrule xschm_28/4 :
[
xattr_87 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] , 
xattr_88 in ['ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax'] , 
xattr_89 in ['c', 'd'] ]
==>
[
xattr_90 set 'bj' , 
xattr_91 set 'aa' , 
xattr_92 set 'p' ].

xrule xschm_28/5 :
[
xattr_87 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] , 
xattr_88 in ['ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax'] , 
xattr_89 in ['e', 'f', 'g', 'h', 'i'] ]
==>
[
xattr_90 set 'aa' , 
xattr_91 set 'aq' , 
xattr_92 set 'ai' ].

xrule xschm_28/6 :
[
xattr_87 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] , 
xattr_88 in ['ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax'] , 
xattr_89 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] ]
==>
[
xattr_90 set 'ax' , 
xattr_91 set 'aq' , 
xattr_92 set 'ao' ].

xrule xschm_28/7 :
[
xattr_87 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] , 
xattr_88 in ['ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax'] , 
xattr_89 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] ]
==>
[
xattr_90 set 'ax' , 
xattr_91 set 'ag' , 
xattr_92 set 'r' ].

xrule xschm_28/8 :
[
xattr_87 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] , 
xattr_88 in ['ay', 'az'] , 
xattr_89 in ['c', 'd'] ]
==>
[
xattr_90 set 'as' , 
xattr_91 set 'ac' , 
xattr_92 set 'k' ].

xrule xschm_28/9 :
[
xattr_87 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] , 
xattr_88 in ['ay', 'az'] , 
xattr_89 in ['e', 'f', 'g', 'h', 'i'] ]
==>
[
xattr_90 set 'be' , 
xattr_91 set 'af' , 
xattr_92 set 'g' ].

xrule xschm_28/10 :
[
xattr_87 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] , 
xattr_88 in ['ay', 'az'] , 
xattr_89 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] ]
==>
[
xattr_90 set 'bg' , 
xattr_91 set 'aj' , 
xattr_92 set 'd' ].

xrule xschm_28/11 :
[
xattr_87 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al'] , 
xattr_88 in ['ay', 'az'] , 
xattr_89 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] ]
==>
[
xattr_90 set 'av' , 
xattr_91 set 'ak' , 
xattr_92 set 'b' ].

xrule xschm_28/12 :
[
xattr_87 in ['am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax'] , 
xattr_88 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj'] , 
xattr_89 in ['c', 'd'] ]
==>
[
xattr_90 set 'bf' , 
xattr_91 set 'z' , 
xattr_92 set 'ai' ].

xrule xschm_28/13 :
[
xattr_87 in ['am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax'] , 
xattr_88 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj'] , 
xattr_89 in ['e', 'f', 'g', 'h', 'i'] ]
==>
[
xattr_90 set 'bc' , 
xattr_91 set 'y' , 
xattr_92 set 'b' ].

xrule xschm_28/14 :
[
xattr_87 in ['am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax'] , 
xattr_88 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj'] , 
xattr_89 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] ]
==>
[
xattr_90 set 'ab' , 
xattr_91 set 'x' , 
xattr_92 set 'ah' ].

xrule xschm_28/15 :
[
xattr_87 in ['am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax'] , 
xattr_88 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj'] , 
xattr_89 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] ]
==>
[
xattr_90 set 'av' , 
xattr_91 set 'ai' , 
xattr_92 set 'ab' ].

xrule xschm_28/16 :
[
xattr_87 in ['am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax'] , 
xattr_88 in ['ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax'] , 
xattr_89 in ['c', 'd'] ]
==>
[
xattr_90 set 'be' , 
xattr_91 set 'ba' , 
xattr_92 set 'j' ].

xrule xschm_28/17 :
[
xattr_87 in ['am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax'] , 
xattr_88 in ['ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax'] , 
xattr_89 in ['e', 'f', 'g', 'h', 'i'] ]
==>
[
xattr_90 set 'az' , 
xattr_91 set 'av' , 
xattr_92 set 'b' ].

xrule xschm_28/18 :
[
xattr_87 in ['am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax'] , 
xattr_88 in ['ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax'] , 
xattr_89 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] ]
==>
[
xattr_90 set 'aj' , 
xattr_91 set 'ao' , 
xattr_92 set 'h' ].

xrule xschm_28/19 :
[
xattr_87 in ['am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax'] , 
xattr_88 in ['ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax'] , 
xattr_89 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] ]
==>
[
xattr_90 set 'aw' , 
xattr_91 set 'af' , 
xattr_92 set 's' ].

xrule xschm_28/20 :
[
xattr_87 in ['am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax'] , 
xattr_88 in ['ay', 'az'] , 
xattr_89 in ['c', 'd'] ]
==>
[
xattr_90 set 'y' , 
xattr_91 set 'az' , 
xattr_92 set 'an' ].

xrule xschm_28/21 :
[
xattr_87 in ['am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax'] , 
xattr_88 in ['ay', 'az'] , 
xattr_89 in ['e', 'f', 'g', 'h', 'i'] ]
==>
[
xattr_90 set 'ap' , 
xattr_91 set 'z' , 
xattr_92 set 'r' ].

xrule xschm_28/22 :
[
xattr_87 in ['am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax'] , 
xattr_88 in ['ay', 'az'] , 
xattr_89 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] ]
==>
[
xattr_90 set 'bb' , 
xattr_91 set 'ab' , 
xattr_92 set 'e' ].

xrule xschm_28/23 :
[
xattr_87 in ['am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax'] , 
xattr_88 in ['ay', 'az'] , 
xattr_89 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] ]
==>
[
xattr_90 set 'ap' , 
xattr_91 set 'n' , 
xattr_92 set 'u' ].

xrule xschm_28/24 :
[
xattr_87 in ['ay', 'az'] , 
xattr_88 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj'] , 
xattr_89 in ['c', 'd'] ]
==>
[
xattr_90 set 'bh' , 
xattr_91 set 'an' , 
xattr_92 set 'ak' ].

xrule xschm_28/25 :
[
xattr_87 in ['ay', 'az'] , 
xattr_88 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj'] , 
xattr_89 in ['e', 'f', 'g', 'h', 'i'] ]
==>
[
xattr_90 set 'at' , 
xattr_91 set 'ah' , 
xattr_92 set 'k' ].

xrule xschm_28/26 :
[
xattr_87 in ['ay', 'az'] , 
xattr_88 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj'] , 
xattr_89 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] ]
==>
[
xattr_90 set 'ad' , 
xattr_91 set 'ak' , 
xattr_92 set 'am' ].

xrule xschm_28/27 :
[
xattr_87 in ['ay', 'az'] , 
xattr_88 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj'] , 
xattr_89 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] ]
==>
[
xattr_90 set 'bd' , 
xattr_91 set 'ab' , 
xattr_92 set 'aa' ].

xrule xschm_28/28 :
[
xattr_87 in ['ay', 'az'] , 
xattr_88 in ['ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax'] , 
xattr_89 in ['c', 'd'] ]
==>
[
xattr_90 set 'aj' , 
xattr_91 set 'an' , 
xattr_92 set 'h' ].

xrule xschm_28/29 :
[
xattr_87 in ['ay', 'az'] , 
xattr_88 in ['ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax'] , 
xattr_89 in ['e', 'f', 'g', 'h', 'i'] ]
==>
[
xattr_90 set 'ac' , 
xattr_91 set 'u' , 
xattr_92 set 'm' ].

xrule xschm_28/30 :
[
xattr_87 in ['ay', 'az'] , 
xattr_88 in ['ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax'] , 
xattr_89 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] ]
==>
[
xattr_90 set 'ag' , 
xattr_91 set 'ax' , 
xattr_92 set 'x' ].

xrule xschm_28/31 :
[
xattr_87 in ['ay', 'az'] , 
xattr_88 in ['ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax'] , 
xattr_89 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] ]
==>
[
xattr_90 set 'bh' , 
xattr_91 set 'r' , 
xattr_92 set 'z' ].

xrule xschm_28/32 :
[
xattr_87 in ['ay', 'az'] , 
xattr_88 in ['ay', 'az'] , 
xattr_89 in ['c', 'd'] ]
==>
[
xattr_90 set 'aq' , 
xattr_91 set 's' , 
xattr_92 set 'c' ].

xrule xschm_28/33 :
[
xattr_87 in ['ay', 'az'] , 
xattr_88 in ['ay', 'az'] , 
xattr_89 in ['e', 'f', 'g', 'h', 'i'] ]
==>
[
xattr_90 set 'ae' , 
xattr_91 set 's' , 
xattr_92 set 'z' ].

xrule xschm_28/34 :
[
xattr_87 in ['ay', 'az'] , 
xattr_88 in ['ay', 'az'] , 
xattr_89 in ['j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa'] ]
==>
[
xattr_90 set 'x' , 
xattr_91 set 'u' , 
xattr_92 set 'm' ].

xrule xschm_28/35 :
[
xattr_87 in ['ay', 'az'] , 
xattr_88 in ['ay', 'az'] , 
xattr_89 in ['ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap'] ]
==>
[
xattr_90 set 'be' , 
xattr_91 set 'o' , 
xattr_92 set 'aa' ].

xrule xschm_29/0 :
[
xattr_90 in ['w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_91 in ['n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_92 in ['b', 'c', 'd', 'e', 'f', 'g', 'h'] ]
==>
[
xattr_93 set 'aj' , 
xattr_94 set 'ae' ].

xrule xschm_29/1 :
[
xattr_90 in ['w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_91 in ['n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_92 in ['i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_93 set 'ak' , 
xattr_94 set 'ad' ].

xrule xschm_29/2 :
[
xattr_90 in ['w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_91 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_92 in ['b', 'c', 'd', 'e', 'f', 'g', 'h'] ]
==>
[
xattr_93 set 'ac' , 
xattr_94 set 'r' ].

xrule xschm_29/3 :
[
xattr_90 in ['w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_91 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_92 in ['i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_93 set 'ae' , 
xattr_94 set 'ar' ].

xrule xschm_29/4 :
[
xattr_90 in ['w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_91 eq 'au' , 
xattr_92 in ['b', 'c', 'd', 'e', 'f', 'g', 'h'] ]
==>
[
xattr_93 set 'av' , 
xattr_94 set 'l' ].

xrule xschm_29/5 :
[
xattr_90 in ['w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_91 eq 'au' , 
xattr_92 in ['i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_93 set 'ao' , 
xattr_94 set 't' ].

xrule xschm_29/6 :
[
xattr_90 in ['w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_91 in ['av', 'aw'] , 
xattr_92 in ['b', 'c', 'd', 'e', 'f', 'g', 'h'] ]
==>
[
xattr_93 set 'bh' , 
xattr_94 set 'aq' ].

xrule xschm_29/7 :
[
xattr_90 in ['w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_91 in ['av', 'aw'] , 
xattr_92 in ['i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_93 set 'y' , 
xattr_94 set 'am' ].

xrule xschm_29/8 :
[
xattr_90 in ['w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_91 in ['ax', 'ay', 'az', 'ba'] , 
xattr_92 in ['b', 'c', 'd', 'e', 'f', 'g', 'h'] ]
==>
[
xattr_93 set 'be' , 
xattr_94 set 'l' ].

xrule xschm_29/9 :
[
xattr_90 in ['w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq'] , 
xattr_91 in ['ax', 'ay', 'az', 'ba'] , 
xattr_92 in ['i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_93 set 'x' , 
xattr_94 set 'z' ].

xrule xschm_29/10 :
[
xattr_90 in ['ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj'] , 
xattr_91 in ['n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_92 in ['b', 'c', 'd', 'e', 'f', 'g', 'h'] ]
==>
[
xattr_93 set 'az' , 
xattr_94 set 't' ].

xrule xschm_29/11 :
[
xattr_90 in ['ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj'] , 
xattr_91 in ['n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_92 in ['i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_93 set 'av' , 
xattr_94 set 't' ].

xrule xschm_29/12 :
[
xattr_90 in ['ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj'] , 
xattr_91 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_92 in ['b', 'c', 'd', 'e', 'f', 'g', 'h'] ]
==>
[
xattr_93 set 'ao' , 
xattr_94 set 'af' ].

xrule xschm_29/13 :
[
xattr_90 in ['ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj'] , 
xattr_91 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] , 
xattr_92 in ['i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_93 set 'ab' , 
xattr_94 set 'x' ].

xrule xschm_29/14 :
[
xattr_90 in ['ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj'] , 
xattr_91 eq 'au' , 
xattr_92 in ['b', 'c', 'd', 'e', 'f', 'g', 'h'] ]
==>
[
xattr_93 set 'v' , 
xattr_94 set 'ar' ].

xrule xschm_29/15 :
[
xattr_90 in ['ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj'] , 
xattr_91 eq 'au' , 
xattr_92 in ['i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_93 set 'ag' , 
xattr_94 set 'at' ].

xrule xschm_29/16 :
[
xattr_90 in ['ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj'] , 
xattr_91 in ['av', 'aw'] , 
xattr_92 in ['b', 'c', 'd', 'e', 'f', 'g', 'h'] ]
==>
[
xattr_93 set 'bd' , 
xattr_94 set 'h' ].

xrule xschm_29/17 :
[
xattr_90 in ['ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj'] , 
xattr_91 in ['av', 'aw'] , 
xattr_92 in ['i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_93 set 'bc' , 
xattr_94 set 'aq' ].

xrule xschm_29/18 :
[
xattr_90 in ['ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj'] , 
xattr_91 in ['ax', 'ay', 'az', 'ba'] , 
xattr_92 in ['b', 'c', 'd', 'e', 'f', 'g', 'h'] ]
==>
[
xattr_93 set 'as' , 
xattr_94 set 'au' ].

xrule xschm_29/19 :
[
xattr_90 in ['ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj'] , 
xattr_91 in ['ax', 'ay', 'az', 'ba'] , 
xattr_92 in ['i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao'] ]
==>
[
xattr_93 set 'ai' , 
xattr_94 set 'ag' ].

xrule xschm_30/0 :
[
xattr_93 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_94 in ['h', 'i', 'j'] ]
==>
[
xattr_95 set 'ak' , 
xattr_96 set 'an' ].

xrule xschm_30/1 :
[
xattr_93 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_94 in ['k', 'l'] ]
==>
[
xattr_95 set 'ak' , 
xattr_96 set 'ag' ].

xrule xschm_30/2 :
[
xattr_93 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_94 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae'] ]
==>
[
xattr_95 set 'u' , 
xattr_96 set 'af' ].

xrule xschm_30/3 :
[
xattr_93 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az'] , 
xattr_94 in ['af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] ]
==>
[
xattr_95 set 'af' , 
xattr_96 set 'ac' ].

xrule xschm_30/4 :
[
xattr_93 in ['ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi'] , 
xattr_94 in ['h', 'i', 'j'] ]
==>
[
xattr_95 set 'y' , 
xattr_96 set 'ae' ].

xrule xschm_30/5 :
[
xattr_93 in ['ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi'] , 
xattr_94 in ['k', 'l'] ]
==>
[
xattr_95 set 'aa' , 
xattr_96 set 'ay' ].

xrule xschm_30/6 :
[
xattr_93 in ['ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi'] , 
xattr_94 in ['m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae'] ]
==>
[
xattr_95 set 'ad' , 
xattr_96 set 'bc' ].

xrule xschm_30/7 :
[
xattr_93 in ['ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi'] , 
xattr_94 in ['af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au'] ]
==>
[
xattr_95 set 't' , 
xattr_96 set 'ai' ].

xrule xschm_31/0 :
[
xattr_95 in ['f', 'g', 'h'] , 
xattr_96 in ['w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] ]
==>
[
xattr_97 set 'q' , 
xattr_98 set 'ai' ].

xrule xschm_31/1 :
[
xattr_95 in ['f', 'g', 'h'] , 
xattr_96 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj'] ]
==>
[
xattr_97 set 'l' , 
xattr_98 set 'be' ].

xrule xschm_31/2 :
[
xattr_95 in ['i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae'] , 
xattr_96 in ['w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] ]
==>
[
xattr_97 set 'g' , 
xattr_98 set 'bg' ].

xrule xschm_31/3 :
[
xattr_95 in ['i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae'] , 
xattr_96 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj'] ]
==>
[
xattr_97 set 'ah' , 
xattr_98 set 'bb' ].

xrule xschm_31/4 :
[
xattr_95 in ['af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_96 in ['w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] ]
==>
[
xattr_97 set 'z' , 
xattr_98 set 'bc' ].

xrule xschm_31/5 :
[
xattr_95 in ['af', 'ag', 'ah', 'ai', 'aj', 'ak'] , 
xattr_96 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj'] ]
==>
[
xattr_97 set 'af' , 
xattr_98 set 'ao' ].

xrule xschm_31/6 :
[
xattr_95 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_96 in ['w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at'] ]
==>
[
xattr_97 set 'u' , 
xattr_98 set 'ao' ].

xrule xschm_31/7 :
[
xattr_95 in ['al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_96 in ['au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj'] ]
==>
[
xattr_97 set 'ab' , 
xattr_98 set 'ai' ].

xrule xschm_32/0 :
[
xattr_97 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v'] , 
xattr_98 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_99 set 'aw' ].

xrule xschm_32/1 :
[
xattr_97 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v'] , 
xattr_98 in ['bg', 'bh'] ]
==>
[
xattr_99 set 'ao' ].

xrule xschm_32/2 :
[
xattr_97 in ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v'] , 
xattr_98 eq 'bi' ]
==>
[
xattr_99 set 'av' ].

xrule xschm_32/3 :
[
xattr_97 in ['w', 'x'] , 
xattr_98 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_99 set 'as' ].

xrule xschm_32/4 :
[
xattr_97 in ['w', 'x'] , 
xattr_98 in ['bg', 'bh'] ]
==>
[
xattr_99 set 'bd' ].

xrule xschm_32/5 :
[
xattr_97 in ['w', 'x'] , 
xattr_98 eq 'bi' ]
==>
[
xattr_99 set 'an' ].

xrule xschm_32/6 :
[
xattr_97 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_98 in ['v', 'w', 'x', 'y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az', 'ba', 'bb', 'bc', 'bd', 'be', 'bf'] ]
==>
[
xattr_99 set 'bb' ].

xrule xschm_32/7 :
[
xattr_97 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_98 in ['bg', 'bh'] ]
==>
[
xattr_99 set 'v' ].

xrule xschm_32/8 :
[
xattr_97 in ['y', 'z', 'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as'] , 
xattr_98 eq 'bi' ]
==>
[
xattr_99 set 'bb' ].

xstat input/1: [xattr_0,'aa'].
xstat input/1: [xattr_1,'ag'].
xstat input/1: [xattr_2,'p'].
xstat input/1: [xattr_3,'bb'].
