import alice.tuprolog.*;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import java.lang.Double;
import java.lang.Long;
import java.util.Calendar;

public class tuHeart{
    // params: 0 - model, 1- list of tables to fire
    public static void main(String[] srg) throws IOException, InterruptedException {

        BufferedWriter bw = new BufferedWriter(new FileWriter("results.txt",true));
        /*
        Iniclalizacja atrybutow musi byc w pliku PL
         */

        //measuring block
        new Thread(new Measurer()).start();
        while (Measurer.cpu == -1) {
            System.out.println("Waiting for measuring thread...");
            Thread.sleep(3000);
        }
        Date start = new Date();
        //measuring block

        Prolog engine = new Prolog();
        try{
            Theory t = new Theory(new java.io.FileInputStream("tuheart.pl"));
            engine.setTheory(t);

//	     Theory thermostat = new Theory(new java.io.FileInputStream(srg[0]));
//	     engine.addTheory(thermostat);

            srg = new String [2];
//            srg[0] = "psc-zatrudnienie-ocena_kandydata.pl";
//            srg[1] = "[ocenakandydata]";

            srg[0] = "nowy_100a_33s_765r_onlyS.pl";
            srg[1] = "[xschm_0," + "xschm_1," + "xschm_2," + "xschm_3," + "xschm_4," + "xschm_5," + "xschm_6," + "xschm_7," + "xschm_8," + "xschm_9," + "xschm_10," + "xschm_11," + "xschm_12," + "xschm_13," + "xschm_14," + "xschm_15," + "xschm_16," + "xschm_17," + "xschm_18," + "xschm_19," + "xschm_20," + "xschm_21," + "xschm_22," + "xschm_23," + "xschm_24," + "xschm_25," + "xschm_26," + "xschm_27," + "xschm_28," + "xschm_29," + "xschm_30," + "xschm_31," + "xschm_32]";

            Theory theory = new Theory(new java.io.FileInputStream(srg[0]));
            engine.addTheory(theory);

            System.out.println("Theory Loaded...");

            SolveInfo answer;
            answer = engine.solve("gox(input/1,"+srg[1]+",foi).");
            answer = engine.solve("xstat current: State.");
            System.out.println(answer.getSolution());

            while(engine.hasOpenAlternatives()){
                answer = engine.solveNext();
                System.out.println(answer.getSolution());
            }

            Date end = new Date();
            Measurer.ifContinue = false;

            Long [] mem = Measurer.memoryUsage.toArray(new Long[Measurer.memoryUsage.size()]);
            long minMem = mem[0];
            long maxMem = mem[0];
            for (long m : mem) {
                if (m < minMem) minMem = m;
                if (m > maxMem) maxMem = m;
            }

            System.out.println("MAX MEM: "+maxMem+", min mem: "+minMem);

            Double [] proc = Measurer.processorUsage.toArray(new Double[Measurer.processorUsage.size()]);
            double minProc = proc[0];
            double maxProc = proc[0];
            for (double p : proc) {
                if (p < minProc) minProc = p;
                if (p > maxProc) maxProc = p;
            }
                bw.write(printDateTime());
                bw.write("Mem diff: ");
                bw.write(String.valueOf( (maxMem - minMem) / 1024L));
                bw.write(" KB , Processor diff: ");
                bw.write(String.valueOf(maxProc-minProc));
                bw.write(" % , Time diff: ");
                bw.write(String.valueOf(end.getTime() - start.getTime()));
                bw.write(" ms \n-------\n");
                bw.close();

        }catch(Exception e){e.printStackTrace();}
    }

    private static String printDateTime() {
        Calendar c = Calendar.getInstance();
        return String.format("%tY-%tm-%td %tH:%tM:%tS : ", c, c, c, c, c, c);
    }
}
